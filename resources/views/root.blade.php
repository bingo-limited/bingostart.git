<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{config('amis-admin.title')}}</title>
{{--    {{ \Bingo\BingoStart::css() }}--}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/amis@6.0.0/sdk/sdk.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/amis@6.0.0/sdk/helper.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/amis@6.0.0/sdk/iconfont.css">
    <script>
        window.BwmsAdmin = @json($config)
    </script>
</head>
<body>
<div id="app"></div>
{{--{{\Bingo\BingoStart::baseJs() }}--}}
<script src="https://cdn.jsdelivr.net/npm/amis@6.4.0/sdk/sdk.js"></script>

{{ vite_assets() }}
{{--{{ \Bingo\BingoStart::js() }}--}}
</body>
</html>
