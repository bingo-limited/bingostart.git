<?php

namespace Bingo\Foundation\Providers;

use Illuminate\Support\AggregateServiceProvider;
use Illuminate\Contracts\Support\DeferrableProvider;

/**
 * AppDeferSupportServiceProvider supplies deferred providers
 */
class AppDeferSupportServiceProvider extends AggregateServiceProvider implements DeferrableProvider
{
    /**
     * provides gets the services provided by the provider
     */
    protected $providers = [
        // App
        \Bingo\Core\Mail\MailServiceProvider::class,
        \Bingo\Core\Html\HtmlServiceProvider::class,
        //        \Bingo\Core\Flash\FlashServiceProvider::class,
        \Bingo\Core\Parse\ParseServiceProvider::class,
        //        \Bingo\Core\Assetic\AsseticServiceProvider::class,
        \Bingo\Core\Resize\ResizeServiceProvider::class,
        //        \Bingo\Core\Validation\ValidationServiceProvider::class,
        //        \Bingo\Core\Translation\TranslationServiceProvider::class,
        \Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,

        // Console
//        \Bingo\Foundation\Providers\ArtisanServiceProvider::class,
        //        \Bingo\Core\Database\MigrationServiceProvider::class,
        //        \Bingo\Core\Scaffold\ScaffoldServiceProvider::class,
        \Illuminate\Foundation\Providers\ComposerServiceProvider::class
    ];
}
