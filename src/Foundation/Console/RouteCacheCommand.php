<?php

namespace Bingo\Foundation\Console;

use Illuminate\Foundation\Console\RouteCacheCommand as RouteCacheCommandBase;
use Illuminate\Support\HigherOrderTapProxy;

class RouteCacheCommand extends RouteCacheCommandBase
{
    /**
     * Boot a fresh copy of the application and get the routes.
     *
     * @return HigherOrderTapProxy
     */
    protected function getFreshApplicationRoutes(): HigherOrderTapProxy
    {
        $routes = $this->getFreshApplication()['router']->registerLateRoutes();

        return tap($routes->getRoutes(), function ($routes) {
            $routes->refreshNameLookups();
            $routes->refreshActionLookups();
        });
    }
}
