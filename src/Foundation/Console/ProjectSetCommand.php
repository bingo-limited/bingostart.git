<?php

namespace Bingo\Foundation\Console;

use Illuminate\Http\Client\ConnectionException;
use Url;
use Http;
use Config;
use Illuminate\Console\Command;
use Bingo\Core\Composer\Manager as ComposerManager;
use Exception;
use Illuminate\Http\Client\Response;

/**
 * ProjectSetCommand 設置項目許可證密鑰。
 *
 * @package Bingo\system
 */
class ProjectSetCommand extends Command
{
    /**
     * @var string 控制台命令的簽名
     */
    protected $signature = 'project:set {key?}';

    /**
     * 控制台命令描述。
     *
     * @var string
     */
    protected $description = '設置項目許可證密鑰。';

    /**
     * 執行控制台命令。
     *
     * @return int|void
     */
    public function handle()
    {
        $projectKey = (string) $this->argument('key');

        if (! $projectKey) {
            $this->comment(__("請輸入有效的許可證密鑰以繼續。"));

            $projectKey = trim($this->ask(__("許可證密鑰")));
        }

        try {
            // 使用網關驗證輸入
            $result = $this->requestServerData('project/detail', ['id' => $projectKey]);

            // 檢查項目狀態
            $isActive = $result['is_active'] ?? false;
            if (! $isActive) {
                $this->output->error(__("許可證未付費或已過期。請訪問 Bingocms.com 獲取許可證。"));
                return;
            }

            // 存儲項目詳情
            $this->storeProjectDetails($result);

            // 將網關添加為 composer 存儲庫
            ComposerManager::instance()->addBingoRepository($this->getComposerUrl());

            $this->output->success(__("感謝您成為 Bingo CMS 的客戶！"));
        } catch (Exception $e) {
            $this->output->error($e->getMessage());
            return 1;
        }
    }

    /**
     * 存儲項目詳情
     *
     * @param array $result
     */
    protected function storeProjectDetails(array $result): void
    {
        // 在本地保存項目
        if (class_exists(\System\Models\Parameter::class)) {
            \System\Models\Parameter::set([
                'system::project.id' => $result['id'],
                'system::project.key' => $result['project_id'],
                'system::project.name' => $result['name'],
                'system::project.owner' => $result['owner'],
                'system::project.is_active' => $result['is_active']
            ]);
        } else {
            if (! is_dir($cmsStorePath = storage_path('cms'))) {
                mkdir($cmsStorePath);
            }

            $this->injectJsonToFile(storage_path('cms/project.json'), [
                'project' => $result['project_id']
            ]);
        }

        // 保存認證令牌
        ComposerManager::instance()->addAuthCredentials(
            $this->getComposerUrl(false),
            $result['email'],
            $result['project_id']
        );
    }

    /**
     * 聯繫更新服務器以獲取響應。
     *
     * @param string $uri 網關 API URI
     * @param array $postData 額外的 POST 數據
     * @return array
     * @throws Exception
     */
    public function requestServerData(string $uri, array $postData = []): array
    {
        $result = $this->makeHttpRequest($this->createServerUrl($uri), $postData);

        $contents = $result->body();

        if ($result->status() === 404) {
            throw new Exception(__('未找到響應'));
        }

        if ($result->status() !== 200) {
            throw new Exception(
                strlen($contents)
                    ? $contents
                    : __("響應為空")
            );
        }

        $resultData = false;

        try {
            $resultData = @json_decode($contents, true);
        } catch (Exception $ex) {
            throw new Exception(__("響應無效"));
        }

        if ($resultData === false || (is_string($resultData) && ! strlen($resultData))) {
            throw new Exception(__("響應格式錯誤"));
        }

        return $resultData;
    }

    /**
     * 從提供的 URI 創建完整的網關服務器 URL
     *
     * @param  string $uri URI
     * @return string      URL
     */
    protected function createServerUrl(string $uri): string
    {
        $gateway = Config::get('system.update_gateway', 'https://gateway.Bingocms.com/api');
        if (! str_ends_with($gateway, '/')) {
            $gateway .= '/';
        }

        return $gateway.$uri;
    }

    /**
     * 向 URL 發送專門的服務器請求。
     *
     * @param string $url
     * @param array $postData
     * @return Response
     * @throws ConnectionException
     */
    protected function makeHttpRequest(string $url, array $postData): Response
    {
        // 新的 HTTP 實例
        $http = Http::asForm();

        // POST 數據
        $postData['protocol_version'] = '2.0';
        $postData['client'] = 'Bingo CMS';
        $postData['server'] = base64_encode(json_encode([
            'php' => PHP_VERSION,
            'url' => Url::to('/'),
            'since' => date('c')
        ]));

        // 網關認證
        if ($credentials = Config::get('system.update_gateway_auth')) {
            if (is_string($credentials)) {
                $credentials = explode(':', $credentials);
            }

            list($user, $pass) = $credentials;
            $http->withBasicAuth($user, $pass);
        }

        return $http->post($url, $postData);
    }

    /**
     * 返回 composer 的端點
     *
     * @param bool $withProtocol
     * @return string
     */
    protected function getComposerUrl(bool $withProtocol = true): string
    {
        $gateway = Config::get('system.composer_gateway', 'gateway.Bingocms.com');

        return $withProtocol ? 'https://'.$gateway : $gateway;
    }

    /**
     * 將 JSON 數組合併到現有的 JSON 文件中。
     * 合併對於保留數組值很有用。
     *
     * @param string $filename
     * @param array $jsonArr
     * @param bool $merge
     */
    protected function injectJsonToFile(string $filename, array $jsonArr, bool $merge = false): void
    {
        $contentsArr = file_exists($filename)
            ? json_decode(file_get_contents($filename), true)
            : [];

        $newArr = $merge
            ? array_merge_recursive($contentsArr, $jsonArr)
            : $this->mergeRecursive($contentsArr, $jsonArr);

        $content = json_encode($newArr, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);

        file_put_contents($filename, $content);
    }

    /**
     * 替代原生的 PHP array_merge_recursive 函數，使其更適合配置。
     * 標量值被替換而不是合併到自己的新數組中。
     *
     * @param array $array1
     * @param array $array2
     * @return array
     */
    protected function mergeRecursive(array $array1, array $array2): array
    {
        if ($array2) {
            foreach ($array2 as $key => $val2) {
                if (
                    is_array($val2) &&
                    (($val1 = $array1[$key] ?? null) !== null) &&
                    is_array($val1)
                ) {
                    $array1[$key] = $this->mergeRecursive($val1, $val2);
                } else {
                    $array1[$key] = $val2;
                }
            }
        }

        return $array1;
    }
}
