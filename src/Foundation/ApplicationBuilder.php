<?php

namespace Bingo\Foundation;

use Illuminate\Foundation\Configuration\ApplicationBuilder as BaseApplicationBuilder;
use Bingo\Foundation\Http\Kernel as BingoHttpKernel;
use Bingo\Foundation\Console\Kernel as BingoConsoleKernel;
use Bingo\Exceptions\Handler as BingoExceptionHandler;

class ApplicationBuilder extends BaseApplicationBuilder
{
    /**
     * 註冊應用程序的標準內核類與自定義類。
     *
     * @return $this
     */
    public function withKernels(): self
    {
        $this->app->singleton(
            \Illuminate\Contracts\Http\Kernel::class,
            BingoHttpKernel::class
        );

        $this->app->singleton(
            \Illuminate\Contracts\Console\Kernel::class,
            BingoConsoleKernel::class
        );

        return $this;
    }

    /**
     * 註冊應用程序的自定義異常處理程序。
     *
     * @return $this
     */
    public function withExceptionHandler(): self
    {
        $this->app->singleton(
            \Illuminate\Contracts\Debug\ExceptionHandler::class,
            BingoExceptionHandler::class
        );

        return $this;
    }
}
