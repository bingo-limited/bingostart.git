<?php

namespace Bingo\Providers;

use Illuminate\Support\ServiceProvider;
use Bingo\Core\Services\ShortcodeService;

/**
 * ShortcodeServiceProvider 服务提供者，用于注册和配置 Shortcode 服务。
 */
class ShortcodeServiceProvider extends ServiceProvider
{
    /**
     * 注册服务。
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(ShortcodeService::class, function () {
            return new ShortcodeService();
        });
    }

    /**
     * 启动服务。
     *
     * @return void
     */
    public function boot(): void
    {
        // 注册默认的短代码
        $this->registerDefaultShortcodes();
    }

    /**
     * 注册默认的短代码。
     *
     * @return void
     */
    protected function registerDefaultShortcodes(): void
    {
        add_shortcode('example', function ($attrs, $content) {
            return '<div class="example">'.$content.'</div>';
        });

        add_shortcode('button', function ($attrs) {
            $text = $attrs['text'] ?? 'Click me';
            $url = $attrs['url'] ?? '#';
            return '<a href="'.$url.'" class="btn btn-primary">'.$text.'</a>';
        });
    }
}
