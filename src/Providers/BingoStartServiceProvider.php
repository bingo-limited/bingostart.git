<?php

namespace Bingo\Providers;

use Bingo\App\Core\CurrentApp;
use Bingo\Core\Assets\AssetsManager;
use Bingo\Core\Config\SettingsManager;
use Bingo\Core\Input\Contract\ResponseFormat;
use Bingo\Core\Input\Format;
use Bingo\Core\Monitor\DatabaseMonitor;
use Bingo\Core\Monitor\HttpMonitor;
use Bingo\Core\Monitor\StatisticMonitor;
use Bingo\Core\Permission\PermissionManager;
use Bingo\Exceptions\BizException;
use Bingo\Exceptions\Handler;
use Bingo\Support\DB\Query;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Http\Events\RequestHandled;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use ReflectionException;
use Bingo\Support\Macros\MacrosRegister;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Bingo\Module\ModuleManager as ModuleManagerAlias;

/**
 * BingoStart Service Provider
 */
class BingoStartServiceProvider extends ServiceProvider
{
    protected array $routeMiddleware = [
        'admin.bootstrap' => \Bingo\App\Admin\Middleware\BootstrapMiddleware::class,
        'admin.auth' => \Bingo\App\Admin\Middleware\AuthMiddleware::class,
        'web.bootstrap' => \Bingo\App\Web\Middleware\BootstrapMiddleware::class,
        'api.bootstrap' => \Bingo\App\Api\Middleware\BootstrapMiddleware::class,
        'api.session' => \Bingo\App\Api\Middleware\SessionMiddleware::class,
        'openApi.bootstrap' => \Bingo\App\OpenApi\Middleware\BootstrapMiddleware::class,
        'auth.iam' => \Bingo\Middleware\IAMAuthMiddleware::class
    ];

    /**
     * boot
     *
     * @return void
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface|BizException|ReflectionException
     */
    public function boot(): void
    {
        $this->loadViewsFrom(base_path('Modules'), 'module');

        $this->bootModuleProviders();
        $this->registerModuleSettings();
        $this->registerEvents();
        $this->listenDBLog();
        $this->app->make(MacrosRegister::class)->boot();
    }

    /**
     * register
     *
     * @return void
     * @throws ReflectionException
     */
    public function register(): void
    {
        $this->publishConfig();
        $this->registerRouteMiddleware();
        $this->registerCommands();
        $this->registerConfig();
        $this->registerAssetManager();
        $this->registerExceptionHandler();
        $this->setupMonitor();
        $this->registerBladeDirectives();
        $this->registerModulePermissions();
    }

    private function publishConfig(): void
    {
        $path = dirname(__DIR__, 2).DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'%s.php';
        $path_response = sprintf($path, 'response');
        $path_bingo = sprintf($path, 'bingo');
        $path_env = sprintf($path, 'env');
        $path_module = sprintf($path, 'module');
        $path_data = sprintf($path, 'data');
        if ($this->app->runningInConsole()) {
            $this->publishes([$path_response => config_path('response.php')], 'response');
            $this->publishes([$path_bingo => config_path('bingo.php')], 'bingo');
            $this->publishes([$path_env => config_path('env.php')], 'env');
            $this->publishes([$path_module => config_path('module.php')], 'module');
            $this->publishes([$path_data => config_path('data.php')], 'data');
        }
        $this->mergeConfigFrom($path_response, 'response');
        $this->mergeConfigFrom($path_bingo, 'bingo');
        $this->mergeConfigFrom($path_env, 'env');
        $this->mergeConfigFrom($path_module, 'module');
        $this->mergeConfigFrom($path_data, 'data');
        $this->loadViewsFrom(dirname(__DIR__, 2).DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR.'views', 'bingo-admin');
        $this->loadTranslationsFrom(dirname(__DIR__, 2).DIRECTORY_SEPARATOR.'lang'.DIRECTORY_SEPARATOR, 'bingo');

        $this->publishes([dirname(__DIR__, 2).DIRECTORY_SEPARATOR.'asset' => public_path('asset')], 'bingo');
        $this->loadViewsFrom(dirname(__DIR__, 2).DIRECTORY_SEPARATOR.'views', 'bingo');
        $this->publishes([dirname(__DIR__, 2).DIRECTORY_SEPARATOR.'resources'.DIRECTORY_SEPARATOR.'lang' => base_path('resources/lang')], 'bingo');
    }


    /**
     * register commands
     *
     * @return void
     * @throws ReflectionException
     */
    protected function registerCommands(): void
    {
        loadCommands(dirname(__DIR__).DIRECTORY_SEPARATOR.'Commands', 'Bingo\\');
    }

    /**
     * bind config
     *
     * @return void
     */
    protected function registerConfig(): void
    {
        // register config driver
        $this->app->singleton('bingostartConfig', config('bingo.config.driver'));

        //register response format
        $this->app->singleton(ResponseFormat::class, function ($app) {
            $formatter = $app->config->get('response.format.class');
            $config = $app->config->get('response.format.config');
            return match (true) {
                class_exists($formatter) && is_subclass_of($formatter, ResponseFormat::class) => new $formatter($config),
                default => new Format($config),
            };
        });
    }

    /**
     * register asset manager
     * @return void
     */
    protected function registerAssetManager(): void
    {
        $this->app->singleton('assetManager', function () {
            return new AssetsManager([
                'basePath' => public_path('assets'),
                'baseUrl' => 'assets',
                'appendTimestamp' => true,
                'linkAssets' => false,
                'fileMode' => 0644,
                'dirMode' => 0755,
            ]);
        });

    }

    /**
     * 注册模块设置
     */
    private function registerModuleSettings(): void
    {
        $settingsManager = app(SettingsManager::class);
        $modules = ModuleManagerAlias::listAllEnabledModules();
        foreach ($modules as $module => $_) {
            $providerClass = "\\Modules\\$module\\Providers\\{$module}ServiceProvider";
            if (class_exists($providerClass)) {
                $provider = $this->app->getProvider($providerClass);
                if ($provider && method_exists($provider, 'registerSettings')) {
                    $settingsManager->registerSettings($module, $provider->registerSettings());
                }
            }
        }
    }

    /**
     * 注册模块权限
     *
     * 该方法负责在应用程序中注册唯一的权限管理器实例它使用 singleton 方法确保在整个应用程序生命周期中
     * 只创建一个 PermissionManager 实例
     */
    public function registerModulePermissions(): void
    {
        $this->app->singleton(PermissionManager::class, function () {
            return new PermissionManager();
        });
    }

    /**
     * register events
     *
     * @return void
     */
    protected function registerEvents(): void
    {
        Event::listen(RequestHandled::class, config('bingo.response.request_handled_listener'));
    }

    /**
     * register exception handler
     *
     * @return void
     */
    protected function registerExceptionHandler(): void
    {
        if (isRequestFromDashboard()) {
            $this->app->singleton(ExceptionHandler::class, Handler::class);
        }
    }


    /**
     * 加载所有已经开启的模块
     * boot module
     * @throws BindingResolutionException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface|BizException|ReflectionException
     */
    protected function bootModuleProviders(): void
    {
        $modules = ModuleManagerAlias::listAllEnabledModules();

        foreach ($modules as $module => $_) {
            // 1. 注册主服务提供者
            $mainProvider = "\\Modules\\$module\\Providers\\{$module}ServiceProvider";
            if (class_exists($mainProvider)) {
                $this->app->register($mainProvider);
            }

            // 2. 注册模块基础配置中定义的提供者
            $basic = ModuleManagerAlias::getModuleBasic($module);
            if (!empty($basic['providers'])) {
                foreach ($basic['providers'] as $provider) {
                    if (class_exists($provider)) {
                        $this->app->register($provider);
                    }
                }
            }

            // 3. 注册 Providers 目录下的其他服务提供者
            $this->registerModuleAdditionalProviders($module, $mainProvider);

            // 4. 注册模块命令
            $this->registerModuleCommands($module);
        }

        $this->registerModuleRoutes();
    }

    /**
     * 注册模块下的其他服务提供者
     *
     * @param string $module
     * @param string $mainProvider
     */
    private function registerModuleAdditionalProviders(string $module, string $mainProvider): void
    {
        $providersPath = base_path("Modules/$module/Providers");
        if (!is_dir($providersPath)) {
            return;
        }

        // 使用 PHP 原生的目录遍历，性能更好
        $dir = new \DirectoryIterator($providersPath);
        foreach ($dir as $file) {
            if ($file->isFile() && str_ends_with($file->getFilename(), 'ServiceProvider.php')) {
                $providerClass = "\\Modules\\$module\\Providers\\" . $file->getBasename('.php');
                if (class_exists($providerClass) && $providerClass !== $mainProvider) {
                    $this->app->register($providerClass);
                }
            }
        }
    }

    /**
     * 注册模块的命令
     *
     * @param string $module
     */
    private function registerModuleCommands(string $module): void
    {
        $commandsPath = base_path("Modules/$module/Commands");
        if (is_dir($commandsPath)) {
            $namespace = "Modules\\$module\\Commands";
            loadCommands($commandsPath, $namespace);
        }
    }

    private function registerRouteMiddleware(): void
    {
        $router = app('router');
        foreach ($this->routeMiddleware as $key => $middleware) {
            $router->aliasMiddleware($key, $middleware);
        }
    }

    /**
     * register module routes
     *
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    protected function registerModuleRoutes(): void
    {
        if (! $this->app->routesAreCached()) {
            $configRoute = $this->app['config']->get('bingo.route');

            if (! empty($configRoute)) {
                $routes = $this->app['config']->get('bingo.module.routes');
                foreach ($routes as $routeArr) {
                    $apiType = $routeArr["type"];
                    $route = $routeArr["route"];
                    switch ($apiType) {
                        case CurrentApp::WEB:
                            CurrentApp::set(CurrentApp::WEB);
                            Route::prefix(config('bingo.web.prefix'))
                                ->middleware(['web.bootstrap'])
                                ->group($route);
                            break;
                        case CurrentApp::OPEN_API:
                            CurrentApp::set(CurrentApp::OPEN_API);
                            Route::prefix(config('bingo.openApi.prefix'))
                                ->middleware(['openApi.bootstrap'])
                                ->group($route);
                            break;
                        case CurrentApp::API:
                            CurrentApp::set(CurrentApp::API);
                            Route::prefix(config('bingo.api.prefix'))
                                ->middleware(['api.bootstrap'])
                                ->group($route);
                            break;
                        case CurrentApp::ADMIN:
                            CurrentApp::set(CurrentApp::ADMIN);
                            if (! config('bingostart.admin.disabled', false)) {
                                Route::prefix(config('bingo.admin.prefix'))
                                    ->middleware($configRoute['middlewares'])
                                    ->group($route);
                            }
                            break;
                        default:
                            break;
                    }

                }
            }
        }
    }

    /**
     * listen db log
     *
     * @return void
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    protected function listenDBLog(): void
    {
        if ($this->app['config']->get('bingo.listen_db_log')) {
            Query::listen();

            $this->app->terminating(function () {
                Query::log();
            });
        }
    }

    /**
     * file exist
     *
     * @return bool
     */
    protected function routesAreCached(): bool
    {
        return $this->app->routesAreCached();
    }


    private function setupMonitor(): void
    {
        DatabaseMonitor::init();
        HttpMonitor::init();
        StatisticMonitor::init();
    }

    /**
     * register blade directives
     * 注册自定义模版命令
     * @return void
     */

    private function registerBladeDirectives(): void
    {
        $this->app->singleton('assetPathDriver', config('bingo.asset.driver'));

        Blade::directive(
            'asset',
            function ($expression = '') use (&$assetBase) {
                if (empty($expression)) {
                    return '';
                }
                $regx = '/(.+)/i';
                if (preg_match($regx, $expression, $mat)) {
                    $file = trim($mat[1], '\'" ');
                    $driver = app('assetPathDriver');
                    return $driver->getCDN($file).$driver->getPathWithHash($file);
                } else {
                    return '';
                }
            }
        );
    }
}
