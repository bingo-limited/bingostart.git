<?php

namespace Bingo\Providers;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider as ServiceProviderBase;

/**
 * ServiceProvider 是一个空的总类
 *
 * @package bingo\support
 */
abstract class ServiceProvider extends ServiceProviderBase
{
    /**
     * @var Application app 实例
     */
    protected $app;

    /**
     * callBeforeResolving 设置一个在解析之前的监听器，如果已经解析则立即触发。
     *
     * @param string $name
     * @param callable $callback
     * @return void
     * @throws BindingResolutionException
     */
    protected function callBeforeResolving(string $name, callable $callback): void
    {
        $this->app->beforeResolving($name, $callback);

        if ($this->app->resolved($name)) {
            $callback($this->app->make($name), $this->app);
        }
    }
}
