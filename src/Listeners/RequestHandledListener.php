<?php

namespace Bingo\Listeners;

use Bingo\Enums\Code;
use Illuminate\Foundation\Http\Events\RequestHandled;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response as IlluminateResponse;

class RequestHandledListener
{
    /**
     * Handle the event.
     *
     * @param RequestHandled $event
     * @return void
     */
    public function handle(RequestHandled $event): void
    {
        $response = $event->response;
        if ($this->isDownloadResponse($response)) {
            // 这里不做处理，因为下载的响应已经在其他地方被正确设置
            return;
        }

        if ($response instanceof JsonResponse) {
            $exception = $response->exception;
            if ($response->getStatusCode() == SymfonyResponse::HTTP_OK && ! $exception) {
                $response->setData($this->formatData($response->getData()));
            }
        }
    }

    /**
     * @param mixed $data
     * @return array
     */
    protected function formatData(mixed $data): array
    {
        // 检查$data是否为对象，并且同时包含'code'和'message'属性
        if (is_object($data) && property_exists($data, 'code') && property_exists($data, 'message')) {
            return (array) $data;
        }

        $responseData = [
            'code' => Code::SUCCESS->value(),
            'message' => Code::SUCCESS->description("code"),
        ];

        if (is_object($data) && property_exists($data, 'per_page')
            && property_exists($data, 'total')
            && property_exists($data, 'current_page')) {
            $responseData['data'] = $data->data;
            $responseData['total'] = $data->total;
            $responseData['limit'] = $data->per_page;
            $responseData['page'] = $data->current_page;

            return $responseData;
        }

        $responseData['data'] = $data;

        return $responseData;
    }

    /**
     * 判断响应是否为下载响应
     *
     * @param $response
     * @return bool
     */
    protected function isDownloadResponse($response): bool
    {
        return $response instanceof IlluminateResponse &&
            $response->headers->has('Content-Disposition') &&
            str_contains($response->headers->get('Content-Disposition'), 'attachment');
    }
}
