<?php

declare(strict_types=1);

namespace Bingo\Base;

use Bingo\Enums\Code;
use Bingo\Exceptions\FailedException;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * base bingo controller
 */
abstract class BingoController extends Controller
{
    /**
     * @param string|null $guard
     * @param string|null $field
     * @return mixed
     * @throws FailedException
     */
    protected function getLoginUser(string|null $guard = null, string|null $field = null): mixed
    {
        $user = Auth::guard($guard ?: getGuardName())->user();

        if (! $user) {
            throw new FailedException('登录失效, 请重新登录', Code::LOST_LOGIN);
        }

        if ($field) {
            return $user->getAttribute($field);
        }

        return $user;
    }


    /**
     * @param null $guard
     * @return mixed
     * @throws FailedException
     */
    protected function getLoginUserId($guard = null): mixed
    {
        return $this->getLoginUser($guard, 'id');
    }
}
