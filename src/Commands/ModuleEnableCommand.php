<?php

namespace Bingo\Commands;

use Bingo\Core\Events\ModuleEnabledEvent;
use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Event;
use Bingo\Module\ModuleManager;

class ModuleEnableCommand extends Command
{
    protected $signature = 'bingo:module:enable {module}';

    public function handle(): void
    {
        $module = $this->argument('module');
        BizException::throwsIf(! ModuleManager::isExists($module), Code::FAILED, T('Module Invalid'));
        $installs = ModuleManager::listAllInstalledModules();
        $basic = ModuleManager::getModuleBasic($module);
        BizException::throwsIf(! $basic, Code::FAILED, 'Module basic empty');
        $installs[$module]['enable'] = true;
        ModuleManager::saveUserInstalledModules($installs);
        //        Bingo::clearCache();

        ModuleManager::callHook($module, 'hookEnabled');

        $event = new ModuleEnabledEvent();
        $event->name = $module;
        Event::dispatch($event);
        $this->info('Module Enable Success');
    }

}
