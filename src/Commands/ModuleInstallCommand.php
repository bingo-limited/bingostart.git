<?php

namespace Bingo\Commands;

use Bingo\BingoStart;
use Bingo\Core\Events\ModuleInstalledEvent;
use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;
use Illuminate\Console\Application;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Event;
use Bingo\Core\Util\FileUtil;
use Bingo\Core\Util\VersionUtil;
use Bingo\Module\ModuleManager;
use Illuminate\Support\Facades\Process;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * 安装模块
 * Class ModuleInstallCommand
 * @package Bingo\Commands
 * @since 1.0.0
 */
class ModuleInstallCommand extends Command
{
    protected $signature = 'bingo:module:install {module} {--link-asset} {--force}';
    protected $description = 'install bingo module';

    public function handle(): void
    {
        $module = $this->argument('module');
        BizException::throwsIf(! ModuleManager::isExists($module), Code::FAILED, T('Module Invalid'));
        $installs = ModuleManager::listAllInstalledModules();
        $basic = ModuleManager::getModuleBasic($module);
        BizException::throwsIf(! $basic, Code::FAILED, 'Module basic empty');
        BizException::throwsIf(! VersionUtil::match(BingoStart::VERSION, $basic['bingostartVersion']), Code::FAILED, T('Module %s:%s depends on Bingo:%s, install fail', $module, $basic['version'], $basic['bingostartVersion']));
        if (method_exists(ModuleManager::class, 'getEnv')) {
            $env = ModuleManager::getEnv();
            BizException::throwsIf(
                ! in_array($env, $basic['env']),
                Code::FAILED,
                T('Module %s:%s compatible with env %s, current is %s', $module, $basic['version'], join(',', $basic['env']), $env)
            );
        }

        foreach ($basic['require'] as $require) {
            list($m, $v) = VersionUtil::parse($require);
            BizException::throwsIf(! isset($installs[$m]), Code::FAILED, T('Module %s:%s depend on %s:%s, install fail', $module, $basic['version'], $m, $v));
            $mBasic = ModuleManager::getModuleBasic($m);
            BizException::throwsIf(! VersionUtil::match($mBasic['version'], $v), Code::FAILED, T('Module %s:%s depend on %s:%s, install fail', $module, $basic['version'], $m, $v));
        }

        if (! empty($basic['conflicts'])) {
            foreach ($basic['conflicts'] as $conflict) {
                list($m, $v) = VersionUtil::parse($conflict);
                if (! isset($installs[$m])) {
                    continue;
                }
                $mBasic = ModuleManager::getModuleBasic($m);
                BizException::throwsIf(VersionUtil::match($mBasic['version'], $v), Code::FAILED, T('Module %s:%s conflict with %s:%s, install fail', $module, $basic['version'], $m, $v));
            }
        }

        $this->migrate($module);
        $this->seed($module);
        $this->publishAsset($module);
        $this->publishRoot($module);

        if (! isset($installs[$module])) {
            $installs[$module] = [
                'isSystem' => ModuleManager::isSystemModule($module),
                'enable' => false,
                'config' => []
            ];
            try {
                ModuleManager::saveUserInstalledModules($installs);
            } catch (NotFoundExceptionInterface|ContainerExceptionInterface $e) {
                $this->error($e->getMessage());
            }
        }

        //BingoStart::clearCache();

        ModuleManager::callHook($module, 'hookInstalled');

        $event = new ModuleInstalledEvent();
        $event->name = $module;
        Event::dispatch($event);

        $this->info('Module Install Success');
    }


    /**
     * 迁移数据
     * @param $module
     * @return void
     */
    private function migrate($module): void
    {
        $path = ModuleManager::path($module, 'database/migrations');
        if (! file_exists($path)) {
            return;
        }
        $this->info('Module Migrate Success');
        $this->call('migrate', ['--path' => ModuleManager::relativePath($module, 'Migrate'), '--force' => true]);
    }

    /**
     * 导入初始化数据
     * @param $module
     * @return void
     */
    protected function seed($module): void
    {
        if (app()->runningInConsole()) {
            Process::run(Application::formatCommandString('bingo:seed '.$module))->throw();
        } else {
            ModuleManager::dbSeed($module);
        }
        $this->info('Module Seed Success');
    }

    /**
     * 发布配置文件
     * publishRoot函数的用途是在模块化开发中，将模块的资源文件（如配置文件、视图文件等）
     * 发布到Laravel应用的相应目录下，同时提供了文件备份和更新的机制，
     * 保证了文件操作的安全性和可追溯性。这样的设计有利于模块的独立开发和维护，使得模块之间的依赖和冲突可以有效管理。
     * @param $module
     * @return void
     */
    private function publishRoot($module): void
    {
        $root = ModuleManager::path($module, 'ROOT');
        if (! file_exists($root)) {
            return;
        }
        $files = FileUtil::listAllFiles($root);
        $files = array_filter($files, function ($file) {
            return $file['isFile'];
        });
        $publishFiles = 0;
        foreach ($files as $file) {
            $relativePath = $file['filename'];
            $relativePathBackup = $relativePath.'._delete_.'.$module;
            $currentFile = base_path($relativePath);
            $currentFileBackup = $currentFile.'._delete_.'.$module;
            if (file_exists($currentFile) && ! file_exists($currentFileBackup)) {
                rename($currentFile, $currentFileBackup);
                $this->info("Module Root Publish : $relativePath -> $relativePathBackup");
            }
            if (! file_exists($currentFile) || md5_file($currentFile) != file_get_contents($file['pathname'])) {
                FileUtil::ensureFilepathDir($currentFile);
                file_put_contents($currentFile, file_get_contents($file['pathname']));
                if (! file_exists($currentFileBackup)) {
                    file_put_contents($currentFileBackup, '__BINGO_EMPTY_FILE__');
                }
                $publishFiles++;
            }
        }
        $this->info("Module Root Publish : $publishFiles item(s)");
    }

    /**
     * 发布静态资源
     *
     * publishAsset函数的代码是用于在Laravel框架中处理模块的资源文件（通常是前端资源如JavaScript、CSS、图片等）的发布
     * 将模块modules/xxx/Asset目录下的资源文件发布到public/vendor/xxx目录下。
     * @param $module
     * @return void
     */
    private function publishAsset($module): void
    {
        $force = $this->option('force');
        $linkAsset = $this->option('link-asset');
        $fs = $this->laravel['files'];
        $from = ModuleManager::path($module, 'Asset').'/';
        if (! file_exists($from)) {
            return;
        }
        $to = public_path("vendor/$module/");
        if (file_exists($to) && ! $force) {
            $this->info("Module Asset Publish : Ignore");
            return;
        }
        if (! file_exists(public_path('vendor'))) {
            @mkdir(public_path('vendor'), 0755);
        }
        if ($linkAsset) {
            $linkFrom = ModuleManager::path($module, 'Asset');
            $linkToRelative = "vendor/$module";
            $linkTo = public_path($linkToRelative);
            FileUtil::link($linkFrom, $linkTo);
        } else {
            $fs->deleteDirectory($to);
            $fs->copyDirectory($from, $to);
            $this->info("Module Asset Publish : $from -> $to");
        }
    }

}
