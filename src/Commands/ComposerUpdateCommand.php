<?php

namespace Bingo\Commands;

use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;
use Illuminate\Console\Command;
use Bingo\Module\ModuleManager;
use Illuminate\Support\Facades\File;

class ComposerUpdateCommand extends Command
{
    protected $signature = 'module:composer-update {module}';

    public function handle(): void
    {
        $module = $this->argument('module');
        BizException::throwsIf(! ModuleManager::isExists($module), Code::FAILED, T('Module Invalid'));
        $this->components->task("Updating Composer.json <fg=cyan;options=bold>{$module}</> Module", function () use ($module) {

            $composer_path = ModuleManager::path($module).DIRECTORY_SEPARATOR.'composer.json';

            $composer = json_decode(File::get($composer_path), true);

            $autoload = data_get($composer, 'autoload.psr-4');

            if (! $autoload) {
                return;
            }

            $key_name_with_app = sprintf('modules\\%s\\App\\', $module);

            if (! array_key_exists($key_name_with_app, $autoload)) {
                return;
            }

            unset($autoload[$key_name_with_app]);
            $key_name_with_out_app = sprintf('modules\\%s\\', $module);
            $autoload[$key_name_with_out_app] = 'app/';

            data_set($composer, 'autoload.psr-4', $autoload);

            file_put_contents($composer_path, json_encode($composer, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

        });
    }

}
