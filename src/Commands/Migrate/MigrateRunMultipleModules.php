<?php

namespace Bingo\Commands\Migrate;

use Bingo\BingoStart;
use Bingo\Commands\BingoCommand;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

/**
 * 多模块迁移命令
 *
 * 此命令用于迁移多个bingo模块的数据库迁移文件。
 * 可以通过传递多个模块名称来执行多个模块的迁移。
 *
 * 使用方法：
 * 1. 迁移多个指定模块：
 *    php artisan bingo:migrate:multiple {module1} {module2} ... {--force}
 *    例如：php artisan bingo:migrate:multiple UserModule ProductModule --force
 *
 * 2. 强制迁移多个指定模块（不提示确认信息）：
 *    php artisan bingo:migrate:multiple {module1} {module2} ... --force
 */
class MigrateRunMultipleModules extends BingoCommand
{
    /**
     * 命令的名称和签名
     *
     * @var string
     */
    protected $signature = 'bingo:migrate:multiple {modules*} {--force}';

    /**
     * 命令的描述
     *
     * @var string
     */
    protected $description = '迁移多个bingo模块. 使用方法: php artisan bingo:migrate:multiple module1 module2 --force';

    /**
     * 创建一个新的命令实例
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 执行命令
     *
     * @return void
     */
    public function handle(): void
    {
        // 获取传入的模块列表
        $modules = $this->argument('modules');

        // 遍历每个模块进行迁移
        foreach ($modules as $module) {
            $this->info("正在迁移模块 [$module]...");

            // 检查模块的迁移文件目录是否存在
            if (File::isDirectory(BingoStart::getModuleMigrationPath($module))) {

                // 遍历模块迁移文件目录下的所有文件
                foreach (File::files(BingoStart::getModuleMigrationPath($module)) as $file) {
                    // 获取迁移文件中的表名
                    $migrationContent = file_get_contents($file->getRealPath());
                    preg_match('/Schema::create\(\'([^\']+)\'/', $migrationContent, $matches);
                    $tableName = $matches[1] ?? null;

                    $this->info("正在迁移表 [$tableName] ");

                    // 如果表已经存在，跳过这个迁移文件
                    if ($tableName && Schema::hasTable($tableName)) {
                        $this->info("表 [$tableName] 已经存在，跳过迁移文件 [{$file->getFilename()}]");
                        continue;
                    }

                    // 构建迁移文件的路径
                    $path = Str::of(BingoStart::getModuleRelativePath(BingoStart::getModuleMigrationPath($module)))
                        ->remove('.')->append($file->getRelativePathname());

                    // 调用Laravel的迁移命令
                    $this->call('migrate', [
                        '--path' => $path,
                        '--force' => $this->option('force')
                    ]);

                    // 输出迁移命令的结果
                    $output = trim(Artisan::output());
                    $this->info($output);
                }
                $this->info("模块 [$module] 迁移成功");
            } else {
                $this->error("模块 [$module] 没有迁移文件");
            }
        }
    }
}
