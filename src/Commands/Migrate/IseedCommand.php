<?php

namespace Bingo\Commands\Migrate;

use Bingo\BingoStart;
use Bingo\Commands\BingoCommand;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class IseedCommand extends BingoCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'bingo:migrate:iseed {module} {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate seed file from table';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $seederPath = BingoStart::getModuleSeederPath($this->argument('module'));

        $file = $seederPath.$this->getSeederName().'.php';

        if (File::exists($file)) {
            $answer = $this->ask($file.' already exists, Did you want replace it?', 'Y');

            if (! Str::of($answer)->lower()->exactly('y')) {
                exit;
            }
        }


        $tables = explode(",", $this->argument('name'));
        $max = intval($this->option('max'));
        $chunkSize = intval($this->option('chunksize'));
        $exclude = explode(",", $this->option('exclude'));
        $preRunEvents = explode(",", $this->option('prerun'));
        $postRunEvents = explode(",", $this->option('postrun'));
        $dumpAuto = intval($this->option('dumpauto'));
        $indexed = ! $this->option('noindex');
        $orderBy = $this->option('orderby');
        $direction = $this->option('direction');


        if ($max < 1) {
            $max = null;
        }

        if ($chunkSize < 1) {
            $chunkSize = null;
        }

        $tableIncrement = 0;
        foreach ($tables as $table) {
            $table = trim($table);
            $preRunEvent = null;
            if (isset($preRunEvents[$tableIncrement])) {
                $preRunEvent = trim($preRunEvents[$tableIncrement]);
            }
            $postRunEvent = null;
            if (isset($postRunEvents[$tableIncrement])) {
                $postRunEvent = trim($postRunEvents[$tableIncrement]);
            }
            $tableIncrement++;

            // if file does not exist or force option is turned on generate seeder
            if (! \File::exists($file) || $this->option('force')) {
                $this->printResult(
                    (new Iseed())->generateSeed(
                        $table,
                        $file,
                        $this->option('database'),
                        $max,
                        $chunkSize,
                        $exclude,
                        $preRunEvent,
                        $postRunEvent,
                        $dumpAuto,
                        $indexed,
                        $orderBy,
                        $direction
                    ),
                    $table
                );
                continue;
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments(): array
    {
        return [
            ['tables', InputArgument::REQUIRED, 'comma separated string of table names'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions(): array
    {
        return [
            ['clean', null, InputOption::VALUE_NONE, 'clean iseed section', null],
            ['force', null, InputOption::VALUE_NONE, 'force overwrite of all existing seed classes', null],
            ['database', null, InputOption::VALUE_OPTIONAL, 'database connection', \Config::get('database.default')],
            ['max', null, InputOption::VALUE_OPTIONAL, 'max number of rows', null],
            ['chunksize', null, InputOption::VALUE_OPTIONAL, 'size of data chunks for each insert query', null],
            ['exclude', null, InputOption::VALUE_OPTIONAL, 'exclude columns', null],
            ['prerun', null, InputOption::VALUE_OPTIONAL, 'prerun event name', null],
            ['postrun', null, InputOption::VALUE_OPTIONAL, 'postrun event name', null],
            ['dumpauto', null, InputOption::VALUE_OPTIONAL, 'run composer dump-autoload', true],
            ['noindex', null, InputOption::VALUE_NONE, 'no indexing in the seed', null],
            ['orderby', null, InputOption::VALUE_OPTIONAL, 'orderby desc by column', null],
            ['direction', null, InputOption::VALUE_OPTIONAL, 'orderby direction', null],
            ['classnameprefix', null, InputOption::VALUE_OPTIONAL, 'prefix for class and file name', null],
            ['classnamesuffix', null, InputOption::VALUE_OPTIONAL, 'suffix for class and file name', null],
        ];
    }

    /**
     * Provide user feedback, based on success or not.
     *
     * @param boolean $successful
     * @param string $table
     * @return void
     */
    protected function printResult(bool $successful, string $table): void
    {
        if ($successful) {
            $this->info("Created a seed file from table {$table}");
            return;
        }

        $this->error("Could not create seed file from table {$table}");
    }

    /**
     * seeder name
     *
     * @return string
     */
    protected function getSeederName(): string
    {
        return Str::of($this->argument('name'))->ucfirst()->toString();
    }

}
