<?php

namespace Bingo\Commands\Migrate;

use Bingo\BingoStart;
use Bingo\Commands\BingoCommand;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class SeedRun extends BingoCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bingo:seed {module} {--seeder=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'bingo db seed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $files = File::allFiles(BingoStart::getModuleSeederPath($this->argument('module')));

        $fileNames = [];

        $seeder = $this->option('seeder');

        if ($seeder) {
            foreach ($files as $file) {
                if (pathinfo($file->getBasename(), PATHINFO_FILENAME) == $seeder) {
                    $class = require_once $file->getRealPath();

                    $seeder = new $class();

                    $seeder->run();
                }
            }
        } else {
            foreach ($files as $file) {
                if (File::exists($file->getRealPath())) {
                    $class = require_once $file->getRealPath();
                    $class = new $class();
                    if ($class instanceof Seeder) { // 检查返回的是不是 Seeder 实例
                        $class->run();
                    }
                }
            }

        }
        $this->info($this->argument('module')." db seed success");
    }
}
