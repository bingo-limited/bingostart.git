<?php

namespace Bingo\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

/**
 * 导出菜单权限配置命令
 *
 * 此命令用于将数据库中的菜单权限导出到模块的配置文件中。
 *
 * 使用方法：
 * 1. 导出指定模块的菜单权限：
 *    php artisan bingo:export:menu-config {module}
 *    例如：php artisan bingo:export:menu-config Cms
 */
class ExportMenuToConfigCommand extends Command
{
    /**
     * 命令的签名（命令名及参数）
     *
     * @var string
     */
    protected $signature = 'bingo:export:menu-config {module}';

    /**
     * 命令的描述
     *
     * @var string
     */
    protected $description = '导出模块的菜单权限到配置文件';

    /**
     * 执行命令的处理逻辑
     *
     * @return void
     */
    public function handle(): void
    {
        $module = $this->argument('module');
        $table = 'permissions';

        // 从数据库中获取指定模块的菜单权限
        $data = DB::table($table)->where('deleted_at', 0)->where('module', $module)->get();
        $data = json_decode($data->toJson(), true);

        // 导出到配置文件
        $this->exportToConfig($data, $module);

        $this->info('菜单权限已成功导出到配置文件');
    }

    /**
     * 将菜单权限数据导出到配置文件
     *
     * @param array $data 菜单权限数据
     * @param string $module 模块名称
     * @return void
     */
    protected function exportToConfig(array $data, string $module): void
    {
        // 配置文件路径
        $configPath = base_path("Modules/{$module}/config");

        // 确保配置目录存在
        if (! File::exists($configPath)) {
            File::makeDirectory($configPath, 0755, true);
        }

        $configFilePath = $configPath.'/permissions.php';

        // 生成配置内容
        $permissions = ['permissions' => $data];
        $configContent = "<?php\n\nreturn ".var_export($permissions, true).";\n";

        // 写入配置文件
        File::put($configFilePath, $configContent);
    }
}
