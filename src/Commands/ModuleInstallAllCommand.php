<?php

namespace Bingo\Commands;

use Bingo\BingoStart;
use Illuminate\Console\Command;
use Bingo\Core\Input\Response;
use Bingo\Module\ModuleManager;

class ModuleInstallAllCommand extends Command
{
    protected $signature = 'bingo:module:install-all {--link-asset}';

    public function handle(): void
    {
        $linkAsset = $this->option('link-asset');
        $this->info("ModuleInstallAll\n");
        foreach (ModuleManager::listAllInstalledModulesInRequiredOrder() as $module) {
            if (! ModuleManager::isExists($module)) {
                continue;
            }
            $this->warn(">>> Module $module Installing");
            $ret = ModuleManager::install($module, false, [
                'linkAsset' => $linkAsset,
            ]);
            if (Response::isSuccess($ret)) {
                $this->info($ret['data']['output']);
            } else {
                $this->error($ret['message']);
            }
            $this->info("");
        }

        $this->publishHotfixFiles();

        $this->warn("ModuleInstallAll Run Finished");

    }

    private function publishHotfixFiles(): void
    {
        $this->warn(">>> Publish Hotfix");
        $env = BingoStart::env();
        $dir = rtrim(base_path('vendor/bingostart/Bingo/resources/hot_fix'), '/').'/';
        $jsonFile = $dir.$env.'.json';
        if (! file_exists($jsonFile)) {
            return;
        }
        $json = @json_decode(file_get_contents($jsonFile), true);
        foreach ($json as $f => $path) {
            $content = file_get_contents($dir.$env.'/'.$f);
            @file_put_contents(base_path($path), $content);
            $this->info('Hotfix: '.$path);
        }
        $this->info("");
    }

}
