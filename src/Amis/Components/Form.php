<?php

namespace Bingo\Amis\Components;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use JsonSerializable;
use Bingo\Amis\Components\Form\Actions;
use Bingo\Amis\Components\Form\FormActions;
use Bingo\Amis\Components\Form\FormHooks;
use Bingo\Amis\Components\Form\FormMain;
use Bingo\Amis\Components\Form\FormResource;
use Bingo\Amis\Components\Form\FormToolbar;
use Bingo\Amis\Components\Form\Toolbar;
use Bingo\Amis\Renderers\Form\AmisForm;
use Bingo\Amis\Renderers\Page;

class Form implements JsonSerializable
{
    use FormMain, FormResource, ModelBase, FormToolbar, FormActions, FormHooks, ExtraQueryParams;

    public const REMOVE_FLAG_NAME = '_remove_flag';

    private Page $page;
    private string $routeName;

    private Builder $builder;
    private Model $model;

    protected bool $isDialog = false;

    public function __construct()
    {
        $this->page = Page::make()->title(T('Edit'));
        $this->form = AmisForm::make();

        $this->toolbar = new Toolbar($this);
        $this->actions = new Actions($this);

        $this->isDialog = (int) request('_dialog', 0) === 1;

    }

    public static function make($builder, string $routeName, $fun, array $extraParams = []): Form
    {
        $form = new static();
        $form->builder = $builder;
        $form->routeName = $routeName;
        $form->model = $builder->getModel();
        $fun($form);
        $form->setExtraQueryParams($extraParams);
        return $form;
    }

    public function model(): Model|Builder
    {
        return $this->builder->getModel();
    }

    /**
     * 获取AmisPage实例
     * @return Page
     */
    public function usePage(): Page
    {
        return $this->page;
    }

    /**
     * @return bool
     */
    public function isDialog(): bool
    {
        return $this->isDialog;
    }

    public function dialog(): Form
    {
        $this->isDialog = true;
        return $this;
    }

    public function jsonSerialize()
    {

        if ($this->isDialog()) {
            return $this->renderForm();
        }

        $this->page
            ->toolbar($this->toolbar->renderToolbar())
            ->body([
                $this->renderForm(),
            ]);

        return $this->page;
    }
}
