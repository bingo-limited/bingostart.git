<?php

namespace Bingo\Amis\Components\Grid;

use Bingo\Amis\Components\Grid;
use Closure;

trait GridToolbar
{
    protected Toolbar $toolbar;


    /**
     * 禁用新增操作
     * @param bool $bool
     * @return Grid|GridToolbar
     */
    public function disableCreate(bool $bool = true): self
    {
        $this->toolbar->disableCreate($bool);
        return $this;
    }

    public function disableMultilingualLanguage(bool $bool = true): self
    {
        $this->toolbar->disableMultilingualLanguage($bool);
        return $this;
    }

    /**
     * 工具栏
     * @param Closure $fun
     * @return Grid|GridToolbar
     */
    public function toolbar(Closure $fun): self
    {
        $fun($this->toolbar);
        return $this;
    }

}
