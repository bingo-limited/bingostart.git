<?php

namespace Bingo\Amis\Renderers;

/**
 * Card2 卡片渲染器
 * @method $this body($v)
 * @method $this bodyClassName($v)
 * @method $this style($v)
 * @method $this hideCheckToggler($v)
 * @method $this checkOnItemClick($v)
 * @method $this wrapperComponent($v)
 */
class Card2 extends BaseSchema
{
    public string $type = 'card2';
}
