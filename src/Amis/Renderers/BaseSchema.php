<?php

namespace Bingo\Amis\Renderers;

use Closure;
use JsonSerializable;

/**
 * 基础Schema
 * @method $this type($v) 类型
 * @method $this className($v) 样式类名
 * @method $this style($v) 样式
 * @method $this ref($v) ref
 * @method $this disabled($v)  是否禁用
 * @method $this disabledOn($v) 禁用条件
 * @method $this hidden($v) 隐藏
 * @method $this hiddenOn($v) 隐藏条件
 * @method $this visible($v) 显示
 * @method $this visibleOn($v) 显示条件
 * @method $this id($v) 唯一标识
 * @method $this value($v) 默认值
 *
 * @method $this onEvent($v) 配置事件
 *
 * @method getValue($value) 给组件赋值时自定义处理
 * @method setValue($value) 组件赋值提交时自定义处理
 * @method onDelete($value) 删除时自定义处理
 * @method defaultAttr() 可以自定义属性的设置
 */
class BaseSchema implements JsonSerializable
{
    public string $type;
    protected array $amisSchema = [];  // 存储权限配置和其他设置


    public static function make(): static
    {
        return new static();
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }

    public function __get($name)
    {
        return data_get($this, $name);
    }

    public function __call($name, $arguments)
    {
        abort_if(count($arguments) !== 1, 400, "{$name} method parameter error");
        $argument = $arguments[0];
        if ($argument instanceof Closure) {
            $argument = $argument();
        }
        $this->$name = $argument;
        return $this;
    }


    /**
     * 设置权限标识和无权限时的替换值
     * @param string $sign 权限标识
     * @param mixed $replaceValue 无权限时的替换值
     * @return $this
     */
    public function permission(string $sign, mixed $replaceValue = ''): static
    {
        $this->amisSchema['bingo_permission'] = $sign;
        $this->amisSchema['bingo_permission_replace_value'] = $replaceValue;
        return $this;
    }

    /**
     * 过滤数据基于设置的权限
     * @return array
     */
    protected function filteredResults(): array
    {
        $filteredData = [];
        foreach (get_object_vars($this) as $key => $value) {
            if (isset($this->amisSchema['bingo_permission']) && ! admin_user()->can($this->amisSchema['bingo_permission'])) {
                $filteredData[$key] = $this->amisSchema['bingo_permission_replace_value'];
            } else {
                $filteredData[$key] = $value;
            }
        }
        return $filteredData;
    }

    public function jsonSerialize(): array
    {
        if (method_exists($this, 'defaultAttr')) {
            $this->defaultAttr();
        }

        $filteredData = $this->filteredResults();

        if (empty($this->amisSchema)) {
            unset($filteredData['amisSchema']);
        }

        return $filteredData;
    }

}
