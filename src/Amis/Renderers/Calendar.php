<?php

namespace Bingo\Amis\Renderers;

/**
 * 日历
 * @method $this schedules($v) 日程
 * @method $this scheduleClassNames($v) 日程类名
 * @method $this scheduleAction($v) 点击日程事件
 */
class Calendar extends BaseSchema
{
    public string $type = 'calendar';
}
