<?php

namespace Bingo\Amis\Renderers;

/**
 * @method self lineStyle($v) 'dashed' | 'solid'
 * @method self color($v)
 * @method self direction($v)
 * @method self rotate($v)
 * @method self titleClassName($v)
 * @method self titlePosition($v)
 *
 */
class Divider extends BaseSchema
{
    public string $type = 'divider';

}
