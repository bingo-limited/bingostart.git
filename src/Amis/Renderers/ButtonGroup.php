<?php

namespace Bingo\Amis\Renderers;

/**
 * 按钮组
 * @method $this btnClassName($v) 按钮样式
 * @method $this btnActiveClassName($v) 激活按钮样式
 * @method $this buttons($v) 按钮组
 * @method $this btnLevel($v) 按钮级别
 * @method $this btnActiveLevel($v) 激活按钮级别
 * @method $this vertical($v) 垂直排列
 * @method $this tiled($v) 平铺
 * @method $this disabled($v) 禁用
 * @method $this visible($v) 显示
 * @method $this visibleOn($v) 显示条件
 * @method $this size($v) 尺寸
 */
class ButtonGroup extends BaseSchema
{
    public string $type = 'button-group';
}
