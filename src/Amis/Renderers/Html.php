<?php

namespace Bingo\Amis\Renderers;

/**
 * @method $this html($v) html内容
 */
class Html extends BaseSchema
{
    public string $type = 'html';
}
