<?php

namespace Bingo\Amis\Renderers;

/**
 * 折叠面板组
 * @method $this activeKey($v)
 * @method $this accordion($v)
 * @method $this expandIcon($v)
 * @method $this expandIconPosition($v)
 * @method $this body($v)
 */
class CollapseGroup extends BaseSchema
{
    public string $type = 'collapse-group';
}
