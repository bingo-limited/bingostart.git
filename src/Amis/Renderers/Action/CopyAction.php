<?php

namespace Bingo\Amis\Renderers\Action;

use Bingo\Amis\Renderers\Button;

/**
 * @method $this copy($v)
 */
class CopyAction extends Button
{
    public string $actionType = 'copy';
}
