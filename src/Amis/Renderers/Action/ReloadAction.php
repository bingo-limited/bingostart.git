<?php

namespace Bingo\Amis\Renderers\Action;

use Bingo\Amis\Renderers\Button;

/**
 * @method $this target($v)
 */
class ReloadAction extends Button
{
    public string $actionType = 'reload';
}
