<?php

namespace Bingo\Amis\Renderers\Action;

use Bingo\Amis\Renderers\Button;

/**
 * @method $this link($v)
 */
class LinkAction extends Button
{
    public string $actionType = 'link';
}
