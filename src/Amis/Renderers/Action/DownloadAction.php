<?php

namespace Bingo\Amis\Renderers\Action;

class DownloadAction extends AjaxAction
{
    public string $actionType = 'download';
}
