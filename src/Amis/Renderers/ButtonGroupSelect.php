<?php

namespace Bingo\Amis\Renderers;

use Bingo\Amis\Renderers\Form\FormBase;

/**
 * 按钮组选择器
 * @method $this vertical($v) 垂直排列
 * @method $this tiled($v) 平铺
 * @method $this btnLevel($v) 按钮级别
 * @method $this btnActiveLevel($v) 激活按钮级别
 * @method $this options($v) 选项
 * @method $this source($v) 数据源
 * @method $this multiple($v) 允许多选
 * @method $this labelField($v) 标签字段
 * @method $this valueField($v) 字段
 * @method $this joinValues($v) 连接值
 * @method $this extractValue($v) 提取值
 * @method $this autoFill($v) 自动填充
 */
class ButtonGroupSelect extends FormBase
{
    public string $type = 'button-group-select';
}
