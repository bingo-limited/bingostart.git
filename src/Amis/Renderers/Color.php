<?php

namespace Bingo\Amis\Renderers;

/**
 * 颜色选择器
 * @method $this defaultColor($v)
 * @method $this showValue($v)
 */
class Color extends BaseSchema
{
    public string $type = 'color';
}
