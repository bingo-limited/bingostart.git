<?php

namespace Bingo\Amis\Renderers\Form;

/**
 * @method $this optionsTip($v)
 * @method $this dropdown($v)
 */
class InputTag extends FormOptions
{
    public string $type = 'input-tag';
}
