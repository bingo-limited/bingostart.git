<?php

namespace Bingo\Amis\Renderers\Form;

/**
 * @method $this funcs($v)
 * @method $this fields($v)
 * @method $this fieldClassName($v)
 * @method $this config($v)
 * @method $this embed($v)
 * @method $this source($v)
 * @method $this draggable($v)
 * @method $this searchable($v)
 * @method $this selectMode($v)
 * @method $this builderMode($v)
 * @method $this showANDOR($v)
 * @method $this showNot($v)
 * @method $this showIf($v)
 * @method $this addGroupBtnVisibleOn($v)
 * @method $this addBtnVisibleOn($v)
 * @method $this inputSettings($v)
 * @method $this formula($v)
 * @method $this formulaForIf($v)
 */
class ConditionBuilder extends FormBase
{
    public string $type = 'condition-builder';
}
