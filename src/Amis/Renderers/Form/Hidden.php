<?php

namespace Bingo\Amis\Renderers\Form;

class Hidden extends FormBase
{
    public string $type = 'hidden';
}
