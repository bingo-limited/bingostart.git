<?php

namespace Bingo\Amis\Renderers\Form;

class InputMonthRange extends InputDateRange
{
    public string $type = 'input-month-range';
}
