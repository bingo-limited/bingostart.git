<?php

namespace Bingo\Amis\Renderers\Form;

/**
 * @method $this length($v)
 */
class UUID extends FormBase
{
    public string $type = 'uuid';
}
