<?php

namespace Bingo\Amis\Renderers\Form;

class InputYearRange extends InputDateRange
{
    public string $type = 'input-year-range';
}
