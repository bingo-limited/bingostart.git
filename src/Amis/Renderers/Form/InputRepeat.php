<?php

namespace Bingo\Amis\Renderers\Form;

/**
 * @method $this options($v)
 */
class InputRepeat extends FormBase
{
    public string $type = 'input-repeat';
}
