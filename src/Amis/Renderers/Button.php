<?php

namespace Bingo\Amis\Renderers;

/**
 * 按钮
 * @method $this id($v) id
 * @method $this block($v) 是否块级元素
 * @method $this disabledTip($v) 禁用时的提示
 * @method $this icon($v) 图标
 * @method $this align($v) 对齐方式
 * @method $this iconClassName($v) 图标类名
 * @method $this rightIcon($v) 右侧图标
 * @method $this rightIconClassName($v) 右侧图标类名
 * @method $this loadingClassName($v) 加载中的样式
 * @method $this label($v) 按钮文字
 * @method $this level($v) 按钮级别
 * @method $this primary($v) 是否主要按钮
 * @method $this size($v) 尺寸
 * @method $this tooltip($v) 提示
 * @method $this tooltipPlacement($v) 提示位置
 * @method $this type($v) 按钮类型
 * @method $this confirmText($v) 确认提示文字
 * @method $this required($v) 是否必填
 * @method $this activeLevel($v) 激活时的级别
 * @method $this activeClassName($v) 激活时的样式
 * @method $this close($v) 关闭按钮
 * @method $this requireSelected($v) 是否需要选中
 * @method $this mergeData($v) 合并数据
 * @method $this target($v) 打开目标
 * @method $this countDown($v) 倒计时
 * @method $this countDownTpl($v) 倒计时模板
 * @method $this badge($v) 角标
 * @method $this hotKey($v) 快捷键
 * @method $this loadingOn($v) 加载中
 * @method $this onClick($v) 点击事件
 * @method $this body($v) 内容
 * @method $this actionType($v) 动作类型
 * @method $this content($v) 内容
 * @method $this url($v) 链接
 * @method $this blank($v) 打开方式
 * @method $this dialog($v) 弹窗设置
 *
 */
class Button extends BaseSchema
{
    public string $type = 'button';
}
