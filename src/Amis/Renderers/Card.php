<?php

namespace Bingo\Amis\Renderers;

/**
 * Card 卡片渲染器
 * @method $this header($v) 头部
 * @method $this body($v) 内容
 * @method $this media($v) 媒体
 * @method $this actions($v) 操作
 * @method $this toolbar($v) 工具栏
 * @method $this secondary($v) 次要内容
 * @method $this source($v) 数据源
 * @method $this href($v) 外部链接
 */
class Card extends BaseSchema
{
    public string $type = 'card';

}
