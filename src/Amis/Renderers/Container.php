<?php

namespace Bingo\Amis\Renderers;

/**
 * 容器
 * @method $this body($v)
 * @method $this bodyClassName($v)
 * @method $this style($v)
 * @method $this wrapperComponent($v)
 */
class Container extends BaseSchema
{
    public string $type = 'container';
}
