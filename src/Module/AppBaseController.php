<?php

namespace Bingo\Module;

use Illuminate\Routing\Controller;
use Bingo\Core\View\ResponsiveViewTrait;

class AppBaseController extends Controller
{
    use ResponsiveViewTrait;
}
