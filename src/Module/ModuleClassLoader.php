<?php

namespace Bingo\Module;

use Composer\Autoload\ClassLoader;

/**
 * 模块命名空间动态加载器
 *
 * Class ModuleClassLoader
 * @package Bingo\Module
 */
class ModuleClassLoader
{
    /** @var ClassLoader $loader */
    private static $loader = null;
    private static array $namespacesAdded = [];

    private static function loaderInit(): void
    {
        if (null == self::$loader) {
            self::$loader = app(ClassLoader::class);
            self::$loader->register(true);
        }
    }

    public static function addClass($class, $file): void
    {
        self::loaderInit();
        self::$loader->addClassMap([$class => $file]);
    }

    public static function addNamespace($namespace, $path): void
    {
        self::loaderInit();
        if (! ends_with($namespace, '\\')) {
            $namespace = $namespace.'\\';
        }
        $namespacesAdded[$namespace] = $path;
        self::$loader->addPsr4($namespace, [$path]);
    }

    public static function addNamespaceIfMissing($namespace, $path): void
    {
        if (! self::hasNamespace($namespace)) {
            self::addNamespace($namespace, $path);
        }
    }

    /**
     * @param $namespace
     * @return bool
     * @since 1.0.0
     */
    public static function hasNamespace($namespace): bool
    {
        if (! ends_with($namespace, '\\')) {
            $namespace = $namespace.'\\';
        }
        return isset($namespacesAdded[$namespace]);
    }
}
