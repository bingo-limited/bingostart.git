<?php

namespace Bingo\Module;

use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;
use Exception;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Bingo\Core\Input\Response;
use Bingo\Core\Util\ArrayUtil;
use Bingo\Core\Util\FileUtil;
use Bingo\Core\Util\SerializeUtil;
use Bingo\Core\Util\VersionUtil;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class ModuleManager
{
    /**
     * 已安装模块配置保存KEY
     */
    public const MODULE_ENABLE_LIST = 'ModuleList';
    /**
     * 系统模块配置保存
     */
    public const MODULE_SYSTEM_OVERWRITE_CONFIG = 'ModuleSystemOverwriteConfig';

    /**
     * 获取模块的基本信息
     * @param $name
     * @return array|null
     * @throws BizException
     */
    public static function getModuleBasic($name): ?array
    {
        static $basic = [];
        if (array_key_exists($name, $basic)) {
            return $basic[$name];
        }
        if (file_exists($path = self::path($name, 'config.json'))) {
            $config = json_decode(file_get_contents($path), true);
            if (empty($config)) {
                BizException::throws(Code::FAILED, '模块配置文件错误 - '.$name);
            }
            $basic[$name] = array_merge([
                'name' => 'None',
                'title' => 'None',
                'version' => '1.0.0',
                'env' => [
                    'laravel10'
                ],
                'types' => [],
                'tags' => [],
                'require' => [
                    // 'Xxx:*'
                    // 'Xxx:>=*'
                    // 'Xxx:==*'
                    // 'Xxx:<=*'
                    // 'Xxx:>*'
                    // 'Xxx:<*'
                ],
                'suggest' => [
                    // 'Xxx:*'
                    // 'Xxx:>=*'
                    // 'Xxx:==*'
                    // 'Xxx:<=*'
                    // 'Xxx:>*'
                    // 'Xxx:<*'
                ],
                // 已知冲突模块
                'conflicts' => [
                    // 'Xxx:*'
                    // 'Xxx:>=*'
                    // 'Xxx:==*'
                    // 'Xxx:<=*'
                    // 'Xxx:>*'
                    // 'Xxx:<*'
                ],
                'bingostartVersion' => '*',
                'author' => 'Author',
                'description' => 'Description',
                'keywords' => '',
                'config' => [],
                'providers' => [],
            ], $config);
        } else {
            $basic[$name] = null;
        }
        return $basic[$name];
    }

    private static function callCommand($command, $param = []): array
    {
        try {
            $exitCode = Artisan::call($command, $param);

            $output = trim(Artisan::output());
            if (0 !== $exitCode) {
                Log::error("Bingo.ModuleManager.CallCommand.Error - $command - $output");
                return Response::generate(-1, "ERROR:$exitCode", ['output' => $output]);
            }
            return Response::generateSuccessData(['output' => $output]);
        } catch (Exception $e) {
            $message = $e->getMessage();
            //            BizException::throws(Code::FAILED,$message);
            return Response::generateError(T('Server Error').': '.$message);
        }
    }

    public static function clean($module): void
    {
        $path = self::path($module);
        if (file_exists($path)) {
            FileUtil::rm($path);
        }
    }

    /**
     * 模块安装
     * @param $module string
     * @param $force bool
     * @param $option array
     * @return array
     */
    public static function install(string $module, bool $force = false, array $option = []): array
    {
        $param = ['module' => $module];
        if ($force) {
            $param['--force'] = true;
        }
        if (! empty($option['linkAsset'])) {
            $param['--link-asset'] = true;
        }
        return self::callCommand('bingo:module:install', $param);
    }

    /**
     * 模块卸载
     * @param $module
     * @return array
     */
    public static function uninstall($module): array
    {
        return self::callCommand('bingo:module:uninstall', ['module' => $module]);
    }

    /**
     * 模块启用
     * @param $module
     * @return array
     */
    public static function enable($module): array
    {
        return self::callCommand('bingo:module:enable', ['module' => $module]);
    }

    /**
     * 模块禁用
     * @param $module
     * @return array
     */
    public static function disable($module): array
    {
        return self::callCommand('bingo:module:disable', ['module' => $module]);
    }

    /**
     * 执行模块迁移
     * @param $module
     * @return array
     */
    public static function dbSeed($module): array
    {
        return self::callCommand('bingo:seed', ['module' => $module]);
    }

    /**
     * 检查模块是否存在
     * @param $name
     * @return bool
     */
    public static function isExists($name): bool
    {
        return file_exists(self::path($name, 'config.json'));
    }

    /**
     * 模块绝对路径
     * @param $module
     * @param string $path
     * @return string
     */
    public static function path($module, string $path = ''): string
    {
        return base_path(self::relativePath($module, $path));
    }

    /**
     * 模块相对路径
     * @param $module
     * @param string $path
     * @return string
     */
    public static function relativePath($module, string $path = ''): string
    {
        return "Modules/$module".($path ? "/".trim($path, '/') : '');
    }

    /**
     * 检测是否是系统模块
     * @param $module
     * @return bool
     */
    public static function isSystemModule($module): bool
    {
        $modules = config('modules.system', []);
        return isset($modules[$module]);
    }

    /**
     * 检测模块是否已安装
     * @param $name
     * @return bool
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function isModuleInstalled($name): bool
    {
        if (! self::isExists($name)) {
            return false;
        }
        $modules = self::listAllInstalledModules();
        return isset($modules[$name]);
    }

    /**
     * 检测模块是否启用
     * @param $name
     * @return bool
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function isModuleEnabled($name): bool
    {
        $modules = self::listAllInstalledModules();
        return ! empty($modules[$name]['enable']);
    }


    /**
     * 检测模块是否匹配
     * @param $name
     * @param $version
     * @return bool
     * @throws BizException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     * @since 2.0.0
     *
     * @example
     * Ad >=1.4.0
     */
    public static function isModuleEnableMatch($name, $version): bool
    {
        if (! self::isModuleEnabled($name)) {
            return false;
        }
        $basic = self::getModuleBasic($name);
        if (! $basic) {
            return false;
        }
        return VersionUtil::match($basic['version'], $version);
    }

    /**
     * 列出本地所有的模块
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function listModules(): array
    {
        $files = FileUtil::listFiles(base_path('Modules'));
        $modules = [];
        foreach ($files as $v) {
            if (! $v['isDir'] || ! preg_match('/^[a-zA-Z0-9_]+$/', $v['filename'])) {
                continue;
            }
            if (starts_with($v['filename'], '_delete_.') || starts_with($v['filename'], '_')
                || ! file_exists($v['pathname'].'/config.json')) {
                continue;
            }
            $modules[$v['filename']] = [
                'enable' => false,
                'isSystem' => false,
                'isInstalled' => false,
                'config' => [],
            ];
        }
        foreach (self::listSystemInstalledModules() as $m => $config) {
            if (isset($modules[$m])) {
                $modules[$m]['isInstalled'] = true;
                $modules[$m]['isSystem'] = true;
                $modules[$m]['enable'] = ! empty($config['enable']);
            }
        }
        foreach (self::listUserInstalledModules() as $m => $config) {
            if (isset($modules[$m])) {
                $modules[$m]['isInstalled'] = true;
                $modules[$m]['enable'] = ! empty($config['enable']);
            }
        }
        return $modules;
    }

    /**
     * 列出所有已安装系统模块
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function listSystemInstalledModules(): array
    {
        $modules = array_build(config('module.system', []), function ($k, $v) {
            $v['isSystem'] = true;
            if (! isset($v['enable'])) {
                $v['enable'] = false;
            }
            return [$k, $v];
        });
        if (config('env.BINGO_MODULES')) {
            foreach (explode(',', config('env.BINGO_MODULES')) as $m) {
                if (! empty($m)) {
                    $modules[$m] = [
                        'isSystem' => true,
                        'enable' => true,
                    ];
                }
            }
        }
        try {
            $systemConfig = bingostart_config()->getArray(self::MODULE_SYSTEM_OVERWRITE_CONFIG);
            if (! empty($systemConfig)) {
                foreach ($systemConfig as $m => $config) {
                    if (empty($modules[$m]) || ! is_array($config)) {
                        continue;
                    }
                    if (! isset($modules[$m]['config'])) {
                        $modules[$m]['config'] = [];
                    }
                    $modules[$m]['config'] = array_merge($modules[$m]['config'], $config);
                }
            }
        } catch (Exception) {
        }
        return $modules;
    }

    /**
     * 列出所有已安装用户模块
     * listUserInstalledModules 函数是用于获取和处理所有已安装用户模块的信息的工具，
     * 它通过特定的配置项读取模块信息，并添加必要的状态信息。
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function listUserInstalledModules(): array
    {
        try {
            return array_build(bingostart_config()->getArray(self::MODULE_ENABLE_LIST), function ($k, $v) {
                $v['isSystem'] = false;
                if (! isset($v['enable'])) {
                    $v['enable'] = false;
                }
                return [$k, $v];
            });
        } catch (Exception) {
            return [];
        }
    }

    /**
     * 列出所有已安装的模块名称
     * @return string[]
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function listAllEnableModuleNames(): array
    {
        return array_keys(self::listAllEnabledModules());
    }

    /**
     * 列出所有已安装模块，包括系统和用户安装
     * @return array
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function listAllEnabledModules(): array
    {
        return array_filter(self::listAllInstalledModules(), function ($item) {
            return $item['enable'];
        });
    }

    /**
     * 列出所有模块，包括系统和用户安装
     * @param $forceReload boolean
     * @return array|null
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function listAllInstalledModules(bool $forceReload = false): ?array
    {
        static $modules = null;
        if ($forceReload) {
            $modules = null;
        }
        if (null !== $modules) {
            return $modules;
        }
        $modules = array_merge(self::listUserInstalledModules(), self::listSystemInstalledModules());
        return $modules;
    }

    /**
     * 保存用户模块信息
     * @param $modules
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function saveUserInstalledModules($modules): void
    {
        $modules = array_map(function ($item) {
            return ArrayUtil::keepKeys($item, [
                'config', 'enable',
            ]);
        }, array_filter($modules, function ($m) {
            return empty($m['isSystem']);
        }));

        bingostart_config()->setArray(self::MODULE_ENABLE_LIST, $modules);
    }

    /**
     * 获取已安装模块的依赖数
     * @param bool $ignoreError
     * @return array
     * @throws BizException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function listAllInstalledModulesInRequiredOrder(bool $ignoreError = false): array
    {
        $modules = self::listAllInstalledModules();
        $modules = array_keys($modules);
        $moduleInfoMap = [];
        foreach ($modules as $module) {
            $basic = self::getModuleBasic($module);
            if (empty($basic)) {
                continue;
            }
            $moduleInfoMap[$module] = $basic['require'];
        }
        $orderedModules = [];
        for ($i = 0; $i < 100; $i++) {
            foreach ($modules as $module) {
                if (in_array($module, $orderedModules)) {
                    continue;
                }
                $allPassed = true;
                if (! empty($moduleInfoMap[$module])) {
                    foreach ($moduleInfoMap[$module] as $requireModule) {
                        list($m, $v) = VersionUtil::parse($requireModule);
                        if (! in_array($m, $orderedModules)) {
                            $allPassed = false;
                        }
                    }
                }
                if ($allPassed) {
                    $orderedModules[] = $module;
                }
            }
            if (count($orderedModules) == count($modules)) {
                break;
            }
        }
        if (! $ignoreError) {
            if (count($modules) !== count($orderedModules)) {
                list($inserts, $deletes) = ArrayUtil::diff($orderedModules, $modules);
                $errors = [];
                foreach ($inserts as $insert) {
                    $requires = $moduleInfoMap[$insert];
                    foreach ($requires as $one) {
                        if (! in_array($one, $orderedModules)) {
                            $errors[] = T('Module %s Depends On %s', $insert, $one);
                        }
                    }
                }
                if (! empty($errors)) {
                    BizException::throws(Code::FAILED, T('Module Not Fully Installed').' '.join('; ', $errors));
                } else {
                    BizException::throws(Code::FAILED, T('Module Not Fully Installed').' '.T('Requires').'  '.SerializeUtil::jsonEncode($modules));
                }
            }
        }
        return $orderedModules;
    }

    /**
     * 获取已安装模块信息
     * @param $module
     * @return mixed|null
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function getInstalledModuleInfo($module): mixed
    {
        $modules = self::listAllInstalledModules();
        return $modules[$module] ?? null;
    }

    /**
     * 保存模块设置
     * @param $module
     * @param $config
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function saveUserInstalledModuleConfig($module, $config): void
    {
        $modules = self::listUserInstalledModules();
        if (! empty($modules[$module])) {
            if (empty($modules[$module]['config'])) {
                $modules[$module]['config'] = [];
            }
            $modules[$module]['config'] = array_merge($modules[$module]['config'], $config);
        }
        self::saveUserInstalledModules($modules);
    }

    /**
     * 保存模块至系统配置
     * @param $module
     * @param $config
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function saveSystemOverwriteModuleConfig($module, $config): void
    {
        $current = bingostart_config()->getArray(self::MODULE_SYSTEM_OVERWRITE_CONFIG);
        $current[$module] = $config;
        bingostart_config()->setArray(self::MODULE_SYSTEM_OVERWRITE_CONFIG, $current);
    }

    /**
     * 获取模块配置信息
     * @param $module string 模块名
     * @param $key string 配置项
     * @param $default mixed|null 默认值
     * @return mixed|null
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function getModuleConfig(string $module, string $key, mixed $default = null): mixed
    {
        $moduleInfo = self::getInstalledModuleInfo($module);
        if (isset($moduleInfo['config'][$key])) {
            $v = $moduleInfo['config'][$key];
            if (true === $default || false === $default) {
                return boolval($v);
            }
            if (is_int($default)) {
                return intval($v);
            }
            if (is_array($default)) {
                if (is_string($v)) {
                    $v = @json_decode($v, true);
                }
                if (null === $v) {
                    return $default;
                }
                return $v;
            }
            return $moduleInfo['config'][$key];
        }
        return $default;
    }

    /**
     * 动态重载
     */
    public static function hotReloadSystemConfig(): void
    {
        $configSystem = config('module.system', []);
        $file = base_path('config/module.php');
        if (file_exists($file)) {
            if (function_exists('opcache_invalidate')) {
                opcache_invalidate($file);
            }
            $configModuleContent = (include $file);
            $configSystem = array_merge($configSystem, $configModuleContent['system']);
            config([
                'module.system' => $configSystem,
            ]);
        }
        self::listAllInstalledModules(true);
    }

    /**
     * 动态调用模块ModuleServiceProvider的方法
     *
     * @param $module
     * @param $method
     * @param array $args
     */
    public static function callHook($module, $method, array $args = []): void
    {
        $cls = '\\Modules\\'.$module.'\\Core\\ModuleHook';
        if (class_exists($cls)) {
            $hook = app($cls);
            if (method_exists($hook, $method)) {
                call_user_func_array([$hook, $method], $args);
            }
        }
    }

    /**
     * 获取
     * @return string
     */
    public static function getEnv(): string
    {
        return 'laravel11';
    }

}
