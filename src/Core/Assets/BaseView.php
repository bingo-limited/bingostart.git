<?php

namespace Bingo\Core\Assets;

use Illuminate\Support\Facades\File;

class BaseView
{
    public const EVENT_BEGIN_PAGE = 'beginPage';
    public const EVENT_END_PAGE = 'endPage';
    public const EVENT_BEFORE_RENDER = 'beforeRender';
    public const EVENT_AFTER_RENDER = 'afterRender';

    protected $context;
    protected array $params = [];
    protected mixed $theme;
    protected array $blocks = [];
    protected array $cacheStack = [];
    protected array $dynamicPlaceholders = [];
    private array $_viewFiles = [];

    public function __construct($theme = null)
    {
        $this->theme = $theme;
    }

    public function render($view, $params = [], $context = null): false|string
    {
        $viewFile = $this->findViewFile($view, $context);
        return $this->renderFile($viewFile, $params, $context);
    }

    protected function findViewFile($view, $context = null)
    {
        if (view()->exists($view)) {
            return view($view)->getPath();
        }

        throw new \InvalidArgumentException("View file does not exist: $view");
    }

    public function renderFile($viewFile, $params = [], $context = null): false|string
    {
        $output = "";
        $viewFile = File::exists($viewFile) ? $viewFile : resource_path("views/$viewFile.blade.php");

        if (! File::exists($viewFile)) {
            throw new \InvalidArgumentException("The view file does not exist: $viewFile");
        }

        $oldContext = $this->context;
        if ($context !== null) {
            $this->context = $context;
        }
        $this->_viewFiles[] = $viewFile;

        if ($this->beforeRender($viewFile, $params)) {
            $output = $this->renderPhpFile($viewFile, $params);
            $this->afterRender($viewFile, $params, $output);
        }

        array_pop($this->_viewFiles);
        $this->context = $oldContext;

        return $output;
    }

    public function getViewFile()
    {
        return empty($this->_viewFiles) ? false : end($this->_viewFiles);
    }

    protected function beforeRender($viewFile, $params): bool
    {
        // Trigger the before render event
        event(self::EVENT_BEFORE_RENDER, [$viewFile, $params]);

        return true;
    }

    protected function afterRender($viewFile, $params, &$output): void
    {
        // Trigger the after render event
        event(self::EVENT_AFTER_RENDER, [$viewFile, $params, &$output]);
    }

    public function renderPhpFile($viewFile, $params = []): false|string
    {
        ob_start();
        extract($params, EXTR_OVERWRITE);
        include $viewFile;
        return ob_get_clean();
    }

    public function beginPage(): void
    {
        ob_start();
        ob_implicit_flush(false);

        // Trigger the beginning page event
        event(self::EVENT_BEGIN_PAGE);
    }

    public function endPage(): void
    {
        // Trigger the end page event
        event(self::EVENT_END_PAGE);
        ob_end_flush();
    }

    protected function attributes(array $attributes): string
    {
        $result = [];
        foreach ($attributes as $key => $value) {
            $result[] = sprintf('%s="%s"', $key, htmlspecialchars((string) $value, ENT_QUOTES, 'UTF-8'));
        }
        return implode(' ', $result);
    }

    protected function isRelativeUrl(string $url): bool
    {
        return ! preg_match('/^(http|https):\/\//', $url);
    }
}
