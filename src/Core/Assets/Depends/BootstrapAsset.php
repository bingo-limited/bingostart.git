<?php

namespace Bingo\Core\Assets\Depends;

use Bingo\Core\Assets\AssetsBundle;

class BootstrapAsset extends AssetsBundle
{
    public ?string $sourcePath = '@bower/bootstrap';
    public array $css = [
        'dist/css/bootstrap.css',
    ];
}
