<?php

namespace Bingo\Core\Assets;

use Illuminate\Support\Facades\URL;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Facades\Session;

class BaseHtml
{
    public static array $voidElements = [
        'area' => 1,
        'base' => 1,
        'br' => 1,
        'col' => 1,
        'command' => 1,
        'embed' => 1,
        'hr' => 1,
        'img' => 1,
        'input' => 1,
        'keygen' => 1,
        'link' => 1,
        'meta' => 1,
        'param' => 1,
        'source' => 1,
        'track' => 1,
        'wbr' => 1,
    ];

    public static function a($text, $url = null, $options = [])
    {

        if ($url !== null) {
            $options['href'] = Url::to($url);
        }
        return static::tag('a', $text, $options);
    }

    /**
     * 生成引用外部 JavaScript 文件的 script 标签。
     *
     * @param string $url 外部 JavaScript 文件的 URL。
     * @param array $options script 标签的 HTML 属性。
     * @return HtmlString|string 生成的 script 标签。
     */
    public static function jsFile(string $url, array $options = []): HtmlString|string
    {
        $options['src'] = URL::to($url);
        if (isset($options['condition'])) {
            $condition = $options['condition'];
            unset($options['condition']);
            return self::wrapIntoCondition(self::tag('script', '', $options), $condition);
        }

        return self::tag('script', '', $options);
    }

    /**
     * 生成引用外部 CSS 文件的 link 标签。
     *
     * @param string $url 外部 CSS 文件的 URL。
     * @param array $options link 标签的 HTML 属性。
     * @return HtmlString|string 生成的 link 标签。
     */
    public static function cssFile(string $url, array $options = []): HtmlString|string
    {
        if (! isset($options['rel'])) {
            $options['rel'] = 'stylesheet';
        }
        $options['href'] = URL::to($url);

        if (isset($options['condition'])) {
            $condition = $options['condition'];
            unset($options['condition']);
            return self::wrapIntoCondition(self::tag('link', '', $options), $condition);
        } elseif (isset($options['noscript']) && $options['noscript'] === true) {
            unset($options['noscript']);
            return '<noscript>'.self::tag('link', '', $options).'</noscript>';
        }

        return self::tag('link', '', $options);
    }

    /**
     * 生成包含 CSRF 参数和令牌的 meta 标签。
     *
     * @return HtmlString 生成的 CSRF meta 标签。
     */
    public static function csrfMetaTags(): HtmlString
    {
        if (Session::has('_token')) {
            //            $csrfParam = '_token'; // Laravel 默认的 CSRF 参数名称
            $csrfParam = '_csrf'; // YII迁移 默认的 CSRF 参数名称
            $csrfToken = csrf_token(); // 获取 CSRF 令牌

            $csrfParamTag = self::tag('meta', '', ['name' => 'csrf-param', 'content' => $csrfParam]);
            $csrfTokenTag = self::tag('meta', '', ['name' => 'csrf-token', 'content' => $csrfToken]);

            return new HtmlString($csrfParamTag."\n".$csrfTokenTag."\n");
        }

        return new HtmlString('');
    }

    /**
     * 生成一个完整的 HTML 标签。
     *
     * @param bool|string|null $name 标签名称。如果 $name 为 `null` 或 `false`，则内容将被渲染而不包含任何标签。
     * @param string $content 要包含在开始和结束标签之间的内容。
     * @param array $options HTML 标签的属性（名称-值对）。
     * @return HtmlString 生成的 HTML 标签。
     */
    public static function tag(bool|string|null $name, string $content = '', array $options = []): HtmlString
    {
        if ($name === null || $name === false) {
            return new HtmlString($content);
        }
        $html = "<$name".self::renderTagAttributes($options).'>';
        return new HtmlString(isset(static::$voidElements[strtolower($name)]) ? $html : "$html$content</$name>");
    }

    /**
     * 从数组生成 HTML 属性字符串。
     *
     * @param array $attributes HTML 属性。
     * @return string 生成的 HTML 属性字符串。
     */
    private static function renderTagAttributes(array $attributes): string
    {
        $html = '';

        foreach ($attributes as $name => $value) {
            if ($value !== null) {
                $html .= ' '.$name.'="'.e($value).'"';
            }
        }

        return $html;
    }

    /**
     * 将给定内容包装在条件注释中（针对 IE）。
     *
     * @param string $content 原始 HTML 内容。
     * @param string $condition 条件字符串。
     * @return string 生成的 HTML。
     */
    private static function wrapIntoCondition(string $content, string $condition): string
    {
        if (str_contains($condition, '!IE')) {
            return "<!--[if $condition]><!-->\n".$content."\n<!--<![endif]-->";
        }

        return "<!--[if $condition]>\n".$content."\n<![endif]-->";
    }

    /**
     * 将内容进行 HTML 实体编码。
     *
     * @param string $content 要编码的内容。
     * @param bool $doubleEncode 是否双重编码。
     * @return string 编码后的内容。
     */
    public static function encode(string $content, bool $doubleEncode = true): string
    {
        return htmlspecialchars($content, ENT_QUOTES | ENT_SUBSTITUTE, 'UTF-8', $doubleEncode);
    }
}
