<?php

namespace Bingo\Core\Assets;

use Illuminate\Support\Facades\File;
use InvalidArgumentException;

class AssetsManager
{
    /**
     * @var array 资源包配置列表
     */
    public array $bundles = [];

    /**
     * @var string 发布资源文件的根目录
     */
    public string $basePath = 'public/assets';

    /**
     * @var string 发布资源文件的基本URL
     */
    public string $baseUrl = 'assets';

    /**
     * @var array 从源资源文件到目标资源文件的映射
     */
    public array $assetMap = [];

    /**
     * @var bool 是否使用符号链接发布资源文件
     */
    public bool $linkAssets = false;

    /**
     * @var int|null 设置新发布的资源文件的权限
     */
    public ?int $fileMode;

    /**
     * @var int 设置新生成的资源目录的权限
     */
    public int $dirMode = 0775;

    /**
     * @var callable|null 在复制每个子目录或文件之前调用的PHP回调
     */
    public $beforeCopy;

    /**
     * @var callable|null 在成功复制每个子目录或文件后调用的PHP回调
     */
    public $afterCopy;

    /**
     * @var bool 是否强制复制发布目录
     */
    public bool $forceCopy = false;

    /**
     * @var bool 是否在每个发布的资源URL中添加时间戳
     */
    public bool $appendTimestamp = false;

    /**
     * @var callable|null 生成资产目录的哈希回调
     */
    public $hashCallback;

    /**
     * @var array 发布的资源
     */
    private array $_published = [];

    /**
     * 构造函数，初始化配置
     *
     * @param array $config 配置数组
     */
    public function __construct(array $config = [])
    {
        $this->basePath = public_path($this->basePath);
        $this->baseUrl = asset($this->baseUrl); // 使用 Laravel 的 asset 辅助函数
        $this->applyConfig($config);
    }

    /**
     * 应用配置到类属性
     *
     * @param array $config 配置数组
     */
    protected function applyConfig(array $config): void
    {
        foreach ($config as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * 获取资源包实例
     *
     * @param string $name 资源包类名
     * @param bool $publish 是否发布资源
     * @return AssetsBundle 资源包实例
     * @throws InvalidArgumentException
     */
    public function getBundle(string $name, bool $publish = false): AssetsBundle
    {
        if (! isset($this->bundles[$name])) {
            return $this->bundles[$name] = $this->loadBundle($name, [], $publish);
        } elseif ($this->bundles[$name] instanceof AssetsBundle) {
            return $this->bundles[$name];
        } elseif (is_array($this->bundles[$name])) {
            return $this->bundles[$name] = $this->loadBundle($name, $this->bundles[$name], $publish);
        }

        throw new InvalidArgumentException("Invalid asset bundle configuration: $name");
    }

    /**
     * 加载资源包类
     *
     * @param string $name 资源包类名
     * @param array $config 资源包配置
     * @param bool $publish 是否发布资源
     * @return AssetsBundle 资源包实例
     */
    protected function loadBundle(string $name, array $config = [], bool $publish = true): AssetsBundle
    {
        $bundle = new $name($config);
        if ($publish) {
            $bundle->publish($this);
        }

        return $bundle;
    }

    /**
     * 发布文件或目录
     *
     * @param string $path 文件或目录路径
     * @param array $options 发布选项
     * @return array 发布的路径和URL
     * @throws InvalidArgumentException
     */
    public function publish(string $path, array $options = []): array
    {
        $path = realpath($path);

        if (isset($this->_published[$path])) {
            return $this->_published[$path];
        }

        if (! is_string($path) || ($src = realpath($path)) === false) {
            throw new InvalidArgumentException("The file or directory to be published does not exist: $path");
        }

        if (! is_readable($path)) {
            throw new InvalidArgumentException("The file or directory to be published is not readable: $path");
        }

        if (is_file($src)) {
            return $this->_published[$path] = $this->publishFile($src);
        }

        return $this->_published[$path] = $this->publishDirectory($src, $options);
    }

    /**
     * 发布文件
     *
     * @param string $src 文件路径
     * @return array 发布的路径和URL
     * @throws InvalidArgumentException
     */
    protected function publishFile(string $src): array
    {
        $dir = $this->hash($src);
        $fileName = basename($src);
        $dstDir = $this->basePath.DIRECTORY_SEPARATOR.$dir;
        $dstFile = $dstDir.DIRECTORY_SEPARATOR.$fileName;

        if (! is_dir($dstDir)) {
            File::makeDirectory($dstDir, $this->dirMode, true, true);
        }

        if ($this->linkAssets) {
            if (! is_file($dstFile)) {
                symlink($src, $dstFile);
            }
        } elseif (@filemtime($dstFile) < @filemtime($src)) {
            copy($src, $dstFile);
            if ($this->fileMode !== null) {
                chmod($dstFile, $this->fileMode);
            }
        }

        if ($this->appendTimestamp && ($timestamp = @filemtime($dstFile)) > 0) {
            $fileName = $fileName."?v=$timestamp";
        }

        return [$dstFile, asset($this->baseUrl."/$dir/$fileName")]; // 使用 asset 辅助函数生成 URL
    }

    /**
     * 发布目录
     *
     * @param string $src 目录路径
     * @param array $options 发布选项
     * @return array 发布的路径和URL
     */
    protected function publishDirectory(string $src, array $options): array
    {
        $dir = $this->hash($src);
        $dstDir = $this->basePath.DIRECTORY_SEPARATOR.$dir;

        if ($this->linkAssets) {
            if (! is_dir($dstDir)) {
                File::makeDirectory(dirname($dstDir), $this->dirMode, true, true);
                symlink($src, $dstDir);
            }
        } elseif (! empty($options['forceCopy']) || ($this->forceCopy && ! isset($options['forceCopy'])) || ! is_dir($dstDir)) {
            $opts = array_merge(
                $options,
                [
                    'dirMode' => $this->dirMode,
                    'fileMode' => $this->fileMode,
                    'copyEmptyDirectories' => false,
                ]
            );
            if (! isset($opts['beforeCopy'])) {
                if ($this->beforeCopy !== null) {
                    $opts['beforeCopy'] = $this->beforeCopy;
                } else {
                    $opts['beforeCopy'] = function ($from, $to) {
                        return strncmp(basename($from), '.', 1) !== 0;
                    };
                }
            }
            if (! isset($opts['afterCopy']) && $this->afterCopy !== null) {
                $opts['afterCopy'] = $this->afterCopy;
            }
            File::copyDirectory($src, $dstDir, $opts);
        }

        return [$dstDir, $this->baseUrl.'/'.$dir];
    }

    /**
     * 生成资产目录的哈希值
     *
     * @param string $path 路径
     * @return string 哈希值
     */
    protected function hash(string $path): string
    {
        if (is_callable($this->hashCallback)) {
            return call_user_func($this->hashCallback, $path);
        }
        $path = (is_file($path) ? dirname($path) : $path).filemtime($path);
        return sprintf('%x', crc32($path.app()->version().'|'.$this->linkAssets));
    }

    /**
     * 获取指定资源的实际URL
     *
     * @param AssetsBundle $bundle 资源包
     * @param string $asset 资源路径
     * @param bool|null $appendTimestamp 是否添加时间戳
     * @return string 实际URL
     */
    public function getAssetUrl(AssetsBundle $bundle, string $asset, bool $appendTimestamp = null): string
    {
        $assetUrl = $this->getActualAssetUrl($bundle, $asset);
        $assetPath = $this->getAssetPath($bundle, $asset);

        $withTimestamp = $this->appendTimestamp;
        if ($appendTimestamp !== null) {
            $withTimestamp = $appendTimestamp;
        }

        if ($withTimestamp && $assetPath && ($timestamp = @filemtime($assetPath)) > 0) {
            return "$assetUrl?v=$timestamp";
        }

        return $assetUrl;
    }

    /**
     * 获取指定资源的实际文件路径
     *
     * @param AssetsBundle $bundle 资源包
     * @param string $asset 资源路径
     * @return string|false 实际文件路径
     */
    public function getAssetPath(AssetsBundle $bundle, string $asset): false|string
    {
        if (($actualAsset = $this->resolveAsset($bundle, $asset)) !== false) {
            return self::isRelative($actualAsset) ? $this->basePath.'/'.$actualAsset : false;
        }

        return self::isRelative($asset) ? $bundle->basePath.'/'.$asset : false;
    }

    /**
     * 解析资源路径
     *
     * @param AssetsBundle $bundle 资源包
     * @param string $asset 资源路径
     * @return string|false 解析后的路径
     */
    protected function resolveAsset(AssetsBundle $bundle, string $asset): false|string
    {
        if (isset($this->assetMap[$asset])) {
            return $this->assetMap[$asset];
        }
        if ($bundle->sourcePath !== null && self::isRelative($asset)) {
            $asset = $bundle->sourcePath.'/'.$asset;
        }

        $n = mb_strlen($asset, 'UTF-8');
        foreach ($this->assetMap as $from => $to) {
            $n2 = mb_strlen($from, 'UTF-8');
            if ($n2 <= $n && substr_compare($asset, $from, $n - $n2, $n2) === 0) {
                return $to;
            }
        }

        return false;
    }

    /**
     * 获取指定资源的实际URL
     *
     * @param AssetsBundle $bundle 资源包
     * @param string $asset 资源路径
     * @return string 实际URL
     */
    public function getActualAssetUrl(AssetsBundle $bundle, string $asset): string
    {
        if (($actualAsset = $this->resolveAsset($bundle, $asset)) !== false) {
            $asset = $actualAsset;
            $baseUrl = $this->baseUrl;
        } else {
            $baseUrl = $bundle->baseUrl;
        }

        if (! self::isRelative($asset) || strncmp($asset, '/', 1) === 0) {
            return $asset;
        }

        // 使用相对路径，移除域名部分
        return ltrim("$baseUrl/$asset", '/');
    }


    /**
     * 检查 URL 是否是相对的
     *
     * @param string $url
     * @return bool
     */
    public static function isRelative(string $url): bool
    {
        return strncmp($url, '//', 2) !== 0 && ! str_contains($url, '://');
    }
}
