<?php

namespace Bingo\Core\Element\Navigation;

use Bingo\Core\Element\ElementBase;

/**
 * ItemDefinition
 *
 * @method ItemDefinition useConfig(array $config) useConfig 應用提供的配置
 * @method ItemDefinition code(string $code) 導航項目的代碼
 * @method ItemDefinition label(string $label) 導航項目的標籤
 * @method ItemDefinition url(string $url) 導航項目的 URL 地址
 * @method ItemDefinition icon(null $icon) 要顯示的圖標
 * @method ItemDefinition order(int $order) 顯示時的排序號
 * @method ItemDefinition customData(array $customData) 包含在導航項目中的自定義數據
 *
 * @package bingo\core\element
 */
class ItemDefinition extends ElementBase
{
    /**
     * 初始化此項目的默認值
     */
    protected function initDefaultValues(): void
    {
        $this->order(-1);
    }
}
