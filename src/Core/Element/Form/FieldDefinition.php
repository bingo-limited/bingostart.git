<?php

namespace Bingo\Core\Element\Form;

use Bingo\Core\Element\ElementBase;

/**
 * FieldDefinition
 *
 * @method FieldDefinition fieldName(string $name) 此欄位的 fieldName
 * @method FieldDefinition label(string $label) 此欄位的 label
 * @method FieldDefinition value(string $value) 表單欄位的值
 * @method FieldDefinition valueFrom(string $valueFrom) 用於顯示值的模型屬性 valueFrom
 * @method FieldDefinition defaults(string $defaults) defaults 為支持的欄位指定默認值
 * @method FieldDefinition defaultFrom(string $defaultFrom) 用於默認值的模型屬性 defaultFrom
 * @method FieldDefinition type(string $type) 顯示模式的類型，例如：text, textarea
 * @method FieldDefinition autoFocus(bool $autoFocus) autoFocus 標記欄位在加載時獲得焦點
 * @method FieldDefinition readOnly(bool $readOnly) readOnlys 指定欄位是否為只讀
 * @method FieldDefinition disabled(bool $disabled) disabled 指定欄位是否被禁用
 * @method FieldDefinition hidden(bool $hidden) hidden 定義欄位而不顯示它
 * @method FieldDefinition tab(string $tab) 此欄位所屬的標籤頁
 * @method FieldDefinition size(string $size) 欄位的大小，例如：tiny, small, large, huge, giant
 * @method FieldDefinition comment(string $comment) 表單欄位的註釋
 * @method FieldDefinition commentAbove(string $comment) 表單欄位上方的註釋
 * @method FieldDefinition commentHtml(bool $commentHtml) commentHtml 如果註釋是 HTML 格式
 * @method FieldDefinition placeholder(string $placeholder) 當未提供值時顯示的佔位符
 * @method FieldDefinition order(int $order) 顯示時的排序號
 *
 * @package bingo\core\element
 */
class FieldDefinition extends ElementBase
{
    /**
     * @var callable|null $optionsCallback
     */
    protected $optionsCallback;

    /**
     * @var array $config
     */
    public array $config = [];

    /**
     * 初始化此欄位的默認值
     */
    protected function initDefaultValues(): void
    {
        $this
            ->hidden(false)
            ->autoFocus(false)
            ->readOnly(false)
            ->disabled(false)
            ->displayAs('text')
            ->span('full')
            ->size('large')
            ->commentPosition('below')
            ->commentHtml(false)
            ->spanClass('')
            ->comment('')
            ->placeholder('')
            ->order(-1)
        ;
    }

    /**
     * 使用配置
     *
     * @param array<string, mixed> $config
     */
    public function useConfig(array $config): self
    {
        $this->config = array_merge($this->config, $config);

        // 配置默認代理到 defaults
        if (array_key_exists('default', $this->config)) {
            $this->defaults($this->config['default']);
        }

        return $this;
    }

    /**
     * 此欄位的顯示類型
     */
    public function displayAs(string $type): self
    {
        $this->type($type);

        return $this;
    }

    /**
     * 在表單上設置欄位的位置
     */
    public function span(string $value = 'full', string $spanClass = ''): self
    {
        $this->config['span'] = $value;
        $this->config['spanClass'] = $spanClass;

        return $this;
    }

    /**
     * 設置 spanClass
     */
    public function spanClass(string $spanClass): self
    {
        $this->config['spanClass'] = $spanClass;

        return $this;
    }

    /**
     * 如果已指定選項，則返回 true
     */
    public function hasOptions(): bool
    {
        if ($this->optionsCallback !== null) {
            return true;
        }

        return isset($this->config['options']) && is_array($this->config['options']);
    }

    /**
     * 獲取/設置下拉列表、單選列表和複選框列表的選項
     *
     * @param array<mixed>|callable|null $value
     * @return array<mixed>|self
     */
    public function options($value = null)
    {
        // 獲取
        if ($value === null) {
            if ($this->optionsCallback !== null) {
                return ($this->optionsCallback)();
            }

            return $this->config['options'] ?? [];
        }

        // 設置
        if (is_callable($value)) {
            $this->optionsCallback = $value;
        } else {
            $this->config['options'] = $value;
        }

        return $this;
    }

    /**
     * 如果欄位匹配提供的上下文，則返回 true
     *
     * @param string|array<string> $context
     */
    public function matchesContext($context): bool
    {
        if ($context === '*' || !isset($this->config['context'])) {
            return true;
        }

        $fieldContext = (array)$this->config['context'];
        $checkContext = (array)$context;

        return !empty(array_intersect($fieldContext, $checkContext));
    }
}