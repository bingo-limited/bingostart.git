<?php

namespace Bingo\Core\Element\Lists;

use Bingo\Core\Element\ElementBase;

/**
 * ColumnDefinition
 *
 * @method ColumnDefinition useConfig(array $config) useConfig 應用提供的配置
 * @method ColumnDefinition columnName(string $name) 此列的列名
 * @method ColumnDefinition label(string $label) 列表列的標籤
 * @method ColumnDefinition shortLabel(string $shortLabel) 用於列表標題的短標籤
 * @method ColumnDefinition type(string $type) 顯示模式的類型，例如：text, number
 * @method ColumnDefinition align(string $align) 對齊列，例如：left, right 或 center
 * @method ColumnDefinition hidden(bool $hidden) hidden 定義列而不顯示它
 * @method ColumnDefinition sortable(bool $sortable) sortable 指定此列是否可排序
 * @method ColumnDefinition searchable(bool $searchable) searchable 指定此列是否可搜索
 * @method ColumnDefinition invisible(bool $invisible) invisible 在默認列表設置中隱藏
 * @method ColumnDefinition clickable(bool $clickable) clickable 禁用點擊列時的默認點擊行為
 * @method ColumnDefinition order(int $order) 顯示時的排序號
 * @method ColumnDefinition after(string $after) after 將此列放置在另一個現有列名之後，使用顯示順序（+1）
 * @method ColumnDefinition before(string $before) before 將此列放置在另一個現有列名之前，使用顯示順序（-1）
 *
 * @package bingo\core\element
 */
class ColumnDefinition extends ElementBase
{
    /**
     * 初始化此列的默認值
     */
    protected function initDefaultValues(): void
    {
        $this
            ->displayAs('text')
            ->hidden(false)
            ->sortable()
            ->searchable(false)
            ->invisible(false)
            ->clickable()
            ->order(-1)
        ;
    }

    /**
     * 此列的顯示類型
     * @todo $config 已棄用，請參見 useConfig
     */
    public function displayAs(string $type): ColumnDefinition
    {
        $this->type = $type;

        return $this;
    }
}