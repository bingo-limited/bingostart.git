<?php

declare(strict_types=1);

namespace Bingo\Core\Permission;

use Illuminate\Support\Facades\Cache;

/**
 * 權限管理器類
 */
class PermissionManager
{
    /**
     * 緩存鍵名稱
     */
    protected const CACHE_KEY = 'all_permissions';

    /**
     * @var array<string, array<string, array>> 存儲所有權限
     */
    protected array $permissions = [];

    /**
     * 註冊模塊的權限
     *
     * @param string $module 模塊名稱
     * @param array<string, array> $permissions 權限配置數組
     * @param string $appCode 應用代碼
     */
    public function registerPermissions(string $module, array $permissions, string $appCode): void
    {
        $allPermissions = $this->getAllPermissions();
        $this->processPermissions($module, $permissions, $allPermissions, $appCode);
        $this->saveAllPermissions($allPermissions);
    }

    /**
     * 處理權限配置，包括嵌套的子權限
     *
     * @param string $module 模塊名稱
     * @param array<string, array> $permissions 權限配置數組
     * @param array<string, array<string, array>> &$allPermissions 所有權限的引用
     * @param string $appCode 應用代碼
     * @param string|null $parentMark 父權限的標識
     */
    protected function processPermissions(string $module, array $permissions, array &$allPermissions, string $appCode, ?string $parentMark = null): void
    {
        foreach ($permissions as $permission) {
            $permissionMark = $permission['permission_mark'] ?? '';
            $parentId = isset($permission['parent_id']) ? (string)$permission['parent_id'] : null; // 将 parent_id 转换为字符串

            $allPermissions[$appCode][$permissionMark] = [
                'permission_name' => $permission['permission_name'],
                'module' => $module,
                'parent_id' => $parentId,
                'route' => $permission['route'] ?? '',
//                'icon' => $permission['icon'] ?? '',
                'component' => $permission['component'] ?? '',
                'permission_mark' => $permission['permission_mark'],
                'type' => $permission['type'] ?? 1,
//                'sort' => $permission['sort'] ?? 0
            ];

            if (isset($permission['children']) && is_array($permission['children'])) {
                $this->processPermissions($module, $permission['children'], $allPermissions, $appCode, $permissionMark);
            }

            if (isset($permission['actions']) && is_array($permission['actions'])) {
                foreach ($permission['actions'] as $action) {
                    $actionMark = $action['permission_mark'];
                    $allPermissions[$appCode][$actionMark] = [
                        'permission_name' => $action['permission_name'],
                        'module' => $module,
                        'parent_id' => $permissionMark,
                        'permission_mark' => $actionMark,
                        'route' => $action['route'] ?? '',
//                        'icon' => $action['icon'] ?? '',
                        'component' => $action['component'] ?? '',
                        'type' => $action['type'] ?? 3,
//                        'sort' => $action['sort'] ?? 0
                    ];
                }
            }
        }
    }

    /**
     * 獲取所有註冊的權限
     *
     * @return array<string, array<string, array>>
     */
    public function getAllPermissions(): array
    {
        if (empty($this->permissions)) {
            $this->permissions = Cache::get(self::CACHE_KEY, []);
        }
        return $this->permissions;
    }

    /**
     * 保存所有權限到緩存
     *
     * @param array<string, array<string, array>> $permissions
     */
    protected function saveAllPermissions(array $permissions): void
    {
        Cache::forever(self::CACHE_KEY, $permissions);
        $this->permissions = $permissions;
    }

    /**
     * 檢查權限是否存在
     *
     * @param string $permissionMark 權限標識
     * @param string $appCode 應用代碼
     * @return bool
     */
    public function hasPermission(string $permissionMark, string $appCode): bool
    {
        $allPermissions = $this->getAllPermissions();
        return isset($allPermissions[$appCode][$permissionMark]);
    }

    /**
     * 獲取特定權限
     *
     * @param string $permissionMark 權限標識
     * @param string $appCode 應用代碼
     * @return array|null
     */
    public function getPermission(string $permissionMark, string $appCode): ?array
    {
        $allPermissions = $this->getAllPermissions();
        return $allPermissions[$appCode][$permissionMark] ?? null;
    }

    /**
     * 獲取權限樹結構
     *
     * @param string $appCode 應用代碼
     * @return array
     */
    public function getPermissionTree(string $appCode): array
    {
        $allPermissions = $this->getAllPermissions();
        $appPermissions = $allPermissions[$appCode] ?? [];
        $tree = [];

        foreach ($appPermissions as $permissionMark => $permission) {
            if ($permission['parent_id'] === null) {
                $tree[] = $this->buildTreeNode($permissionMark, $appPermissions);
            }
        }

        return $tree;
    }

    /**
     * 構建權限樹節點
     *
     * @param string $permissionMark
     * @param array<string, array> $permissions
     * @return array
     */
    protected function buildTreeNode(string $permissionMark, array $permissions): array
    {
        $node = $permissions[$permissionMark];
        $node['children'] = [];

        foreach ($permissions as $childMark => $childPermission) {
            if ($childPermission['parent_id'] === $permissionMark) {
                $node['children'][] = $this->buildTreeNode($childMark, $permissions);
            }
        }

        return $node;
    }
}
