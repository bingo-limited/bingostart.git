<?php

namespace Bingo\Core\Parse;

/**
 * Helper class for passing partially parsed Markdown input
 * to and from the markdown.beforeParse and markdown. Parse
 * event handlers

 */
class MarkdownData
{
    /**
     * @var string
     */
    public $text;

    public function __construct($text)
    {
        $this->text = $text;
    }
}
