<?php

namespace Bingo\Core\Parse;

use Bingo\Facade\Yaml;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Support\DeferrableProvider;

/**
 * ParseServiceProvider
 */
class ParseServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * register the service provider.
     */
    public function register(): void
    {
        $this->app->singleton('parse.markdown', function ($app) {
            return new Markdown();
        });

        $this->app->singleton('parse.yaml', function ($app) {
            return new Yaml();
        });


        $this->app->singleton('parse.ini', function ($app) {
            return new Ini();
        });
    }

    /**
     * provides the returned services.
     * @return array
     */
    public function provides(): array
    {
        return [
            'parse.markdown',
            'parse.yaml',
            'parse.twig',
            'parse.ini'
        ];
    }
}
