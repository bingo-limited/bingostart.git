<?php

declare(strict_types=1);

namespace Bingo\Core\Mail;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Bingo\Module\ModuleManager;
use InvalidArgumentException;
use Modules\Edm\Models\Template as EdmTemplate;

/**
 * MailsManager 管理邮件模板
 */
final class MailsManager
{
    /**
     * @var array 注册的邮件模板
     */
    protected array $registeredTemplates = [];

    /**
     * @var array 模板缓存
     */
    protected array $templateCache = [];

    /**
     * @var array 默认的宏变量
     */
    protected array $defaultMacros = [
        'app_name' => null,
        'client_name' => null,
        'user_email' => null
    ];

    /**
     * @var array 模板宏变量定义
     */
    protected array $templateMacros = [];

    /**
     * @var array 数据库模板缓存
     */
    protected array $dbTemplateCache = [];

    /**
     * @var string 缓存键
     */
    private const CACHE_KEY = 'mail_templates';

    /**
     * @var int 缓存时间（分钟）
     */
    private const CACHE_MINUTES = 60;

    /**
     * 创建此单例的新实例
     */
    public static function instance(): static
    {
        static $loaded = false;

        $instance = App::make('mail.template');

        // 只在第一次加载时初始化
        if (!$loaded) {
            if (empty($instance->registeredTemplates)) {
                $instance->loadRegisteredTemplates();
            }
            $loaded = true;
        }

        return $instance;
    }

    /**
     * 加载所有已注册的邮件模板并同步到数据库
     */
    public function loadRegisteredTemplates(): void
    {
        // 如果缓存中已有数据，直接使用缓存
        if ($templates = cache()->get(self::CACHE_KEY)) {
            $this->dbTemplateCache = $templates->all();
            return;
        }

        // 清空现有模板和缓存
        $this->registeredTemplates = [];
        $this->dbTemplateCache = [];

        // 加载注册的模板
        $this->loadModuleTemplates();

        // 同步到数据库
        $this->syncTemplatesWithDatabase();
    }

    /**
     * 从模块加载注册的模板
     */
    protected function loadModuleTemplates(): void
    {
        $modules = ModuleManager::listAllEnabledModules();
        foreach ($modules as $module => $value) {
            $providerClass = "Modules\\$module\\Providers\\{$module}ServiceProvider";

            if (class_exists($providerClass)) {
                /** @var \Bingo\Providers\BingoModuleServiceProvider $provider */
                $provider = App::getProvider($providerClass);
                if ($provider) {
                    try {
                        $templates = $provider->registerMailTemplates();
                        if (is_array($templates) && !empty($templates)) {
                            foreach ($templates as $code => $template) {
                                $this->registeredTemplates[$code] = array_merge($template, [
                                    'module' => $module,
                                    'code' => $code
                                ]);
                            }
                        }
                    } catch (\Exception $e) {
                        \Log::error("Failed to load templates from provider", [
                            'module' => $module,
                            'error' => $e->getMessage()
                        ]);
                    }
                }
            }
        }
    }

    /**
     * 同步模板到数据库
     */
    protected function syncTemplatesWithDatabase(): void
    {
        // 一次性获取所有模板
        $existingTemplates = EdmTemplate::whereIn('invoke', array_keys($this->registeredTemplates))
            ->get()
            ->keyBy('invoke');

        // 将所有已存在的模板加入缓存
        foreach ($existingTemplates as $code => $template) {
            $this->dbTemplateCache[$code] = $template;
        }

        $hasChanges = false;

        foreach ($this->registeredTemplates as $code => $template) {
            $dbTemplate = $existingTemplates->get($code);

            if (!$dbTemplate) {
                \Log::info("Creating new template record for: {$code}");
                // 如果数据库中不存在，创建新记录
                $this->createTemplateRecord($code, $template);
                $hasChanges = true;
            } else {
                // 如果已存在但内容为空，则更新内容
                if (empty($dbTemplate->content)) {
                    \Log::info("Updating empty content for template: {$code}");
                    $dbTemplate->update([
                        'content' => $template['content'] ?? $this->getDefaultContent($template)
                    ]);
                    // 更新缓存
                    $this->dbTemplateCache[$code] = $dbTemplate->fresh();
                    $hasChanges = true;
                }
            }
        }

        // 更新 Redis 缓存
        cache()->put(self::CACHE_KEY, collect($this->dbTemplateCache), self::CACHE_MINUTES);
    }

    /**
     * 创建数据库模板记录
     */
    protected function createTemplateRecord(string $code, array $template): EdmTemplate
    {
        $dbTemplate = EdmTemplate::create([
            'name' => $template['name'] ?? $code,
            'invoke' => $code,
            'subject' => $template['subject'] ?? '',
            'summary' => $template['description'] ?? '',
            'type' => $template['type'] ?? 1,
            'content' => $template['content'] ?? $this->getDefaultContent($template),
            'lang' => config('app.locale', 'zh_CN')
        ]);

        // 添加到对象缓存
        $this->dbTemplateCache[$code] = $dbTemplate;

        // 更新 Redis 缓存
        $templates = cache()->get(self::CACHE_KEY, collect());
        $templates->put($code, $dbTemplate);
        cache()->put(self::CACHE_KEY, $templates, self::CACHE_MINUTES);

        return $dbTemplate;
    }

    /**
     * 获取数据库模板，优先从缓存获取
     */
    protected function getDbTemplate(string $code): ?EdmTemplate
    {
        // 先从对象缓存获取
        if (isset($this->dbTemplateCache[$code])) {
            return $this->dbTemplateCache[$code];
        }

        // 从 Redis 缓存获取所有模板
        $templates = cache()->remember(self::CACHE_KEY, self::CACHE_MINUTES, function () {
            return EdmTemplate::whereIn('invoke', array_keys($this->registeredTemplates))
                ->get()
                ->keyBy('invoke');
        });

        // 如果缓存中存在该模板，保存到对象缓存并返回
        if ($templates->has($code)) {
            $this->dbTemplateCache[$code] = $templates->get($code);
            return $this->dbTemplateCache[$code];
        }

        // 如果缓存中不存在，则从数据库查询
        $dbTemplate = EdmTemplate::where('invoke', $code)->first();
        if ($dbTemplate) {
            $this->dbTemplateCache[$code] = $dbTemplate;
            // 更新缓存
            $templates->put($code, $dbTemplate);
            cache()->put(self::CACHE_KEY, $templates, self::CACHE_MINUTES);
        }

        return $dbTemplate;
    }

    /**
     * 重置模板到默认内容
     */
    public function resetTemplate(string $code): bool
    {

        $template = $this->registeredTemplates[$code] ?? null;
        if (!$template) {
            \Log::error("Template not found in registered templates", [
                'code' => $code,
                'available_templates' => array_keys($this->registeredTemplates)
            ]);
            return false;
        }

        $dbTemplate = $this->getDbTemplate($code);
        if (!$dbTemplate) {
            \Log::error("Template not found in database", [
                'code' => $code
            ]);
            return false;
        }

        try {
            $defaultContent = $this->getDefaultContent($template);
            if (empty($defaultContent)) {
                \Log::error("Failed to get default content", [
                    'code' => $code,
                    'view' => $template['view'] ?? 'not set'
                ]);
                return false;
            }

            $dbTemplate->update([
                'subject' => $template['subject'] ?? '',
                'content' => $defaultContent,
                'summary' => $template['description'] ?? ''
            ]);

            // 更新对象缓存
            $this->dbTemplateCache[$code] = $dbTemplate->fresh();

            // 刷新 Redis 缓存
            cache()->forget(self::CACHE_KEY);

            return true;
        } catch (\Exception $e) {
            \Log::error("Exception while resetting template", [
                'code' => $code,
                'error' => $e->getMessage()
            ]);
            return false;
        }
    }

    /**
     * 获取默认内容
     */
    protected function getDefaultContent(array $template): string
    {
        try {
            if (!isset($template['view'])) {
                \Log::error("View path not set in template configuration");
                return '';
            }

            $viewPath = $template['view'];

            // 尝试直接从文件系统加载视图
            $moduleName = $template['module'] ?? 'Iam';
            $fullPath = base_path('Modules/' . $moduleName . '/views/' . str_replace(['Iam::', 'emails.'], ['', 'emails/'], $viewPath) . '.blade.php');

            if (!file_exists($fullPath)) {
                \Log::error("View file not found", [
                    'path' => $fullPath
                ]);
                return '';
            }

            // 提供默认的宏变量值
            $defaultData = [
                'user_email' => 'user@example.com',
                'user_name' => 'User Name',
                'app_name' => config('app.name') ?: 'BWMS',
                'client_name' => config('client.name') ?: 'Client',
                'code' => '123456',
                'password' => 'example_password',
                'link' => 'https://example.com/reset-password',
                'title' => '邮件标题',
                'message' => '邮件内容',
                'changed_at' => now()->format('Y-m-d H:i:s'),
                'expiry_date' => now()->addDays(7)->format('Y-m-d'),
                'invitation_link' => 'https://example.com/invitation',
                'birth_date' => now()->format('Y-m-d'),
                'footer' => '如有任何问题，请随时联系我们。'
            ];

            return view()->file($fullPath)
                ->with($defaultData)
                ->render();

        } catch (\Exception $e) {
            \Log::error("Exception while getting default content", [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString(),
                'view' => $template['view'] ?? 'not set'
            ]);
            return '';
        }
    }

    /**
     * 渲染邮件模板
     *
     * @param string $code 模板调用代码，例如: 'edm.welcome'
     * @param array $data 模板变量数据，支持以下变量:
     *                   - user_email: 用户邮箱
     *                   - app_name: 应用名称
     *                   - client_name: 客户名称
     *                   - code: 验证码（用于验证码类模板）
     *                   - title: 邮件标题（用于密码相关模板）
     *                   - message: 邮件内容（用于密码相关模板）
     *                   - link: 重置链接（用于密码重置模板）
     *
     * @throws InvalidArgumentException 当模板不存在时抛出异常
     * @return string 渲染后的 HTML 内容
     *
     * @example
     * ```php
     * // 发送欢迎邮件
     * $html = $mailsManager->render('edm.welcome', [
     *     'user_email' => 'user@example.com',
     *     'app_name' => 'BWMS',
     *     'client_name' => '示例应用'
     * ]);
     *
     * // 发送验证码
     * $html = $mailsManager->render('edm.register_code', [
     *     'user_email' => 'user@example.com',
     *     'code' => '123456'
     * ]);
     *
     * // 发送密码重置链接
     * $html = $mailsManager->render('edm.reset_password_link', [
     *     'user_email' => 'user@example.com',
     *     'title' => '重置密码',
     *     'message' => '您正在重置密码',
     *     'link' => 'https://example.com/reset-password?token=xxx'
     * ]);
     * ```
     */
    public function render(string $code, array $data = []): string
    {
        // 优先从缓存获取模板
        $dbTemplate = $this->getDbTemplate($code);
        if (!$dbTemplate) {
            // 如果数据库中不存在，先加载注册的模板
            $template = $this->getTemplate($code);
            if (!$template) {
                throw new InvalidArgumentException("未找到邮件模板: {$code}");
            }

            // 同步到数据库并缓存
            $dbTemplate = $this->createTemplateRecord($code, $template);
            $this->dbTemplateCache[$code] = $dbTemplate;
        }

        // 使用数据库中的内容，处理宏变量替换，传入模板代码
        return $this->parseMacros($dbTemplate->content, $data, $code);
    }

    /**
     * 返回已注册模板的列表
     */
    public function listRegisteredTemplates(): array
    {
        // 如果已有注册模板，直接返回
        if (!empty($this->registeredTemplates)) {
            return $this->registeredTemplates;
        }

        // 如果缓存中已有数据，从缓存加载模板
        if ($templates = cache()->get(self::CACHE_KEY)) {
            $this->dbTemplateCache = $templates->all();
            // 从缓存的数据库模板中重建注册模板列表
            foreach ($this->dbTemplateCache as $code => $template) {
                $this->registeredTemplates[$code] = [
                    'name' => $template->name,
                    'code' => $template->invoke,
                    'subject' => $template->subject,
                    'description' => $template->summary,
                    'content' => $template->content,
                    'type' => $template->type,
                    'module' => explode('.', $template->invoke)[0] ?? 'Iam'
                ];
            }
            return $this->registeredTemplates;
        }

        // 如果没有缓存，则加载并同步
        $this->loadRegisteredTemplates();
        return $this->registeredTemplates;
    }

    /**
     * 获取模板的视图路径
     */
    public function getViewPathForTemplate(string $code): ?string
    {
        $template = $this->getTemplate($code);
        return $template['view'] ?? null;
    }

    /**
     * 注册邮件模板
     */
    public function registerMailTemplates(string $module, array $templates): void
    {
        try {
            foreach ($templates as $code => $template) {

                $template['module'] = $module;
                $template['code'] = $code;

                // 保存模板的宏变量定义
                if (isset($template['macros'])) {
                    $this->templateMacros[$code] = $template['macros'];
                }

                // 尝试加载视图内容
                if (isset($template['view']) && !isset($template['content'])) {
                    try {
                        $template['content'] = $this->getDefaultContent($template);
                    } catch (\Exception $e) {
                        \Log::error("Failed to load content for template: {$code}", [
                            'error' => $e->getMessage()
                        ]);
                    }
                }

                $this->registeredTemplates[$code] = $template;
            }
        } catch (\Exception $e) {
            \Log::error("Failed to register templates for module: {$module}", [
                'error' => $e->getMessage()
            ]);
        }
    }

    /**
     * 设置默认宏变量
     *
     * @param array $macros
     * @return void
     */
    public function setDefaultMacros(array $macros): void
    {
        $this->defaultMacros = array_merge($this->defaultMacros, $macros);
    }

    /**
     * 处理内容中的宏
     */
    protected function parseMacros(string $content, array $data = [], ?string $code = null): string
    {
        // 获取模板特定的宏变量，但排除默认宏
        $templateMacros = [];
        if ($code && isset($this->templateMacros[$code])) {
            $templateMacros = array_diff_key(
                $this->templateMacros[$code],
                $this->defaultMacros
            );
        }

        // 合并宏变量，优先级：用户数据 > 模板特定宏 > 默认宏
        $macros = array_merge(
            $this->defaultMacros,
            $templateMacros,
            $data
        );

        return preg_replace_callback('/\{\{(.*?)\}\}/', function($matches) use ($macros) {
            $key = trim($matches[1]);
            return $macros[$key] ?? $matches[0];
        }, $content);
    }

    /**
     * 获取指定代码的邮件模板
     */
    public function getTemplate(string $code): ?array
    {
        if (isset($this->templateCache[$code])) {
            return $this->templateCache[$code];
        }

        $templates = $this->listRegisteredTemplates();

        if (isset($templates[$code])) {
            $this->templateCache[$code] = $templates[$code];
            return $this->templateCache[$code];
        }

        return null;
    }

    /**
     * 获取所有注册的模板
     *
     * @return array<string, array>
     */
    protected function getAllTemplates(): array
    {
        return $this->registeredTemplates;
    }

    /**
     * 保存所有模板
     *
     * @param array<string, array> $templates
     */
    protected function saveAllTemplates(array $templates): void
    {
        $this->registeredTemplates = $templates;

        \Log::info("Saved all templates", [
            'count' => count($templates),
            'keys' => array_keys($templates)
        ]);
    }

    /**
     * 获取模板支持的宏变量
     */
    public function getTemplateMacros(string $code): array
    {
        // 返回模板特定的宏变量，排除默认宏
        $templateMacros = $this->templateMacros[$code] ?? [];
        return array_diff_key($templateMacros, $this->defaultMacros);
    }
}
