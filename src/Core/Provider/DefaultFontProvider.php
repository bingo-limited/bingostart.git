<?php

namespace Bingo\Core\Provider;

class DefaultFontProvider extends AbstractFontProvider
{
    public function name(): string
    {
        return 'default';
    }

    public function title(): string
    {
        return '默认字体';
    }

    public function path(): string
    {
        return base_path('vendor/bingo/resources/font/AlibabaPuHuiTi-2-55-Regular.simple.ttf');
    }
}
