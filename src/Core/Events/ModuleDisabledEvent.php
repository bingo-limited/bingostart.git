<?php

namespace Bingo\Core\Events;

/**
 * 模块已启用
 * Class ModuleDisabledEvent
 * @package Bingo\Core\Events
 */
class ModuleDisabledEvent
{
    public $name;
}
