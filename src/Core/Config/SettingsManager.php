<?php

namespace Bingo\Core\Config;

use Bingo\Core\Element\Navigation\ItemDefinition;
use Bingo\Module\ModuleManager;
use Exception;
use Illuminate\Support\Facades\App;
use Bingo\Core\Config\Driver\DatabaseBingoConfig;
use Psr\Container\ContainerExceptionInterface;

/**
 * SettingsManager 管理系统设置
 */
class SettingsManager
{
    /**
     * @var array 注册的设置项
     */
    protected array $items = [];

    /**
     * @var array 按类别分组的设置项
     */
    protected array $groupedItems = [];

    /**
     * @var string 活动模块
     */
    protected string $contextModule;

    /**
     * @var string 活动项代码
     */
    protected string $contextItemCode;

    /**
     * @var DatabaseBingoConfig 配置管理器
     */
    protected DatabaseBingoConfig $configManager;

    /**
     * 创建此单例的新实例
     * @return static
     */
    public static function instance(): static
    {
        return App::make('config.settings');
    }

    /**
     * 构造函数
     */
    public function __construct()
    {
        $this->configManager = App::make(DatabaseBingoConfig::class);
    }

    /**
     * 加载设置项
     * @throws ContainerExceptionInterface
     */
    protected function loadItems(): void
    {
        $modules = ModuleManager::listAllEnabledModules();
        foreach ($modules as $module => $value) {
            $providerClass = "Modules\\$module\\Providers\\{$module}ServiceProvider";
            if (class_exists($providerClass)) {
                /** @var BingoModuleServiceProvider $provider */
                $provider = App::getProvider($providerClass);
                if ($provider && method_exists($provider, 'registerSettings')) {
                    $this->registerSettingItems($module, $provider->registerSettings());
                }
            }
        }

        // 按类别分组设置项
        $catItems = [];
        foreach ($this->items as $code => $item) {
            $category = $item['category'] ?? 'Misc';
            if (!isset($catItems[$category])) {
                $catItems[$category] = [];
            }
            $catItems[$category][$code] = $item;
        }
        $this->groupedItems = $catItems;
    }

    /**
     * 返回按组分类的所有设置项集合
     * @param string|null $context
     * @return array
     */
    public function listItems(string $context = null): array
    {
        if (empty($this->items) || $this->items === null) {
            $this->loadItems();
        }

        if ($context !== null) {
            return $this->filterByContext($this->groupedItems, $context);
        }

        return $this->groupedItems;
    }

    /**
     * 根据给定的上下文过滤一组项目
     * @param array $items
     * @param string $context
     * @return array
     */
    protected function filterByContext(array $items, string $context): array
    {
        $filteredItems = [];
        foreach ($items as $categoryName => $category) {
            $filteredCategory = [];
            foreach ($category as $item) {
                $itemContext = is_array($item->context) ? $item->context : [$item->context];
                if (in_array($context, $itemContext)) {
                    $filteredCategory[] = $item;
                }
            }

            if (count($filteredCategory)) {
                $filteredItems[$categoryName] = $filteredCategory;
            }
        }

        return $filteredItems;
    }

    /**
     * registerSettingItems 註冊後端設定項目。
     * 參數是設定項的陣列。數組鍵代表
     * 設定專案程式碼，特定於插件/模組。中的每個元素
     * 陣列應該是具有以下鍵的關聯數組：
     * - label - 指定設定標籤本地化字串鍵，必要。
     * - icon - 來自 Font Awesome 圖示集合的圖示名稱，如果未提供 iconSvg，則需要此名稱。
     * - iconSvg - SVG 圖示檔案的路徑。
     * - url - 設定項目應該指向的後端相對URL。
     * - class - 設定項應該指向的後端相對URL。
     * - 權限 - 後端使用者應具有的權限數組，可選。
     * 如果使用者俱有任何指定的權限，則會顯示該項目。
     * - 順序 - 項目在設定中的位置，可選。
     * - 類別 - 用於將此項目指派給類別的字串，可選。
     * @param string $module 设置项所有者模块，格式为 Module
     * @param array $definitions 设置项定义数组
     */
    public function registerSettingItems(string $module, array $definitions): void
    {
        if (!$this->items) {
            $this->items = [];
        }

        $this->addSettingItems($module, $definitions);
    }

    /**
     * 动态添加一组设置项
     * @param string $module
     * @param array $definitions
     */
    public function addSettingItems(string $module, array $definitions): void
    {
        foreach ($definitions as $code => $definition) {
            if ($definition && is_array($definition)) {
                $this->addSettingItem($module, $code, $definition);
            }
        }
    }

    /**
     * 动态添加单个设置项
     * @param string $module
     * @param string $code
     * @param array $definition
     */
    public function addSettingItem(string $module, string $code, array $definition): void
    {
        $itemKey = $this->makeItemKey($module, $code);

        if (isset($this->items[$itemKey])) {
            $definition = array_merge((array)$this->items[$itemKey], $definition);
        }

        $item = array_merge($definition, [
            'code' => $code,
            'module' => $module
        ]);
        $this->items[$itemKey] = $this->defineSettingsMenuItem($item);
    }

    /**
     * 定义设置菜单项
     * @param array $config
     * @return SettingsMenuItem|ItemDefinition
     */
    protected function defineSettingsMenuItem(array $config): ItemDefinition|SettingsMenuItem
    {
        return (new SettingsMenuItem())->useConfig($config);
    }

    /**
     * 使用模块和代码删除设置项
     * @param string $module
     * @param string $code
     * @throws Exception
     */
    public function removeSettingItem(string $module, string $code): void
    {
        if (!$this->items) {
            throw new Exception('在加载项之前无法删除设置项。');
        }

        $itemKey = $this->makeItemKey($module, $code);
        unset($this->items[$itemKey]);

        if ($this->groupedItems) {
            foreach ($this->groupedItems as $category => $items) {
                if (isset($items[$itemKey])) {
                    unset($this->groupedItems[$category][$itemKey]);
                }
            }
        }
    }

    /**
     * 设置导航上下文
     * @param string $module
     * @param string $code
     */
    public static function setContext(string $module, string $code): void
    {
        $instance = self::instance();
        $instance->contextModule = strtolower($module);
        $instance->contextItemCode = strtolower($code);
    }

    /**
     * 返回有关当前设置上下文的信息
     * @return object
     */
    public function getContext(): object
    {
        return (object)[
            'itemCode' => $this->contextItemCode,
            'module' => $this->contextModule
        ];
    }

    /**
     * 使用模块和代码查找设置项对象
     * @param string $module
     * @param string $code
     * @return mixed
     */
    public function findSettingItem(string $module, string $code): mixed
    {
        $module = strtolower($module);
        $code = strtolower($code);

        foreach ($this->items as $item) {
            if (strtolower($item['module']) === $module && strtolower($item['code']) === $code) {
                return $item;
            }
        }

        return false;
    }

    /**
     * 过滤用户缺乏权限的设置项
     * @param mixed $user
     * @param array $items
     * @return array
     */
    protected function filterItemPermissions(mixed $user, array $items): array
    {
        if (!$user) {
            return $items;
        }

        return array_filter($items, fn($item) => !$item['permissions'] || $user->hasAnyAccess($item['permissions']));
    }

    /**
     * 创建项目的唯一键
     * @param string $module
     * @param string $code
     * @return string
     */
    protected function makeItemKey(string $module, string $code): string
    {
        return strtoupper($module) . '.' . strtoupper($code);
    }

    /**
     * 保存设置到数据库
     * @param string $module
     * @param array $settings
     * @return void
     */
    public function saveSettingsToDatabase(string $module, array $settings): void
    {
        $this->configManager->set($module, json_encode($settings));
    }

    /**
     * 从数据库加载设置
     * @param string $module
     * @return array
     */
    public function loadSettingsFromDatabase(string $module): array
    {
        $settings = $this->configManager->get($module);
        return json_decode($settings, true) ?? [];
    }
}
