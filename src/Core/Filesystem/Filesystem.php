<?php

namespace Bingo\Core\Filesystem;

use Event;
use ReflectionClass;
use FilesystemIterator;
use Illuminate\Filesystem\Filesystem as FilesystemBase;
use ReflectionException;

/**
 * 檔案系統輔助類
 */
class Filesystem extends FilesystemBase
{
    /**
     * @var ?string 預設檔案權限遮罩，以字串形式表示（"755"）。
     */
    public ?string $filePermissions = null;

    /**
     * @var ?string 預設資料夾權限遮罩，以字串形式表示（"755"）。
     */
    public ?string $folderPermissions = null;

    /**
     * @var array 已知的路徑符號及其前綴。
     */
    public array $pathSymbols = [];

    /**
     * @var array|null 符號連結根目錄的快取。
     */
    protected ?array $symlinkRootCache = null;

    /**
     * anyname 提取不帶副檔名的路徑和檔名
     * @param string $path
     * @return string
     */
    public function anyname(string $path): string
    {
        return str_contains(basename($path), '.') ? substr($path, 0, strrpos($path, '.')) : $path;
    }

    /**
     * isDirectoryEmpty 判斷給定路徑是否不包含任何檔案
     * @param string $directory
     * @return ?bool
     */
    public function isDirectoryEmpty(string $directory): ?bool
    {
        if (! is_readable($directory)) {
            return null;
        }

        $handle = opendir($directory);
        while (false !== ($entry = readdir($handle))) {
            if ($entry !== '.' && $entry !== '..') {
                closedir($handle);
                return false;
            }
        }

        closedir($handle);
        return true;
    }

    /**
     * sizeToString 將檔案大小（位元組）轉換為人類可讀格式
     * @param int $bytes
     * @return string
     */
    public function sizeToString(int $bytes): string
    {
        if ($bytes >= 1073741824) {
            return number_format($bytes / 1073741824, 2).' GB';
        }

        if ($bytes >= 1048576) {
            return number_format($bytes / 1048576, 2).' MB';
        }

        if ($bytes >= 1024) {
            return number_format($bytes / 1024, 2).' KB';
        }

        if ($bytes > 1) {
            return $bytes.' bytes';
        }

        if ($bytes === 1) {
            return $bytes.' byte';
        }

        return '0 bytes';
    }

    /**
     * localToPublic 從絕對路徑返回公共檔案路徑
     * 例如：/home/mysite/public_html/welcome -> /welcome
     * @param string $path 絕對路徑
     * @return ?string
     */
    public function localToPublic(string $path): ?string
    {
        /**
         * @event filesystem.localToPublic
         * 允許在非標準安裝上自定義本地路徑到公共路徑的轉換邏輯。
         *
         * 使用示例
         *
         *     Event::listen('filesystem.localToPublic', function ($path) {
         *         return '/custom/public/path';
         *     });
         */
        if (($event = Event::fire('filesystem.localToPublic', [$path], true)) !== null) {
            return $event;
        }

        // 檢查真實路徑
        $basePath = base_path();
        if (str_starts_with($path, $basePath)) {
            return str_replace("\\", "/", substr($path, strlen($basePath)));
        }

        // 檢查第一級符號連結
        foreach ($this->getRootSymlinks() as $dir) {
            $resolvedDir = readlink($dir);
            if (str_starts_with($path, $resolvedDir)) {
                $relativePath = substr($path, strlen($resolvedDir));
                return str_replace("\\", "/", substr($dir, strlen($basePath)).$relativePath);
            }
        }

        return null;
    }

    /**
     * getRootSymlinks 返回公共目錄中任何未解析的符號連結
     * @return array
     */
    protected function getRootSymlinks(): array
    {
        if ($this->symlinkRootCache === null) {
            $symDirs = [];

            foreach ($this->directories(base_path()) as $dir) {
                if (is_link($dir)) {
                    $symDirs[] = $dir;
                }
            }

            $this->symlinkRootCache = $symDirs;
        }

        return $this->symlinkRootCache;
    }

    /**
     * isLocalPath 如果指定路徑在應用程式路徑內，則返回 true。
     * realpath 在檢查位置之前解析提供的路徑，如果您需要檢查潛在的不存在路徑是否在應用程式路徑內，則設置為 false。
     * @param string $path
     * @param bool $realpath
     * @return bool
     */
    public function isLocalPath(string $path, bool $realpath = true): bool
    {
        $base = base_path();

        if ($realpath) {
            $path = realpath($path);
        }

        return ! ($path === false || strncmp($path, $base, strlen($base)) !== 0);
    }

    /**
     * fromClass 查找類的路徑
     * @param mixed $className 類名或物件
     * @return string 檔案路徑
     * @throws ReflectionException
     */
    public function fromClass(mixed $className): string
    {
        $reflector = new ReflectionClass($className);
        return $reflector->getFileName();
    }

    /**
     * existsInsensitive 判斷檔案是否存在（不區分大小寫）
     * 僅支援檔案，不支援目錄。返回區分大小寫的路徑或 false。
     * @param string $path
     * @return string|bool
     */
    public function existsInsensitive(string $path): string|bool
    {
        if ($this->exists($path)) {
            return $path;
        }

        $directoryName = dirname($path);
        $pathLower = strtolower($path);

        if (! $files = $this->glob($directoryName.'/*', GLOB_NOSORT)) {
            return false;
        }

        foreach ($files as $file) {
            if (strtolower($file) === $pathLower) {
                return $file;
            }
        }

        return false;
    }

    /**
     * normalizePath 返回提供路徑的規範化版本，用於在 Windows 和 Unix 系統中組合使用。
     * @param string $path
     * @return string
     */
    public function normalizePath(string $path): string
    {
        return str_replace('\\', '/', $path);
    }

    /**
     * nicePath 從本地路徑中移除基本路徑，並返回一個相對較好的路徑，適合安全分享。
     * @param string $path
     * @return string
     */
    public function nicePath(string $path): string
    {
        return $this->normalizePath(str_replace([
            base_path(),
            $this->normalizePath(base_path())
        ], '~', $path));
    }

    /**
     * symbolizePath 使用路徑符號轉換路徑。如果未使用符號且未指定默認值，則返回原始路徑。
     * @param string $path
     * @param bool|string $default
     * @return string
     */
    public function symbolizePath(string $path, bool|string $default = false): string
    {
        if (! $firstChar = $this->isPathSymbol($path)) {
            return $default === false ? $path : $default;
        }

        $_path = substr($path, 1);
        return $this->pathSymbols[$firstChar].$_path;
    }

    /**
     * isPathSymbol 如果路徑使用符號，則返回該符號，否則返回 false
     * @param string $path
     * @return bool|string
     */
    public function isPathSymbol(string $path): bool|string
    {
        $firstChar = substr($path, 0, 1);
        if (isset($this->pathSymbols[$firstChar])) {
            return $firstChar;
        }

        return false;
    }

    /**
     * put 寫入檔案內容
     * @param string $path
     * @param string $contents
     * @param bool $lock
     * @return bool
     */
    public function put($path, $contents, $lock = false): bool
    {
        $result = parent::put($path, $contents, $lock);
        $this->chmod($path);
        return $result;
    }

    /**
     * copy 將檔案複製到新位置
     * @param  string  $path
     * @param  string  $target
     * @return bool
     */
    public function copy($path, $target): bool
    {
        $result = parent::copy($path, $target);
        $this->chmod($target);
        return $result;
    }

    /**
     * getSafe 讀取檔案內容的第一部分
     * @param string $path
     * @param float $limitKbs
     * @return false|string
     */
    public function getSafe(string $path, float $limitKbs = 1): false|string
    {
        $limit = $limitKbs * 4096;

        $parser = fopen($path, 'r');

        return fread($parser, $limit);
    }

    /**
     * makeDirectory 創建目錄
     * @param  string  $path
     * @param  int     $mode
     * @param  bool    $recursive
     * @param  bool    $force
     * @return bool
     */
    public function makeDirectory($path, $mode = 0755, $recursive = false, $force = false): bool
    {
        if ($mask = $this->getFolderPermissions()) {
            $mode = $mask;
        }

        // 查找綠葉
        if ($recursive && $mask) {
            $chmodPath = $path;
            while (true) {
                $basePath = dirname($chmodPath);
                if ($chmodPath === $basePath) {
                    break;
                }
                if ($this->isDirectory($basePath)) {
                    break;
                }
                $chmodPath = $basePath;
            }
        } else {
            $chmodPath = $path;
        }

        // 創建目錄
        $result = parent::makeDirectory($path, $mode, $recursive, $force);

        // 應用權限
        if ($mask) {
            $this->chmod($chmodPath, $mask);

            if ($recursive) {
                $this->chmodRecursive($chmodPath, null, $mask);
            }
        }

        return $result;
    }

    /**
     * chmod 修改檔案/資料夾權限
     * @param  string $path
     * @param  int|null $mode
     * @return bool
     */
    public function chmod($path, $mode = null): bool
    {
        if ($mode === null) {
            $mode = $this->isDirectory($path)
                ? $this->getFolderPermissions()
                : $this->getFilePermissions();
        }

        if ($mode === null) {
            return false;
        }

        return @chmod($path, $mode) || false;
    }

    /**
     * chmodRecursive 遞迴修改檔案/資料夾權限
     * @param string $path
     * @param int|null $fileMask
     * @param int|null $directoryMask
     * @return void
     */
    public function chmodRecursive($path, ?int $fileMask = null, ?int $directoryMask = null): void
    {
        if (! $fileMask) {
            $fileMask = $this->getFilePermissions();
        }

        if (! $directoryMask) {
            $directoryMask = $this->getFolderPermissions() ?: $fileMask;
        }

        if (! $fileMask) {
            return;
        }

        if (! $this->isDirectory($path)) {
            $this->chmod($path, $fileMask);
            return;
        }

        $items = new FilesystemIterator($path, FilesystemIterator::SKIP_DOTS);
        foreach ($items as $item) {
            if ($item->isDir()) {
                $_path = $item->getPathname();
                $this->chmod($_path, $directoryMask);
                $this->chmodRecursive($_path, $fileMask, $directoryMask);
            } else {
                $this->chmod($item->getPathname(), $fileMask);
            }
        }
    }

    /**
     * getFilePermissions 返回要使用的默認檔案權限遮罩
     * @return int|null 以八進制（0755）表示的權限遮罩或 null
     */
    public function getFilePermissions(): ?int
    {
        return $this->filePermissions
            ? octdec($this->filePermissions)
            : null;
    }

    /**
     * getFolderPermissions 返回要使用的默認資料夾權限遮罩
     * @return int|null 以八進制（0755）表示的權限遮罩或 null
     */
    public function getFolderPermissions(): ?int
    {
        return $this->folderPermissions
            ? octdec($this->folderPermissions)
            : null;
    }

    /**
     * fileNameMatch 將檔名與模式匹配
     * @param array|string $fileName
     * @param string $pattern
     * @return bool
     */
    public function fileNameMatch(array|string $fileName, string $pattern): bool
    {
        if ($pattern === $fileName) {
            return true;
        }

        $regex = strtr(preg_quote($pattern, '#'), ['\*' => '.*', '\?' => '.']);

        return (bool) preg_match('#^'.$regex.'$#i', $fileName);
    }

    /**
     * lastModifiedRecursive 檢查整個目錄並返回最新檔案的修改時間。
     * @param string $path
     * @return int
     */
    public function lastModifiedRecursive(string $path): int
    {
        $mtime = 0;

        foreach ($this->allFiles($path) as $file) {
            $mtime = max($mtime, $this->lastModified($file->getPathname()));
        }

        return $mtime;
    }

    /**
     * searchDirectory 定位檔案並返回其相對路徑
     * 例如：在目錄 /home/mysite 中搜索檔案 index.php 可能會找到
     * /home/mysite/public_html/welcome/index.php，並返回 public_html/welcome
     * @param string $file 例如：index.php
     * @param string $directory 例如：/home/mysite
     * @param string $rootDir
     * @return string|null 例如：public_html/welcome
     */
    public function searchDirectory(string $file, string $directory, string $rootDir = ''): ?string
    {
        $files = $this->files($directory);
        $directories = $this->directories($directory);

        foreach ($files as $directoryFile) {
            if ($directoryFile->getFileName() === $file) {
                return $rootDir;
            }
        }

        foreach ($directories as $subdirectory) {
            $relativePath = strlen($rootDir)
                ? $rootDir.'/'.basename($subdirectory)
                : basename($subdirectory);

            $result = $this->searchDirectory($file, $subdirectory, $relativePath);
            if ($result !== null) {
                return $result;
            }
        }

        return null;
    }
}
