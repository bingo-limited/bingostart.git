<?php

namespace Bingo\Core\Resize;

use Exception;

/**
 * ResizeBuilder builds resizes on demand
 *
 */
class ResizeBuilder
{
    /**
     * Open a file and return a Resizer
     * @param $filename
     * @return Resizer
     * @throws Exception
     */
    public function open($filename): Resizer
    {
        return new Resizer($filename);
    }
}
