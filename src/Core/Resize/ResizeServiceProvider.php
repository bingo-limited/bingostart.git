<?php

namespace Bingo\Core\Resize;

use Bingo\Providers\ServiceProvider;
use Illuminate\Contracts\Support\DeferrableProvider;

/**
 * ResizeServiceProvider
 */
class ResizeServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * register the service provider.
     */
    public function register(): void
    {
        $this->app->singleton('resizer', function () {
            return new ResizeBuilder();
        });
    }

    /**
     * provides the returned services.
     * @return array
     */
    public function provides(): array
    {
        return ['resizer'];
    }
}
