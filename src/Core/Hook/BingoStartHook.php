<?php

namespace Bingo\Core\Hook;

use Closure;
use InvalidArgumentException;

class BingoStartHook
{
    private static array $listeners = [];

    /**
     * subscribe hook
     * @param $name array|string PageHeadAppend,DialogPageHeadAppend
     * @param $callable Closure
     */
    public static function subscribe(array|string $name, Closure $callable): void
    {
        if (! is_array($name)) {
            $name = [$name];
        }
        foreach ($name as $item) {
            if (! isset(self::$listeners[$item])) {
                self::$listeners[$item] = [];
            }
            self::$listeners[$item][] = $callable;
        }
    }

    /**
     * get hook listeners
     * @param string $name
     * @return array|mixed
     */
    public static function get(string $name = ''): mixed
    {
        if (empty($name)) {
            return self::$listeners;
        }
        return array_key_exists($name, self::$listeners) ? self::$listeners[$name] : [];
    }

    /**
     * fire one hook and get array result
     * @param $name
     * @param null $param
     * @param null $extra
     * @return array
     */
    public static function fire($name, &$param = null, $extra = null): array
    {
        $results = [];
        foreach (static::get($name) as $key => $callable) {
            $results[$key] = self::call($callable, $name, $param, $extra);
        }
        return $results;
    }

    /**
     * fire one hook and get string result
     * @param $name
     * @param null $param
     * @param null $extra
     * @return string
     */
    public static function fireInView($name, &$param = null, $extra = null): string
    {
        return join('', self::fire($name, $param, $extra));
    }

    private static function call($callable, $name = '', &$param = null, $extra = null)
    {
        if ($callable instanceof Closure) {
            return call_user_func_array($callable, [& $param, $extra]);
        } elseif (is_array($callable)) {
            list($callable, $method) = $callable;
            return call_user_func_array([&$callable, $method], [& $param, $extra]);
        } elseif (is_object($callable)) {
            $method = "on$name";
            return call_user_func_array([&$callable, $method], [& $param, $extra]);
        } elseif (str_contains($callable, '::')) {
            $callableParts = explode('::', $callable, 2);
            if (count($callableParts) === 2 && class_exists($callableParts[0]) && method_exists($callableParts[0], $callableParts[1])) {
                return call_user_func_array($callable, [& $param, $extra]);
            } else {
                throw new InvalidArgumentException("Invalid callable format: '$callable'");
            }
        } else {
            throw new InvalidArgumentException('Invalid callable type');
        }
    }
}
