<?php

namespace Bingo\Core\Dao;

use Illuminate\Support\Facades\DB;

class DModelUtil
{
    /**
     * @param $conn
     * @param $model
     * @return DynamicModel
     */
    public static function model($conn, $model): DynamicModel
    {
        $m = new DynamicModel();
        $m->setTable($model);
        $m->setConnection($conn);
        return $m;
    }

    public static function get($conn, $model, $where, $fields = ['*'])
    {
        if (is_string($where) || is_numeric($where)) {
            $where = ['id' => $where];
        }
        $m = self::model($conn, $model)->where($where)->first($fields);
        if (empty($m)) {
            return [];
        }
        return $m->toArray();
    }

    public static function insert($conn, $model, $data): array
    {
        $m = self::model($conn, $model);
        foreach ($data as $k => $v) {
            $m->$k = $v;
        }
        $m->save();
        return $m->toArray();
    }

    public static function insertAll($conn, $model, $dataArr, $updateTimestamp = true): void
    {
        if ($updateTimestamp) {
            foreach ($dataArr as $i => $data) {
                $dataArr[$i]['created_at'] = time();
                $dataArr[$i]['updated_at'] = time();
            }
        }
        DB::connection($conn)->table($model)->insert($dataArr);
    }

    public static function delete($conn, $model, $where)
    {
        if (is_string($where) || is_numeric($where)) {
            $where = ['id' => $where];
        }
        return self::model($conn, $model)->where($where)->delete();
    }

    public static function update($conn, $model, $where, $data)
    {
        if (empty($where)) {
            return [];
        }
        if (is_string($where) || is_numeric($where)) {
            $where = ['id' => $where];
        }
        if (empty($data)) {
            return [];
        }
        return self::model($conn, $model)->where($where)->update($data);
    }
}
