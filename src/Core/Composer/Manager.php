<?php

namespace Bingo\Core\Composer;

use Config;
use Composer\Factory;
use Composer\Composer;
use Composer\Installer;
use Composer\Json\JsonFile;
use Composer\Semver\VersionParser;
use Composer\Config\JsonConfigSource;
use Composer\DependencyResolver\Request;
use Exception;
use Throwable;

/**
 * Composer 管理器超类
 *
 * @method static Manager instance() 获取实例的方法
 *
 */
class Manager
{
    use Concerns\HasOutput;
    use Concerns\HasAssertions;
    use Concerns\HasAutoloader;
    use Concerns\HasRequirements;
    use Concerns\HasBingoCommands;

    /**
     * 初始化单例
     */
    public function init(): void
    {
        $this->setOutput();
    }

    /**
     * 执行 "composer update" 命令
     */
    public function update(array $packages = []): void
    {
        $this->assertEnvironmentReady();
        $this->assertHomeVariableSet();

        try {
            $this->assertHomeDirectory();
            $this->assertComposerWarmedUp();

            Installer::create($this->output, $this->makeComposer())
                ->setDevMode(Config::get('app.debug', false))
                ->setUpdateAllowList($packages)
                ->setPreferDist()
                ->setUpdate(true)
                ->run();
        } finally {
            $this->assertWorkingDirectory();
        }
    }

    /**
     * 执行 "composer require" 命令
     */
    public function require(array $requirements): void
    {
        $this->assertEnvironmentReady();
        $this->assertHomeVariableSet();
        $this->backupComposerFile();

        $statusCode = 1;
        $lastException = new Exception('Failed to update composer dependencies');

        try {
            $this->assertHomeDirectory();
            $this->assertComposerWarmedUp();
            $this->writePackages($requirements);

            $composer = $this->makeComposer();
            $installer = Installer::create($this->output, $composer)
                ->setDevMode(Config::get('app.debug', false))
                ->setPreferDist()
                ->setUpdate(true)
                ->setUpdateAllowTransitiveDependencies(Request::UPDATE_LISTED_WITH_TRANSITIVE_DEPS);

            if ($composer->getLocker()->isLocked()) {
                $installer->setUpdateAllowList(array_keys($requirements));
            }

            $statusCode = $installer->run();
        } catch (Throwable $ex) {
            $statusCode = 1;
            $lastException = $ex;
        } finally {
            $this->assertWorkingDirectory();
        }

        if ($statusCode !== 0) {
            $this->restoreComposerFile();
            throw $lastException;
        }
    }

    /**
     * 执行 "composer remove" 命令
     */
    public function remove(array $packageNames): void
    {
        $requirements = [];
        foreach ($packageNames as $package) {
            $requirements[$package] = false;
        }

        $this->require($requirements);
    }

    /**
     * 添加包但不更新
     */
    public function addPackages(array $requirements): void
    {
        $this->writePackages($requirements);
    }

    /**
     * 移除包但不更新
     */
    public function removePackages(array $packageNames): void
    {
        $requirements = [];
        foreach ($packageNames as $package) {
            $requirements[$package] = false;
        }

        $this->writePackages($requirements);
    }

    /**
     * 返回指定包的版本号
     */
    public function getPackageVersions(array $packageNames): array
    {
        $result = [];
        $packages = $this->listAllPackages();

        foreach ($packageNames as $wantPackage) {
            $wantPackageLower = mb_strtolower($wantPackage);

            foreach ($packages as $package) {
                if (! isset($package['name'])) {
                    continue;
                }
                if (mb_strtolower($package['name']) === $wantPackageLower) {
                    $result[$wantPackage] = $package['version'] ?? null;
                }
            }
        }

        return $result;
    }

    /**
     * 返回如果指定包已安装则为真
     */
    public function hasPackage(string $name): bool
    {
        $name = mb_strtolower($name);

        return array_key_exists($name, $this->getPackageVersions([$name]));
    }

    /**
     * 返回直接安装的包列表
     */
    public function listPackages(): array
    {
        return $this->listPackagesInternal();
    }

    /**
     * 返回安装的所有包列表，包括依赖项
     */
    public function listAllPackages(): array
    {
        return $this->listPackagesInternal(false);
    }

    /**
     * 在 composer 配置中添加一个仓库
     */
    public function addRepository(string $name, string $type, string $address, array $options = []): void
    {
        $file = new JsonFile($this->getJsonPath());

        $config = new JsonConfigSource($file);

        $config->addRepository($name, array_merge([
            'type' => $type,
            'url' => $address
        ], $options));
    }

    /**
     * 从 composer 配置中移除一个仓库
     */
    public function removeRepository(string $name): void
    {
        $file = new JsonFile($this->getJsonPath());

        $config = new JsonConfigSource($file);

        $config->removeConfigSetting($name);
    }

    /**
     * 如果 composer 配置包含仓库地址则返回真
     */
    public function hasRepository(string $address): bool
    {
        $file = new JsonFile($this->getJsonPath());

        $config = $file->read();

        $repos = $config['repositories'] ?? [];

        foreach ($repos as $repo) {
            if (! isset($repo['url'])) {
                continue;
            }

            if (rtrim($repo['url'], '/') === $address) {
                return true;
            }
        }

        return false;
    }

    /**
     * 向 auth 配置文件添加凭据
     */
    public function addAuthCredentials(string $hostname, string $username, string $password, string $type = null): void
    {
        if ($type === null) {
            $type = 'http-basic';
        }

        $file = new JsonFile($this->getAuthPath());

        $config = new JsonConfigSource($file, true);

        $config->addConfigSetting($type.'.'.$hostname, [
            'username' => $username,
            'password' => $password
        ]);
    }

    /**
     * 返回添加到配置文件的认证凭据
     */
    public function getAuthCredentials(string $hostname, string $type = null): ?array
    {
        if ($type === null) {
            $type = 'http-basic';
        }

        $authFile = $this->getAuthPath();

        $config = json_decode(file_get_contents($authFile), true);

        return $config[$type][$hostname] ?? null;
    }

    /**
     * 返回一个新的 composer 实例
     */
    protected function makeComposer(): Composer
    {
        $composer = Factory::create($this->output);

        // 禁用脚本
        $composer->getEventDispatcher()->setRunScripts(false);

        // 丢弃更改以防止状态损坏
        $composer->getConfig()->merge([
            'config' => [
                'discard-changes' => true
            ]
        ]);

        return $composer;
    }

    /**
     * 返回已安装包列表
     */
    protected function listPackagesInternal(bool $useDirect = true): array
    {
        $composerLock = base_path('vendor/composer/installed.json');
        $composerFile = $this->getJsonPath();

        $installedPackages = json_decode(file_get_contents($composerLock), true);
        $packages = $installedPackages['packages'] ?? [];

        $filter = [];
        if ($useDirect) {
            $composerPackages = json_decode(file_get_contents($composerFile), true);
            $require = array_merge(
                $composerPackages['require'] ?? [],
                $composerPackages['require-dev'] ?? []
            );

            foreach ($require as $pkg => $ver) {
                $filter[$pkg] = true;
            }
        }

        $result = [];
        foreach ($packages as $package) {
            $name = $package['name'] ?? '';
            if ($useDirect && ! isset($filter[$name])) {
                continue;
            }

            $result[] = [
                'name' => $name,
                'version' => $this->normalizeVersion($package['version'] ?? ''),
                'description' => $package['description'] ?? '',
            ];
        }

        return $result;
    }

    /**
     * 规范化版本
     */
    protected function normalizeVersion($packageVersion): string
    {
        $version = (new VersionParser())->normalize($packageVersion);
        $parts = explode('.', $version);

        if (count($parts) === 4 && preg_match('{^0\D?}', $parts[3])) {
            unset($parts[3]);
            $version = implode('.', $parts);
        }

        return $version;
    }

    /**
     * getJsonPath 返回 composer.json 文件的路径
     */
    protected function getJsonPath(): string
    {
        return base_path('composer.json');
    }

    /**
     * getAuthPath 返回 auth.json 文件的路径
     */
    protected function getAuthPath(): string
    {
        return base_path('auth.json');
    }
}
