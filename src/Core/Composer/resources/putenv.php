<?php

namespace Composer\Util
{
    /**
     * putenv cannot be removed so we suppress it
     */
    function putenv()
    {
        // Do nothing
    }
}
