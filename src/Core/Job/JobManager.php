<?php

namespace Bingo\Core\Job;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Modules\CronJob\Services\CronJobApplicationService;

/**
 * 计划任务管理器
 */
final class JobManager
{
    private const CACHE_KEY = 'registered_jobs_loaded';
    private const CACHE_TTL = 3600; // 1小时缓存

    /**
     * @var array 注册的计划任务
     */
    protected array $registeredJobs = [];

    /**
     * 创建此单例的新实例
     */
    public static function instance(): static
    {
        return App::make('job.manager');
    }

    /**
     * 加载所有已注册的计划任务
     */
    public function loadRegisteredJobs(): void
    {
        // 检查是否已经加载过任务
        if (Cache::has(self::CACHE_KEY)) {
            return;
        }


        try {
            $cronJobService = app(CronJobApplicationService::class);

            foreach ($this->registeredJobs as $code => $job) {
                try {
                    \Log::info("正在处理计划任务", [
                        'code' => $code,
                        'name' => $job['name']
                    ]);

                    // 检查任务是否已存在
                    $existingJobs = $cronJobService->searchCronJobs($job['name'], 1, 1);
                    if ($existingJobs['total'] === 0) {
                        // 创建新任务
                        $cronJobService->createCronJob([
                            'name' => $job['name'],
                            'type' => $job['type'] ?? 'artisan',
                            'spec' => $job['spec'],
                            'command' => $job['command'],
                            'platform' => $job['platform'] ?? (PHP_OS_FAMILY === 'Windows' ? 'windows' : 'linux'),
                            'creator_id' => $job['creator_id'] ?? 1,
                            'lang' => $job['lang'] ?? config('app.locale', 'zh_CN'),
                            'status' => $job['status'] ?? 'waiting'
                        ]);


                    }
                } catch (\Exception $e) {
                    \Log::error("注册计划任务失败", [
                        'code' => $code,
                        'name' => $job['name'],
                        'error' => $e->getMessage(),
                        'trace' => $e->getTraceAsString()
                    ]);
                }
            }

            // 设置缓存标记，避免重复加载
            Cache::put(self::CACHE_KEY, true, self::CACHE_TTL);

        } catch (\Exception $e) {
            \Log::error('初始化 CronJobApplicationService 失败', [
                'error' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
        }
    }

    /**
     * 注册计划任务
     *
     * @param string $module 模块名称
     * @param array<string,array> $jobs 计划任务配置
     */
    public function registerJobs(string $module, array $jobs): void
    {


        foreach ($jobs as $code => $job) {
            $this->registeredJobs[$module.'.'.$code] = array_merge($job, [
                'module' => $module
            ]);
        }
    }

    /**
     * 获取已注册的计划任务列表
     *
     * @return array<string,array>
     */
    public function getRegisteredJobs(): array
    {
        return $this->registeredJobs;
    }
}
