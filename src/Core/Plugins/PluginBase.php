<?php

namespace Bingo\Core\Plugins;

use Bingo\Contracts\BingoPackage;
use Exception;
use Illuminate\Support\ServiceProvider as ServiceProviderBase;
use ReflectionClass;
use Yaml;

/**
 * PluginBase class
 */
class PluginBase extends ServiceProviderBase implements BingoPackage
{
    /**
     * @var array require plugin dependencies.
     */
    public array $require = [];

    /**
     * @var boolean disabled determines if this plugin should be loaded (false) or not (true).
     */
    public bool $disabled = false;

    /**
     * @var bool loadedYamlConfiguration
     */
    protected $loadedYamlConfiguration = false;

    /**
     * pluginDetails returns information about this plugin, including plugin name and developer name.
     *
     * @return array
     * @throws Exception
     */
    public function pluginDetails(): array
    {
        $thisClass = get_class($this);

        $configuration = $this->getConfigurationFromYaml(sprintf(
            'Plugin configuration file plugin.yaml is not '.
            'found for the plugin class %s. Create the file or override pluginDetails() '.
            'method in the plugin class.',
            $thisClass
        ));

        if (array_key_exists('plugin', $configuration)) {
            return $configuration['plugin'];
        }

        throw new Exception(sprintf(
            'The plugin configuration file plugin.yaml should contain the "plugin" section: %s.',
            $thisClass
        ));
    }

    /**
     * register method, called when the plugin is first registered.
     */
    public function register()
    {
    }

    /**
     * boot method, called right before the request route.
     */
    public function boot()
    {
    }

    /**
     * @inheritDoc
     */
    public function registerComponents(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function registerPageSnippets(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function registerContentFields(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function registerNavigation(): array
    {
        $configuration = $this->getConfigurationFromYaml();

        if (! array_key_exists('navigation', $configuration)) {
            return [];
        }

        $navigation = $configuration['navigation'];

        if (! is_array($navigation)) {
            return [];
        }

        //        array_walk_recursive($navigation, static function (&$item, $key) {
        //            if ($key === 'url') {
        //                $item = Backend::url($item);
        //            }
        //        });

        return $navigation;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function registerPermissions(): array
    {
        $configuration = $this->getConfigurationFromYaml();

        if (! array_key_exists('permissions', $configuration)) {
            return [];
        }

        return $configuration['permissions'];
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function registerSettings(): array
    {
        $configuration = $this->getConfigurationFromYaml();

        if (! array_key_exists('settings', $configuration)) {
            return [];
        }

        $settings = $configuration['settings'];

        if (! is_array($settings)) {
            return [];
        }

        //        array_walk_recursive($settings, function (&$item, $key) {
        //            if ($key === 'url') {
        //                $item = Backend::url($item);
        //            }
        //        });

        return $settings;
    }

    /**
     * @inheritDoc
     */
    public function registerSchedule($schedule): void
    {
    }

    /**
     * @inheritDoc
     */
    public function registerReportWidgets(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function registerFormWidgets(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function registerFilterWidgets(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function registerListColumnTypes(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function registerMailLayouts(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function registerMailTemplates(): array
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function registerMailPartials(): array
    {
        return [];
    }

    /**
     * registerConsoleCommand registers a new console (artisan) command.
     * @param string $key The command name
     * @param string $class The command class
     * @return void
     */
    public function registerConsoleCommand(string $key, string $class): void
    {
        $key = 'command.'.$key;

        $this->app->singleton($key, $class);

        $this->commands($key);
    }

    /**
     * registerValidationRule registers a new validation rule.
     * @param string $key The rule name
     * @param mixed $rule The validation rule
     * @return void
     */
    public function registerValidationRule(string $key, mixed $rule): void
    {
        $this->callAfterResolving('validator', function ($validator) use ($key, $rule) {
            $validator->extend($key, $rule);
        });
    }

    /**
     * getConfigurationFromYaml reads configuration from YAML file.
     * @param string|null $exceptionMessage
     * @return array|bool
     * @throws Exception
     */
    protected function getConfigurationFromYaml(string $exceptionMessage = null): bool|array
    {
        if ($this->loadedYamlConfiguration !== false) {
            return $this->loadedYamlConfiguration;
        }

        $reflection = new ReflectionClass(get_class($this));
        $yamlFilePath = dirname($reflection->getFileName()).'/plugin.yaml';

        if (file_exists($yamlFilePath)) {
            $this->loadedYamlConfiguration = Yaml::parse(file_get_contents($yamlFilePath));

            if (! is_array($this->loadedYamlConfiguration)) {
                throw new Exception(sprintf(
                    'Invalid format of the plugin configuration file: %s. The file should define an array.',
                    $yamlFilePath
                ));
            }
        } else {
            if ($exceptionMessage !== null) {
                throw new Exception($exceptionMessage);
            }

            $this->loadedYamlConfiguration = [];
        }

        return $this->loadedYamlConfiguration;
    }
}
