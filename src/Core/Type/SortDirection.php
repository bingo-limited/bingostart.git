<?php

namespace Bingo\Core\Type;

class SortDirection implements BaseType
{
    public const UP = 'up';
    public const DOWN = 'down';
    public const TOP = 'top';
    public const BOTTOM = 'bottom';

    public static function getList(): array
    {
        return [
            self::UP => T('Up'),
            self::DOWN => T('Down'),
            self::TOP => T('Top'),
            self::BOTTOM => T('Bottom'),
        ];
    }

}
