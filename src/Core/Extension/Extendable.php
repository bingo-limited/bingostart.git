<?php

namespace Bingo\Core\Extension;

/**
 * 可擴展類
 *
 * 如果一個類繼承了這個類，它將啟用對"私有特徵"的支持。
 *
 * 用法：
 *
 *     public $implement = [\Path\To\Some\Namespace\Class::class];
 *
 * 查看 `ExtensionBase` 類以創建擴展類。
 *
 * @package bingo\core\extension
 */
class Extendable
{
    use ExtendableTrait;

    /**
     * @var array 為此類實現擴展。
     */
    public $implement = [];

    /**
     * __construct 構造可擴展類
     */
    public function __construct()
    {
        $this->extendableConstruct();
    }

    /**
     * __get 獲取未定義的屬性
     */
    public function __get($name)
    {
        return $this->extendableGet($name);
    }

    /**
     * __set 設置未定義的屬性
     */
    public function __set($name, $value)
    {
        $this->extendableSet($name, $value);
    }

    /**
     * __call 調用未定義的本地方法
     */
    public function __call($name, $params)
    {
        return $this->extendableCall($name, $params);
    }

    /**
     * __callStatic 調用未定義的靜態方法
     * @param $name
     * @param $params
     * @return mixed
     * @throws \ReflectionException
     */
    public static function __callStatic($name, $params)
    {
        return self::extendableCallStatic($name, $params);
    }

    /**
     * __sleep 準備對象進行序列化。
     */
    public function __sleep()
    {
        $this->extendableDestruct();

        return array_keys(get_object_vars($this));
    }

    /**
     * __wakeup 當模型被反序列化時，檢查是否需要啟動。
     */
    public function __wakeup()
    {
        $this->extendableConstruct();
    }

    /**
     * extend 使用閉包擴展此類
     */
    public static function extend(callable $callback): void
    {
        self::extendableExtendCallback($callback);
    }
}