<?php

namespace Bingo\Core\Extension;

/**
 * ExtensionBase 允許使用"私有特徵"
 */
class ExtensionBase
{
    use ExtensionTrait;

    /**
     * 使用閉包擴展此類
     */
    public static function extend(callable $callback): void
    {
        self::extensionExtendCallback($callback);
    }
}