<?php

namespace Bingo\Core\Monitor;

use Illuminate\Foundation\Http\Events\RequestHandled;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Bingo\Core\Util\SerializeUtil;

class StatisticMonitor
{
    protected static array $timeMap = [];
    protected static $client = null;
    private static bool $inited = false;
    private static $reportAddress = null;

    public static function isEnable(): bool
    {
        if (! defined('LARAVEL_START')) {
            return false;
        }
        if (! self::$inited) {
            self::$reportAddress = config('bingo.statisticServer', null);
            self::$inited = true;
        }
        return ! empty(self::$reportAddress);
    }

    public static function init(): void
    {
        if (! self::isEnable()) {
            return;
        }
        $eventName = 'kernel.handled';
        if (class_exists('Illuminate\\Foundation\\Http\\Events\\RequestHandled')) {
            $eventName = 'Illuminate\\Foundation\\Http\\Events\\RequestHandled';
        }
        Event::listen($eventName, function ($eventOrRequest = null) use ($eventName) {
            $request = $eventOrRequest;
            if ($eventName !== 'kernel.handled') {
                /** @var RequestHandled $eventOrRequest */
                $request = $eventOrRequest->request;
            }
            $time = round((microtime(true) - LARAVEL_START) * 1000, 2);
            //            $url = $request->url();
            $method = $request->method();
            $routeAction = Route::currentRouteAction();
            $domain = \Bingo\Core\Input\Request::domain();
            self::tick($domain, "$method.".$routeAction, $time);
        });
    }

    public static function tickStart($module, $group): int
    {
        if (! self::isEnable()) {
            return 0;
        }
        return self::$timeMap[$module][$group] = microtime(true);
    }

    public static function tickEnd($module, $group, $success = true, $code = 0, $msg = null): void
    {
        if (! self::isEnable()) {
            return;
        }
        if (isset(self::$timeMap[$module][$group]) && self::$timeMap[$module][$group] > 0) {
            $timeStart = self::$timeMap[$module][$group];
            self::$timeMap[$module][$group] = 0;
        } else {
            $timeStart = microtime(true);
        }
        self::send($module, $group, round((microtime(true) - $timeStart) * 1000, 2), $success, $code, $msg);
    }

    public static function tick($module, $group, $costMS, $success = true, $code = 0, $msg = null): void
    {
        if (! self::isEnable()) {
            return;
        }
        self::send($module, $group, round($costMS, 2), $success, $code, $msg);
    }

    private static function send($module, $group, $cost, $success, $code, $msg): void
    {
        try {
            $binData = self::encode($module, $group, $cost, $success, $code, $msg);
            $timeout = 0;
            $socket = stream_socket_client('udp://'.self::$reportAddress, $errno, $errmsg, $timeout);
            if (! $socket) {
                return;
            }
            stream_set_timeout($socket, $timeout);
            stream_socket_sendto($socket, $binData);
        } catch (\Exception $e) {
            Log::error('Bingo.StatisticMonitor.Error -> '.$e->getMessage());
        }
    }

    private static function encode($module, $group, $cost, $success, $code = 0, $msg = ''): string
    {
        $data = [
            'module' => $module,
            'group' => $group,
            'cost' => $cost,
            'success' => $success,
            'time' => time(),
            'code' => $code,
            'msg' => $msg
        ];
        $string = SerializeUtil::jsonEncode($data);
        return pack('N', strlen($string)).$string;
    }
}
