<?php

namespace Bingo\Core\Widgets;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Collection;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class WidgetManager
{
    /**
     * 依赖注入容器，用于解析小部件实例
     *
     * @var Container
     */
    protected Container $container;

    /**
     * 已注册的小部件列表
     *
     * @var array
     */
    public array $widgets = [];

    /**
     * WidgetManager constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * 注册一个新的小部件
     *
     * @param string $className
     * @param array $widgetInfo
     */
    public function registerWidget(string $className, array $widgetInfo): void
    {
        $alias = $widgetInfo['alias'] ?? throw new \InvalidArgumentException("Widget alias must be specified");
        $this->widgets[$alias] = [
            'class' => $className,
            'info' => $widgetInfo
        ];
    }

    /**
     * 获取指定小部件的信息
     *
     * @param string $className
     * @return array|null
     */
    public function getWidget(string $className): ?array
    {
        return $this->widgets[$className] ?? null;
    }

    /**
     * 添加小部件到主 widgets 数组
     *
     * @param string $className
     * @param array $widgetInfo
     */
    protected function addWidget(string $className, array $widgetInfo): void
    {
        $this->widgets[$className] = $widgetInfo;
    }

    /**
     * 获取所有已注册的小部件
     *
     * @return array
     */
    public function getAllWidgets(): array
    {
        return $this->widgets;
    }

    /**
     * 从配置中获取小部件数据
     *
     * @param array $configuredWidgets
     * @return Collection
     */
    public function getWidgetsFromConfig(array $configuredWidgets): Collection
    {
        return collect($configuredWidgets)
            ->map(function ($widgetConfig, $alias) {
                try {
                    $widget = $this->resolveWidget($alias, $widgetConfig);
                    return [
                        'alias' => $alias,
                        'data' => $widget->getData(),
                        'config' => $widgetConfig,
                    ];
                } catch (\Exception $e) {
                    Log::error("Error resolving widget: " . $e->getMessage(), ['alias' => $alias, 'trace' => $e->getTraceAsString()]);
                    return null;
                }
            })
            ->filter()
            ->values();
    }

    /**
     * 根据别名获取小部件并准备数据
     *
     * @param string $alias
     * @return array|null
     * @throws BindingResolutionException
     */
    protected function prepareWidgetData(string $alias): ?array
    {
        $widget = $this->getWidgetByAlias($alias);

        if ($widget) {
            if ($widget->isVisible()) {
                return [
                    'alias' => $widget->getAlias(),
                    'data' => $widget->getData(),
                    'config' => $widget->getConfig(),
                ];
            }
        }

        return null;
    }

    /**
     * 获取指定别名的小部件实例
     *
     * @param string $alias
     * @return WidgetBase|null
     * @throws BindingResolutionException
     */
    public function getWidgetByAlias(string $alias): ?WidgetBase
    {
        foreach ($this->widgets as $widgetClass => $widgetConfig) {
            if ($widgetConfig['alias'] === $alias) {
                return $this->resolveWidget($widgetClass, $widgetConfig);
            }
        }
        Log::error("Widget with alias {$alias} not found.");
        return null;
    }

    /**
     * 实例化并解析小部件
     *
     * @param string $alias
     * @param array $widgetConfig
     * @return WidgetBase
     * @throws BindingResolutionException
     */
    public function resolveWidget(string $alias, array $widgetConfig): WidgetBase
    {
        if (!isset($this->widgets[$alias])) {
            Log::error("Widget not registered", ['alias' => $alias]);
            throw new \Exception("Widget not registered: $alias");
        }

        $className = $this->widgets[$alias]['class'];
        return $this->container->make($className, ['config' => $widgetConfig]);
    }

    /**
     * 通过别名查找类名，如果输入已经是类名则返回原输入
     *
     * @param string $aliasOrClassName
     * @return string
     */
    protected function findClassNameByAlias(string $alias): ?string
    {
        foreach ($this->widgets as $class => $config) {
            if ($config['alias'] === $alias) {
                return $class;
            }
        }
        return null;
    }

    /**
     * 检查小部件是否已注册
     * @param string $alias
     * @return bool
     */
    public function isWidgetRegistered(string $alias): bool
    {
        return isset($this->widgets[$alias]);
    }

    /**
     * 移除已注册的小部件
     *
     * @param string $className
     */
    public function removeWidget(string $className): void
    {
        unset($this->widgets[$className]);
    }

    /**
     * 生成小部件代码
     *
     * @param string $className
     * @return string
     */
    protected function generateWidgetCode(string $className): string
    {
        return Str::slug(class_basename($className));
    }
}
