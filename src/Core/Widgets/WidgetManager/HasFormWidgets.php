<?php

namespace Bingo\Core\Widgets\WidgetManager;

trait HasFormWidgets
{

    /**
     * @var array formWidgets 儲存表單小部件
     */
    protected array $formWidgets = [];

    /**
     * 註冊單個表單小部件
     *
     * @param string $className 小部件類名
     * @param array|string $widgetInfo 註冊信息，可以包含 'code' 鍵
     * @return void
     */
    public function registerFormWidget(string $className, array|string $widgetInfo): void
    {
        if (!is_array($widgetInfo)) {
            $widgetInfo = ['code' => $widgetInfo];
        }

        $widgetCode = $widgetInfo['code'] ?? $this->generateWidgetCode($className);

        $this->formWidgets[$className] = $widgetInfo;
        $this->addWidget($className, array_merge($widgetInfo, [
            'type' => 'form',
            'code' => $widgetCode
        ]));
    }

    /**
     * 獲取所有註冊的表單小部件
     *
     * @return array
     */
    public function getFormWidgets(): array
    {
        return $this->formWidgets;
    }

    /**
     * 解析表單小部件的類名
     *
     * @param string $name 類名或表單小部件代碼
     * @return string 解析後的類名
     */
    public function resolveFormWidget(string $name): string
    {
        foreach ($this->formWidgets as $className => $widgetInfo) {
            if ($widgetInfo['code'] === $name || $className === $name) {
                return $className;
            }
        }

        return $name;
    }

}