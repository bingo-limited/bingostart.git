<?php

namespace Bingo\Core\View;

use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

trait AdminResponsiveViewTrait
{
    public function getModule()
    {
        static $module = null;
        if (null === $module) {
            $cls = get_class($this);
            if (preg_match('/^Modules\\\\([\\w]+).*?/', $cls, $mat)) {
                $module = $mat[1];
            } elseif (method_exists($this, 'getCurrentModule')) {
                $module = $this->getCurrentModule();
            } else {
                $module = '';
            }
        }
        return $module;
    }

    private function fetchViewPath($templateName, $templateRoot, $module, $device, $view)
    {
        if (Str::contains($view, '::')) {
            return $view;
        }
        $viewThemeCustom = "theme.$templateName.$device.$view";
        $viewTheme = "$templateRoot.$device.$view";
        if ($module) {
            $viewModule = "module::$module.views.$device.$view";
        }
        $viewDefault = "theme.default.$device.$view";
        if (view()->exists($viewThemeCustom)) {
            return $viewThemeCustom;
        }
        if (view()->exists($viewTheme)) {
            return $viewTheme;
        }
        if ($module) {
            if (view()->exists($viewModule)) {
                return $viewModule;
            }
        }
        if (view()->exists($viewDefault)) {
            return $viewDefault;
        }

    }

    /**
     * @param $view
     * @return array
     * @throws BizException
     *
     * @example
     * list($view, $viewFrame) = $this->viewPaths('member.index')
     */
    protected function viewPaths($view): array
    {
        /**
         * 存储当前模板名称，如 default, xxxxx
         */
        static $templateName = null;

        /**
         * 当前模板根目录
         */
        static $templateRoot = null;
        if (null === $templateName) {
            $templateName = 'default';
            $templateRoot = 'theme.'.$templateName;
            Session::put('bingoSiteTemplateUsing', $templateName);
        }

        $useView = null;
        $useFrameView = null;
        $module = $this->getModule();
        if (empty($useView)) {
            $useView = $this->fetchViewPath($templateName, $templateRoot, $module, 'admin', $view);
        }
        if (empty($useFrameView)) {
            $useFrameView = $this->fetchViewPath($templateName, $templateRoot, $module, 'admin', 'frame');
        }
        View::share('_viewFrameAdmin', $useFrameView);
        BizException::throwsIfEmpty($useView, Code::FAILED, 'View Not Exists : '.$view);
        return [$useView, $useFrameView];
    }

    /**
     * 渲染模版页面
     * @param $view
     * @param array $viewData
     * @return Application|Factory|\Illuminate\Contracts\View\View|\Illuminate\Foundation\Application
     * @throws BizException
     */
    protected function view($view, array $viewData = [])
    {
        list($view, ) = $this->viewPaths($view);
        return view($view, $viewData);
    }


}
