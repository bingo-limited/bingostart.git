<?php

namespace Bingo\Core\Util;

use BaconQrCode\Renderer\Image\ImagickImageBackEnd;
use BaconQrCode\Renderer\ImageRenderer;
use BaconQrCode\Renderer\RendererStyle\RendererStyle;
use BaconQrCode\Writer;
use Bingo\Enums\Code;
use Bingo\Exceptions\BizException;

/**
 * Class QrcodeUtil
 * @package Bingo\Core\Util
 * @Util 二维码
 */
class QrcodeUtil
{
    /**
     * @Util 生成二维码
     * @desc 生成PNG格式的二维码图片
     * @param $content string 二维码内容
     * @param $size int 大小，默认200
     * @return string 图片二进制串
     */
    public static function png(string $content, int $size = 200)
    {
        if (class_exists(Png::class)) {
            $renderer = new Png();
            $renderer->setMargin(0);
            $renderer->setHeight($size);
            $renderer->setWidth($size);
        } else {
            BizException::throwsIf(! extension_loaded('imagick'), Code::FAILED, 'Please Install imagick extension');
            $renderer = new ImageRenderer(
                new RendererStyle(400),
                new ImagickImageBackEnd()
            );
        }
        $writer = new Writer($renderer);
        return $writer->writeString($content, 'UTF-8');
    }

    /**
     * @Util 生成二维码
     * @desc 生成二维码Base64串
     * @param $content string 二维码内容
     * @param $size int 大小，默认200
     * @return string 二维码Base64字符串
     * @example
     * // 返回 data:image/png;base64,xxxxxxxx
     * QrcodeUtil::pngBase64String('http://www.xxx.com')
     */
    public static function pngBase64String(string $content, int $size = 200): string
    {
        return 'data:image/png;base64,'.base64_encode(self::png($content, $size));
    }
}
