<?php

namespace Bingo\Core\Util;

class SignUtil
{
    public static function check($sign, $params, $appSecret): bool
    {
        if ($sign == self::common($params, $appSecret)) {
            return true;
        }
        // rawurlencode 遵守是94年国际标准备忘录RFC 1738，
        // urlencode 实现的是传统做法，和上者的主要区别是对空格的转义是'+'而不是'%20'
        if ($sign == self::common($params, $appSecret, 'urlencode')) {
            return true;
        }
        return (bool) ($sign == self::common($params, $appSecret, 'rawurlencode'))


        ;
    }

    public static function common($params, $appSecret, $function = 'trim', $appSecretName = 'app_secret'): string
    {
        ksort($params, SORT_STRING);

        $str = [];
        foreach ($params as $k => $v) {
            $str[] = $k.'='.$function($v);
        }

        $str[] = $appSecretName.'='.$appSecret;
        $str = join('&', $str);

        return md5($str);
    }

    public static function checkWithoutSecret($sign, $params, $prefix = null): bool
    {
        // rawurlencode 遵守是94年国际标准备忘录RFC 1738，
        // urlencode 实现的是传统做法，和上者的主要区别是对空格的转义是'+'而不是'%20'
        if ($sign == self::commonWithoutSecret($params, $prefix)) {
            return true;
        }
        return (bool) ($sign == self::commonWithoutSecret($params, $prefix, 'rawurlencode'))


        ;
    }

    public static function commonWithoutSecret($params, $prefix = null, $function = 'urlencode'): string
    {
        ksort($params, SORT_STRING);

        $str = [];
        foreach ($params as $k => $v) {
            $str[] = $k.'='.$function($v);
        }
        $str = join('&', $str);

        if ($prefix) {
            $str = $prefix.'&'.$str;
        }

        return md5($str);
    }
}
