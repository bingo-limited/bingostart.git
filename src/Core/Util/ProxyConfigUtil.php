<?php

namespace Bingo\Core\Util;

/**
 * 代理配置工具類
 *
 * 此類用於管理 HTTP 代理設置，包括代理伺服器地址、端口、認證信息，
 * 以及不需要使用代理的主機列表。
 */
class ProxyConfigUtil
{
    protected ?string $proxyUrl;
    protected ?int $proxyPort;
    protected ?string $proxyUsername;
    protected ?string $proxyPassword;
    protected array $noProxyFor;

    /**
     * 建構函數
     *
     * @param string|null $proxyUrl 代理伺服器 URL
     * @param int|null $proxyPort 代理伺服器端口
     * @param string|null $proxyUsername 代理認證用戶名
     * @param string|null $proxyPassword 代理認證密碼
     * @param array $noProxyFor 不使用代理的主機列表
     */
    public function __construct(
        ?string $proxyUrl = null,
        ?int $proxyPort = null,
        ?string $proxyUsername = null,
        ?string $proxyPassword = null,
        array $noProxyFor = []
    ) {
        $this->proxyUrl = $proxyUrl;
        $this->proxyPort = $proxyPort;
        $this->proxyUsername = $proxyUsername;
        $this->proxyPassword = $proxyPassword;
        $this->noProxyFor = $noProxyFor;
    }

    /**
     * 獲取代理伺服器 URL
     *
     * @return string|null
     */
    public function getProxyUrl(): ?string
    {
        return $this->proxyUrl;
    }

    /**
     * 設置代理伺服器 URL
     *
     * @param string|null $proxyUrl
     */
    public function setProxyUrl(?string $proxyUrl): void
    {
        $this->proxyUrl = $proxyUrl;
    }

    /**
     * 獲取代理伺服器端口
     *
     * @return int|null
     */
    public function getProxyPort(): ?int
    {
        return $this->proxyPort;
    }

    /**
     * 設置代理伺服器端口
     *
     * @param int|null $proxyPort
     */
    public function setProxyPort(?int $proxyPort): void
    {
        $this->proxyPort = $proxyPort;
    }

    /**
     * 獲取代理認證用戶名
     *
     * @return string|null
     */
    public function getProxyUsername(): ?string
    {
        return $this->proxyUsername;
    }

    /**
     * 設置代理認證用戶名
     *
     * @param string|null $proxyUsername
     */
    public function setProxyUsername(?string $proxyUsername): void
    {
        $this->proxyUsername = $proxyUsername;
    }

    /**
     * 獲取代理認證密碼
     *
     * @return string|null
     */
    public function getProxyPassword(): ?string
    {
        return $this->proxyPassword;
    }

    /**
     * 設置代理認證密碼
     *
     * @param string|null $proxyPassword
     */
    public function setProxyPassword(?string $proxyPassword): void
    {
        $this->proxyPassword = $proxyPassword;
    }

    /**
     * 獲取不使用代理的主機列表
     *
     * @return array
     */
    public function getNoProxyFor(): array
    {
        return $this->noProxyFor;
    }

    /**
     * 設置不使用代理的主機列表
     *
     * @param array $noProxyFor
     */
    public function setNoProxyFor(array $noProxyFor): void
    {
        $this->noProxyFor = $noProxyFor;
    }

    /**
     * 添加一個不使用代理的主機
     *
     * @param string $host
     */
    public function addNoProxyFor(string $host): void
    {
        if (!in_array($host, $this->noProxyFor)) {
            $this->noProxyFor[] = $host;
        }
    }
    
    /**
     * 检查是否配置了代理
     *
     * @return bool
     */
    public function isProxyConfigured(): bool
    {
        return !empty($this->proxyUrl) && !empty($this->proxyPort);
    }


    /**
     * 移除一個不使用代理的主機
     *
     * @param string $host
     */
    public function removeNoProxyFor(string $host): void
    {
        $this->noProxyFor = array_diff($this->noProxyFor, [$host]);
    }

    /**
     * 獲取完整的代理字符串
     *
     * @return string|null
     */
    public function getProxyString(): ?string
    {
        if (!$this->proxyUrl) {
            return null;
        }

        $proxyString = $this->proxyUrl;
        if ($this->proxyPort) {
            $proxyString .= ':' . $this->proxyPort;
        }

        if ($this->proxyUsername && $this->proxyPassword) {
            $proxyString = $this->proxyUsername . ':' . $this->proxyPassword . '@' . $proxyString;
        }

        return $proxyString;
    }

    /**
     * 判斷是否應該為指定 URL 使用代理
     *
     * @param string $url
     * @return bool
     */
    public function shouldUseProxyFor(string $url): bool
    {
        $host = parse_url($url, PHP_URL_HOST);
        return !in_array($host, $this->noProxyFor);
    }

    /**
     * 獲取用於 PHP 上下文選項的代理設置
     *
     * @return array
     */
    public function getProxyContextOptions(): array
    {
        $options = [];
        if ($this->getProxyString()) {
            $options['http'] = [
                'proxy' => $this->getProxyString(),
                'request_fulluri' => true,
            ];
            $options['https'] = [
                'proxy' => $this->getProxyString(),
                'request_fulluri' => true,
            ];
        }
        return $options;
    }
}
