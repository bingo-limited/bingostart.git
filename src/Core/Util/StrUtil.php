<?php

namespace Bingo\Core\Util;

use Bingo\Exceptions\BizException;
use Illuminate\Support\Str;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

/**
 * Class StrUtil
 * @package Bingo\Core\Util
 * @Util 字符串处理
 */
class StrUtil
{
    public static function mask($subject, $startIndex = null, $endIndex = null, $maskChar = '*'): string
    {
        $strLen = mb_strlen($subject);

        if (null == $startIndex) {
            $startIndex = floor($strLen / 2);
        }
        if (null == $endIndex) {
            $endIndex = $startIndex + floor($strLen / 2);
        }

        if ($startIndex < 0) {
            $startIndex = 0;
        }
        if ($endIndex >= $strLen - 1) {
            $endIndex = $strLen - 1;
        }

        $maskedSubject = '';
        if ($startIndex > 0) {
            $maskedSubject .= mb_substr($subject, 0, $startIndex);
        }
        $maskedSubject .= str_repeat($maskChar, $endIndex - $startIndex + 1);
        if ($endIndex < $strLen - 1) {
            $maskedSubject .= mb_substr($subject, $endIndex + 1);
        }
        return $maskedSubject;

    }

    /**
     * 返回密码中包含的种类
     *
     * @param $password
     * @return int
     */
    public static function passwordStrength($password): int
    {
        $strength = 0;
        if (! empty($password)) {
            $strength++;
        }
        $password = preg_replace('/\\d+/', '', $password);
        if (! empty($password)) {
            $strength++;
        }
        $password = preg_replace('/[a-z]+/', '', $password);
        if (! empty($password)) {
            $strength++;
        }
        $password = preg_replace('/[A-Z]+/', '', $password);
        if (! empty($password)) {
            $strength++;
        }
        return $strength;
    }

    public static function passwordStrengthUpper($password): bool
    {
        return preg_match('/[A-Z]/', $password) === 1;
    }

    public static function passwordStrengthLower($password): bool
    {
        return preg_match('/[a-z]/', $password) === 1;
    }

    public static function passwordStrengthNumber($password): bool
    {
        return preg_match('/[\d+]/', $password) === 1;
    }

    public static function passwordStrengthSpecial($password): bool
    {
        return preg_match('/[\W_]+/', $password) === 1;
    }

    /**
     * 下划线转驼峰
     *
     * @param $uncamelized_words
     * @param string $separator
     * @return string
     */
    public static function camelize($uncamelized_words, string $separator = '_'): string
    {
        $uncamelized_words = $separator.str_replace($separator, " ", strtolower($uncamelized_words));
        return ltrim(str_replace(" ", "", ucwords($uncamelized_words)), $separator);
    }

    /**
     * 驼峰命名转下划线命名
     *
     * @param $camelCaps
     * @param string $separator
     * @return string
     */
    public static function uncamelize($camelCaps, string $separator = '_'): string
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1".$separator."$2", $camelCaps));
    }

    public static function startWith($haystack, $needles): bool
    {
        foreach ((array) $needles as $needle) {
            if ($needle != '' && mb_strpos($haystack, $needle) === 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * 特殊字符处理<200b><200c><200d>
     * @param string $value
     * @return array|string
     */
    public static function filterSpecialChars(string $value): array|string
    {
        $chars = [
            "\xe2\x80\x8b",
            "\xe2\x80\x8c",
            "\xe2\x80\x8d",
        ];
        return str_replace($chars, '', $value);
    }

    public static function limit($text, $limit = 100, $end = '...'): string
    {
        return Str::limit($text, $limit, $end);
    }

    /**
     * @Util 按照UTF8编码裁减字符串（汉字和英文都占1个宽度）
     * @param $text string 待裁剪字符串
     * @param $limit int 裁剪长度
     * @return string
     */
    public static function mbLimit(string $text, int $limit): string
    {
        return Str::limit($text, $limit, '');
    }

    /**
     * @Util 按照UTF8编码裁减字符串（汉字占3个宽度、英文都占1个宽度）
     * @param $text string 待裁剪字符串
     * @param $limit int 裁剪长度
     */
    public static function mbLimitChars(string $text, int $limit): string
    {
        $chars = mb_str_split($text, 1, 'UTF-8');
        $count = 0;
        $str = '';
        foreach ($chars as $char) {
            $count += strlen($char);
            if ($count > $limit) {
                break;
            }
            $str .= $char;
        }
        return $str;
    }

    /**
     * @Util 计算UTF8字符串宽度（汉字和英文都占1个宽度）
     * @param $text string
     * @return int
     */
    public static function mbLength(string $text): int
    {
        if (empty($text)) {
            return 0;
        }
        return intval(mb_strlen($text, 'UTF-8'));
    }

    public static function mbLengthGt($text, $limit): bool
    {
        return self::mbLength($text) > $limit;
    }

    public static function mbWordwrap($string, $width = 75, $break = "\n"): string
    {
        if ($string === '') {
            return '';
        }

        if (strlen($string) === mb_strlen($string)) {
            return wordwrap($string, $width, $break, true);
        }

        $stringWidth = mb_strlen($string);
        $breakWidth = mb_strlen($break);

        $result = '';
        $lastStart = $lastSpace = 0;

        for ($current = 0; $current < $stringWidth; $current++) {
            $char = mb_substr($string, $current, 1);

            $possibleBreak = $char;
            if ($breakWidth !== 1) {
                $possibleBreak = mb_substr($string, $current, $breakWidth);
            }

            if ($possibleBreak === $break) {
                $result .= mb_substr($string, $lastStart, $current - $lastStart + $breakWidth);
                $current += $breakWidth - 1;
                $lastStart = $lastSpace = $current + 1;
                continue;
            }

            if ($char === ' ') {
                if ($current - $lastStart >= $width) {
                    $result .= mb_substr($string, $lastStart, $current - $lastStart).$break;
                    $lastStart = $current + 1;
                }

                $lastSpace = $current;
                continue;
            }

            if ($current - $lastStart >= $width && $lastStart >= $lastSpace) {
                $result .= mb_substr($string, $lastStart, $current - $lastStart).$break;
                $lastStart = $lastSpace = $current;
                continue;
            }

            if ($current - $lastStart >= $width && $lastStart < $lastSpace) {
                $result .= mb_substr($string, $lastStart, $lastSpace - $lastStart).$break;
                $lastStart = $lastSpace = $lastSpace + 1;
                continue;
            }
        }

        if ($lastStart !== $current) {
            $result .= mb_substr($string, $lastStart, $current - $lastStart);
        }

        return $result;
    }

    public static function split($text, $splitter = ',', $splitterReplaces = ['，', ';', '；']): array
    {
        $text = str_replace($splitterReplaces, $splitter, $text);
        $values = explode($splitter, $text);
        $values = array_map('trim', $values);
        return array_filter($values);
    }

    /**
     * @Util 中文分词，如果未安装分词模块，则使用正则表达式分词
     * @param $content string 分词的内容
     * @return array 分词结果
     * @throws BizException
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public static function wordSplit(string $content): array
    {
        if (bingostart_module_enabled('WordSpliter')) {
            $pcs = \Modules\WordSpliter\Domain\WordSpliterUtil::cut($content);
        } else {
            preg_match_all('/[\x{4e00}-\x{9fa5}]|[A-Za-z]+/u', $content, $mat);
            $pcs = array_filter($mat[0]);
        }
        return $pcs;
    }

    /**
     * 生成UUID，并移除其中的短横线，附加当前线程的ID
     *
     * @return string
     */
    public static function getUUID(): string
    {
        return str_replace('-', '', Str::uuid()->toString()).getmypid();
    }

    /**
     * 生成UUID，并截取到指定位置
     *
     * @param int $endAt
     * @return string
     */
    public static function getUUIDShort(int $endAt): string
    {
        return substr(self::getUUID(), 0, $endAt);
    }

    /**
     * 检查是否为有效的URL (http或https)
     *
     * @param string $url
     * @return bool
     */
    public static function isAvailableUrl(string $url): bool
    {
        return Str::startsWith($url, ['http://', 'https://']);
    }

    /**
     * 对字符串进行脱敏处理，除前面几位和后面几位外，其他的字符以星号代替
     *
     * @param string $content
     * @param int $frontNum
     * @param int $endNum
     * @param null $starNum
     * @return string
     */
    public static function str2Star(string $content, int $frontNum, int $endNum, $starNum = null): string
    {
        if ($frontNum >= strlen($content) || $endNum >= strlen($content) || $frontNum + $endNum >= strlen($content)) {
            return $content;
        }

        $starStr = $starNum === null ? str_repeat('*', strlen($content) - $frontNum - $endNum) : str_repeat('*', $starNum);
        return substr($content, 0, $frontNum).$starStr.substr($content, -$endNum);
    }

    /**
     * 合并两个JSON字符串
     * 如果键相同，后者覆盖前者
     * 如果键不同，合并到前者
     *
     * @param string $originStr
     * @param string $mergeStr
     * @return string|null
     */
    public static function merge(string $originStr, string $mergeStr): ?string
    {
        if (empty($originStr) || empty($mergeStr)) {
            return null;
        }

        $originJSON = json_decode($originStr, true);
        $mergeJSON = json_decode($mergeStr, true);

        if ($originJSON === null || $mergeJSON === null) {
            return null;
        }

        return json_encode(array_merge($originJSON, $mergeJSON));
    }

    /**
     * 自动数据脱敏
     *
     * @param string $str
     * @return string
     */
    public static function autoDesensitization(string $str): string
    {
        if (empty($str)) {
            return $str;
        }

        $len = strlen($str);
        if ($len == 1) {
            return "*";
        }
        if ($len <= 3) {
            return str_repeat("*", $len - 1).substr($str, -1);
        }

        $x = ($len >= 7 && $len % 7 == 0) ? $len / 7 : intval($len / 7) + 1;

        $result = '';
        $startIndex = 0;
        while ($startIndex < $len) {
            for ($i = 1; $i <= 3; $i++) {
                if ($startIndex + $x > $len) {
                    $y = $len - $startIndex;
                    $result .= $i == 1 ? substr($str, $startIndex, $y) : str_repeat("*", $y);
                    $startIndex += $y;
                    break;
                }

                $result .= $i == 1 ? substr($str, $startIndex, $x) : str_repeat("*", $x);
                $startIndex += $x;
            }
        }
        return $result;
    }

}
