<?php

namespace Bingo\Core\Services;

use Thunder\Shortcode\Parser\RegularParser;
use Thunder\Shortcode\Shortcode\ShortcodeInterface;
use Thunder\Shortcode\HandlerContainer\HandlerContainer;
use Thunder\Shortcode\Processor\Processor;
use Thunder\Shortcode\Syntax\CommonSyntax;

class ShortcodeService
{
    private HandlerContainer $handlers;
    private Processor $processor;

    public function __construct()
    {
        $this->handlers = new HandlerContainer();
        $this->processor = new Processor(new RegularParser(new CommonSyntax()), $this->handlers);
    }

    public function register(string $name, callable $callback): void
    {
        if ($this->handlers->has($name)) {
            return;
        }

        $this->handlers->add($name, function (ShortcodeInterface $shortcode) use ($callback) {
            return $callback($shortcode->getParameters(), $shortcode->getContent(), $shortcode->getName());
        });
    }

    public function parse(string $content): string
    {
        return $this->processor->process($content);
    }
}
