<?php

namespace Bingo\Enums\Traits;

use BackedEnum;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;
use ValueError;

/**
 * @method string name()
 * @method int|string value()
 * @method string description(string $localizationGroup = 'enums')
 * @method bool is(BackedEnum $enum)
 * @method bool isNot(BackedEnum $enum)
 * @method bool isAny(array $enums)
 */
trait EnumEnhance
{
    public function name(): string
    {
        return $this->name;
    }

    public function value(): int|string
    {
        return $this->value;
    }

    public function description(string $localizationGroup = 'enums'): string
    {
        $modules = getModuleNameFromClassName(static::class);
        if ($modules) {
            $key = "$modules::$localizationGroup.".static::class.'.'.$this->name;
        } else {
            $key = "$localizationGroup.".static::class.'.'.$this->name;
        }
        return Lang::has($key) ? T($key) : Str::of($this->name)->replace('_', ' ')->lower();
    }

    public function is(BackedEnum $enum): bool
    {
        return $this === $enum;
    }

    public function isNot(BackedEnum $enum): bool
    {
        return ! $this->is($enum);
    }

    public function isAny(array $enums): bool
    {
        return in_array($this, $enums);
    }

    public static function names(): array
    {
        return array_column(static::cases(), 'name');
    }

    public static function values(): array
    {
        return array_column(static::cases(), 'value');
    }

    public static function hasName(string $name, bool $strict = false): bool
    {
        return in_array($name, static::names(), $strict);
    }

    public static function hasValue(int|string $value, bool $strict = false): bool
    {
        return in_array($value, static::values(), $strict);
    }

    public static function fromName(string $name): static
    {
        if (! static::hasName($name)) {
            throw new ValueError("$name is not a valid backing name for enum \"".static::class.'"');
        }

        return head(array_filter(static::cases(), fn (BackedEnum $enum) => $enum->name === $name));
    }

    public static function fromValue(int|string $value): static
    {
        if (! static::hasValue($value)) {
            throw new ValueError("$value is not a valid backing value for enum \"".static::class.'"');
        }

        return head(array_filter(static::cases(), fn (BackedEnum $enum) => $enum->value === $value));
    }

    public static function guess(int|string $key): static
    {
        return match (true) {
            static::hasName($key) => static::fromName($key),
            static::hasValue($key) => static::fromValue($key),
            default => throw new ValueError("$key is illegal for enum \"".static::class.'"')
        };
    }

    public static function toArray(string $localizationGroup = 'enums'): array
    {
        return array_map(function (BackedEnum $item) use ($localizationGroup) {
            /** @var self $item */
            return [
                'name' => $item->name(),
                'value' => $item->value(),
                'description' => method_exists($item, 'description')
                    ? $item->description($localizationGroup)
                    : Str::of($item->name)->replace('_', ' ')->lower(),
            ];
        }, static::cases());
    }

    public static function toSelectArray(string $localizationGroup = 'enums'): array
    {
        return array_reduce(static::toArray($localizationGroup), function ($carry, $item) {
            $carry[$item['value']] = $item['description'];

            return $carry;
        }, []);
    }
}
