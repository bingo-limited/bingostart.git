<?php

declare(strict_types=1);

namespace Bingo\Exceptions;

use Bingo\Enums\Code;

class FailedException extends BingoException
{
    protected $code = Code::FAILED;
}
