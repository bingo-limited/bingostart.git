<?php

declare(strict_types=1);

namespace Bingo\Exceptions;

use Bingo\Enums\HttpStatusCode;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Bingo\Enums\Code;

abstract class BingoException extends HttpException
{
    protected $code = 0;
    protected $message;
    protected array $errors = [];

    /**
     * @param string $message
     * @param int|HttpStatusCode|Code $code
     * @param array $errors
     */
    public function __construct(string $message = '', int|HttpStatusCode|Code $code = 0, array $errors = [])
    {
        if ($code instanceof HttpStatusCode || $code instanceof Code) {
            $code = $code->value();
        }

        if ($this->code instanceof HttpStatusCode && ! $code) {
            $code = $this->code->value();
        }

        if ($this->code instanceof Code && ! $code) {
            $code = $this->code->value();
        }

        $this->errors = $errors;

        parent::__construct($this->statusCode(), $message ?: $this->message, null, [], $code);
    }

    /**
     * status code
     *
     * @return int
     */
    public function statusCode(): int
    {
        return 500;
    }

    /**
     * render
     *
     * @return array
     */
    public function render(): array
    {
        return [
            'code' => $this->code,

            'message' => $this->message,

            'errors' => $this->errors
        ];
    }
}
