<?php

namespace Bingo\Exceptions;

use Bingo\Core\Input\Response;

class ResultException extends \Exception
{
    /**
     * @throws ResultException
     */
    public static function throwsIfFail($ret): void
    {
        if (Response::isError($ret)) {
            throw new ResultException($ret['msg']);
        }
    }

    /**
     * @throws ResultException
     */
    public static function throwsIf($msg, $condition): void
    {
        if ($condition) {
            throw new ResultException($msg);
        }
    }

}
