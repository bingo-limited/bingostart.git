<?php

namespace Bingo\Support\DB;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class Query
{
    /**
     * @var string|null
     */
    protected static string|null $log = null;

    /**
     * @return void
     */
    public static function listen(): void
    {
        DB::listen(function ($query) {
            // 将日期格式化提取到外部变量
            $dateTime = date('Y-m-d H:i');
            // 手动构建日志字符串，避免 sprintf 格式化问题
            $sql = '[' . $dateTime . '] ' . $query->sql . ' | ' . (float)$query->time . ' ms' . PHP_EOL;

            // 如果有绑定的参数，则使用 vsprintf 处理，否则直接追加 SQL 日志
            if (!empty($query->bindings)) {
                static::$log .= vsprintf(str_replace('?', '%s', $sql), $query->bindings);
            } else {
                static::$log .= $sql;
            }
        });
    }


    /**
     * @return void
     */
    public static function log(): void
    {
        if (static::$log) {
            $sqlLogPath = storage_path('logs' . DIRECTORY_SEPARATOR . 'query' . DIRECTORY_SEPARATOR);

            if (!File::isDirectory($sqlLogPath)) {
                File::makeDirectory($sqlLogPath, 0777, true);
            }

            $logFile = $sqlLogPath . date('Ymd') . '.log';

            if (!File::exists($logFile)) {
                File::put($logFile, '', true);
            }

            file_put_contents($logFile, static::$log . PHP_EOL, LOCK_EX | FILE_APPEND);

            static::$log = null;
        }
    }
}
