<?php

namespace Bingo\Support\Concern;

use Illuminate\Support\Collection;
use Bingo\Field\AbstractField;
use Bingo\Field\Type\FieldRenderMode;

/**
 * 字段管理能力
 *
 * Trait HasFields
 * @package Bingo\Support\Concern
 *
 *
 * @method \Bingo\Field\AdminUser            adminUser($column, $label = '')
 * @method \Bingo\Field\AreaChina            areaChina($column, $label = '')
 * @method \Bingo\Field\Audio                audio($column, $label = '')
 * @method \Bingo\Field\Button               button($column, $label = '')
 * @method \Bingo\Field\Captcha              captcha($column, $label = '')
 * @method \Bingo\Field\Checkbox             checkbox($column, $label = '')
 * @method \Bingo\Field\Code                 code($column, $label = '')
 * @method \Bingo\Field\Color                color($column, $label = '')
 * @method \Bingo\Field\ComplexFields        complexFields($column, $label = '')
 * @method \Bingo\Field\ComplexFieldsList    complexFieldsList($column, $label = '')
 * @method \Bingo\Field\Currency             currency($column, $label = '')
 * @method \Bingo\Field\Custom               custom($column, $label = '')
 * @method \Bingo\Field\CustomField          customField($column, $label = '')
 * @method \Bingo\Field\Date                 date($column, $label = '')
 * @method \Bingo\Field\Datetime             datetime($column, $label = '')
 * @method \Bingo\Field\Decimal              decimal($column, $label = '')
 * @method \Bingo\Field\Display              display($column, $label = '')
 * @method \Bingo\Field\DynamicFields        dynamicFields($column, $label = '')
 * @method \Bingo\Field\File                 file($column, $label = '')
 * @method \Bingo\Field\FileTemp             fileTemp($column, $label = '')
 * @method \Bingo\Field\Files                files($column, $label = '')
 * @method \Bingo\Field\Html                 html($column, $label = '')
 * @method \Bingo\Field\Id                   id($column, $label = '')
 * @method \Bingo\Field\Image                image($column, $label = '')
 * @method \Bingo\Field\Images               images($column, $label = '')
 * @method \Bingo\Field\ImagesTemp           imagesTemp($column, $label = '')
 * @method \Bingo\Field\Icon                 icon($column, $label = '')
 * @method \Bingo\Field\Json                 json($column, $label = '')
 * @method \Bingo\Field\JsonIdItems          jsonIdItems($column, $label = '')
 * @method \Bingo\Field\JsonKeyValue         jsonKeyValue($column, $label = '')
 * @method \Bingo\Field\KeyValueList         keyValueList($column, $label = '')
 * @method \Bingo\Field\Link                 link($column, $label = '')
 * @method \Bingo\Field\ManyRelation         manyRelation($column, $label = '')
 * @method \Bingo\Field\Markdown             markdown($column, $label = '')
 * @method \Bingo\Field\Number               number($column, $label = '')
 * @method \Bingo\Field\NumberRange          numberRange($column, $label = '')
 * @method \Bingo\Field\Password             password($column, $label = '')
 * @method \Bingo\Field\Percent              percent($column, $label = '')
 * @method \Bingo\Field\Period               period($column, $label = '')
 * @method \Bingo\Field\Radio                radio($column, $label = '')
 * @method \Bingo\Field\Rate                 rate($column, $label = '')
 * @method \Bingo\Field\Raw                  raw($column, $label = '')
 * @method \Bingo\Field\RichHtml             richHtml($column, $label = '')
 * @method \Bingo\Field\Select               select($column, $label = '')
 * @method \Bingo\Field\SelectRemote         selectRemote($column, $label = '')
 * @method \Bingo\Field\SwitchField          switch ($column, $label = '')
 * @method \Bingo\Field\Tags                 tags($column, $label = '')
 * @method \Bingo\Field\Textarea             textarea($column, $label = '')
 * @method \Bingo\Field\Text                 text($column, $label = '')
 * @method \Bingo\Field\Time                 time($column, $label = '')
 * @method \Bingo\Field\Tree                 tree($column, $label = '')
 * @method \Bingo\Field\Type                 type($column, $label = '')
 * @method \Bingo\Field\Values               values($column, $label = '')
 * @method \Bingo\Field\Video                video($column, $label = '')
 *
 * @example $callback = function (LayoutGrid $layout) { $layout->layoutColumn(4, function ($builder) { }); });
 * @method \Bingo\Layout\LayoutGrid          layoutGrid($callback)
 * @example $callback = function (LayoutTable $layout) { $layout->layoutRow( function ($layout) {  $layout->layoutCol(function ($builder) { });  }); });
 * @method \Bingo\Layout\LayoutTable         layoutTable($callback)
 * @example $callback = function (LayoutTab $layout) { $layout->tab('title',closure});
 * @method \Bingo\Layout\LayoutTab           layoutTab($callback)
 * @example $title = 'title', $callback = function (Form $form) { })
 * @method \Bingo\Layout\LayoutPanel         layoutPanel($title, $callback)
 * @example $title = 'title', $callback = function (Form $form) { })
 * @method \Bingo\Layout\LayoutLine          layoutLine($title, $callback)
 * @method \Bingo\Layout\LayoutSeparator     layoutSeparator($title)
 * @method \Bingo\Layout\LayoutHtml          layoutHtml($html)
 */
trait HasFields
{
    /**
     * 字段集合
     * @var AbstractField[]
     */
    private $fields;
    /**
     * 默认字段渲染模式 @see FieldRenderMode
     * @var string
     */
    private $fieldDefaultRenderMode = 'add';

    private function setupFields()
    {
        $this->fields = new Collection();
    }

    /**
     * 填充所有字段
     */
    public function fillFields()
    {
        $this->fields()->each(function (AbstractField $field) {
            $field->fill($this->item);
        });
    }

    /**
     * 增加一个字段
     * @param AbstractField $field
     * @return $this
     */
    public function pushField(AbstractField $field)
    {
        $this->fields()->push($field);
        return $this;
    }

    /**
     * 移除一个字段
     * @param $column
     * @return $this
     */
    public function removeField($column)
    {
        $this->fields = $this->fields()->filter(function (AbstractField $field) use ($column) {
            return $field->column() != $column;
        });
        return $this;
    }

    /**
     * 在之前追加一个字段
     * @param AbstractField $field
     * @return $this
     */
    public function prependField(AbstractField $field)
    {
        $this->fields()->prepend($field);
        return $this;
    }

    public function fieldDefaultRenderMode($value = null)
    {
        if (null === $value) {
            return $this->fieldDefaultRenderMode;
        }
        return $this->fieldDefaultRenderMode = $value;
    }

    /**
     * @return Collection
     */
    public function fields()
    {
        return $this->fields;
    }

    /**
     * 获取列表中的字段
     * @return AbstractField[]
     */
    public function listableFields()
    {
        return $this->fields->filter(function (AbstractField $item) {
            return $item->listable();
        });
    }

    /**
     * 获取所有可增加字段
     * @param $includeShowOnly bool 是否包含只读字段
     * @return AbstractField[]
     */
    public function addableFields($includeShowOnly = false)
    {
        return $this->fields->filter(function (AbstractField $item) use ($includeShowOnly) {
            return $item->addable() || ($includeShowOnly && $item->formShowOnly());
        });
    }

    /**
     *  获取所有可详情显示字段
     * @return AbstractField[]
     */
    protected function editableFields($includeShowOnly = false)
    {
        return $this->fields->filter(function (AbstractField $item) use ($includeShowOnly) {
            return $item->editable() || ($includeShowOnly && $item->formShowOnly());
        });
    }

    /**
     *  获取所有可详情显示字段
     * @return AbstractField[]
     */
    protected function showableFields()
    {
        return $this->fields->filter(function (AbstractField $item) {
            return $item->showable();
        });
    }

    /**
     * 获取所有可增加字段
     * @return AbstractField[]
     */
    public function sortableFields()
    {
        return $this->fields->filter(function (AbstractField $item) {
            return $item->sortable();
        });
    }

    /**
     * 根据表单name获取字段
     * @param $name
     * @return AbstractField
     */
    public function getFieldByName($name)
    {
        return $this->fields->first(function ($k, AbstractField $item) use ($name) {
            return $item->name() == $name;
        });
    }

    /**
     * 根据字段获取字段
     * @param $name
     * @return AbstractField
     */
    public function getFieldByColumn($column)
    {
        return $this->fields->first(function ($k, AbstractField $item) use ($column) {
            return $item->column() == $column;
        });
    }

}
