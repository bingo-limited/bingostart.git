<?php

namespace Bingo\Data\Support;

use Bingo\Core\Provider\ProviderTrait;

/**
 * @method static AbstractFileManager[] listAll();
 */
class FileManagerProvider
{
    use ProviderTrait;

    private static array $list = [];
}
