<?php

namespace Bingo\Data\Support;

abstract class AbstractFileManager
{
    abstract public function name();

    abstract public function title();

    public function getCategoryTree($category, $param = [])
    {

    }

    public function listExecute($category, $categoryId, $param = [])
    {

    }
}
