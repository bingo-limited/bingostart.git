<?php

namespace Bingo\App\Web\Middleware;

use Illuminate\Http\Request;
use Bingo\App\Core\AccessGate;
use Bingo\App\Core\CurrentApp;
use Bingo\Core\Input\Response;

class BootstrapMiddleware
{
    /**
     * @var AccessGate[]
     */
    private static array $gates = [];

    public static function addGate($cls): void
    {
        self::$gates[] = $cls;
    }

    public function handle(Request $request, \Closure $next)
    {
        //        $request->headers->set('Accept', 'application/json');
        if (method_exists(CurrentApp::class, 'set')) {
            CurrentApp::set(CurrentApp::WEB);
        }

        foreach (self::$gates as $item) {
            /** @var AccessGate $instance */
            $instance = app($item);
            $ret = $instance->check($request);
            if (Response::isError($ret)) {
                return $ret;
            }
        }
        if (file_exists($bootstrap = bingostart_web_path('bootstrap.php'))) {
            require $bootstrap;
        }
        return $next($request);
    }
}
