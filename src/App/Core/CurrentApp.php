<?php

namespace Bingo\App\Core;

use Illuminate\Support\Facades\Session;
use Bingo\Core\Type\BaseType;

class CurrentApp implements BaseType
{
    public const ADMIN = 'Admin';
    public const WEB = 'Web';
    public const OPEN_API = 'OpenApi';
    public const API = 'Api';

    public static function getList(): array
    {
        return [
            self::ADMIN => 'Admin',
            self::WEB => 'Web',
            self::OPEN_API => 'OpenApi',
            self::API => 'Api',
        ];
    }

    public static function set($app): void
    {
        Session::flash('_currentApp', $app);
    }

    public static function get()
    {
        return Session::get('_currentApp');
    }

    public static function is($app): bool
    {
        return self::get() == $app;
    }

}
