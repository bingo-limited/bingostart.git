<?php

namespace Bingo\App\Core;

use Illuminate\Http\Request;

interface AccessGate
{
    public function check(Request $request);
}
