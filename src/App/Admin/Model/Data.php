<?php

namespace Bingo\App\Admin\Model;

use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    protected $table = 'data';
}
