<?php

namespace Bingo\App\Admin\Model;

use Illuminate\Database\Eloquent\Model;

class DataTemp extends Model
{
    protected $table = 'data_temp';
}
