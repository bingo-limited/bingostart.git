<?php

namespace Bingo\App\Admin\Middleware;

use Bingo\App\Core\CurrentApp;
use Illuminate\Http\Request;

class BootstrapMiddleware
{
    public function handle(Request $request, \Closure $next)
    {
        if(method_exists(CurrentApp::class, 'set')) {
            CurrentApp::set(CurrentApp::ADMIN);
        }

        if (file_exists($bootstrap = bingostart_admin_path('bootstrap.php'))) {
            require $bootstrap;
        }
        return $next($request);
    }
}
