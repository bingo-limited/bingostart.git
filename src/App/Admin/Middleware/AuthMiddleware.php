<?php

namespace Bingo\App\Admin\Middleware;

use Bingo\Enums\Code;
use Bingo\Events\User as UserEvent;
use Bingo\Exceptions\FailedException;
use Closure;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Throwable;

class AuthMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        try {
            // 获取 URL 查询参数中的 token
            $token = $request->query('token');

            if ($token) {
                // 将 token 加入到请求的 Authorization 头中
                $request->headers->set('Authorization', 'Bearer '.$token);
            }

            if (! $user = Auth::guard(getGuardName())->user()) {
                throw new AuthenticationException();
            }

            Event::dispatch(new UserEvent($user));

            return $next($request);
        } catch (Exception|Throwable $e) {
            throw new FailedException(Code::LOST_LOGIN->description('code').":{$e->getMessage()}", Code::LOST_LOGIN);
        }
    }

}
