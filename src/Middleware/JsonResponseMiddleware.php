<?php

namespace Bingo\Middleware;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class JsonResponseMiddleware
{
    public function handle(Request $request, \Closure $next)
    {
        $response = $next($request);

        // binary file response
        if ($response instanceof BinaryFileResponse) {
            // set expose header，download excel needs
            $response->headers->set('Access-Control-Expose-Headers', 'filename,write_type');

            return $response;
        }

        // 判断如果是HTML内容进行返回
        if ($this->isViewResponse($response)) {
            $content = $response->getContent();
            $strippedContent = strip_tags($content);
            // 检查处理后的内容是否与原内容相同，且不为空
            if ($content !== $strippedContent && trim($content) !== '') {
                return $response;
            }
        }

        if ($response instanceof Response) {
            return new JsonResponse($response->getContent());
        }

        return $response;
    }

    /**
     * Determine if the response content is a view.
     */
    protected function isViewResponse(SymfonyResponse $response): bool
    {
        // 检查响应的 Content-Type 是否表示 HTML
        $contentType = $response->headers->get('Content-Type');
        return str_contains($contentType, 'text/html') || str_contains($contentType, 'application/xhtml+xml');
    }
}
