<?php

namespace Bingo\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class TransformJsonResponse
{
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        if ($response instanceof Response) {
            $data = json_decode($response->getContent(), true);
            if (is_array($data)) {
                $transformedData = $this->transformKeys($data);
                $response->setContent(json_encode($transformedData));
            }
        }

        return $response;
    }

    protected function transformKeys($data): array
    {
        $newArray = [];
        foreach ($data as $key => $value) {
            $camelKey = Str::camel($key);
            if (is_array($value) || is_object($value)) {
                $value = $this->transformKeys((array) $value);
            }
            $newArray[$camelKey] = $value;
        }
        return $newArray;
    }
}
