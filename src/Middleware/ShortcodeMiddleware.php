<?php

namespace Bingo\Middleware;

use Closure;
use Bingo\Core\Services\ShortcodeService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * ShortcodeMiddleware 中間件用於解析響應內容中的短代碼。
 */
class ShortcodeMiddleware
{
    /**
     * @var ShortcodeService
     */
    protected ShortcodeService $shortcodeService;

    /**
     * 構造函數。
     *
     * @param ShortcodeService $shortcodeService
     */
    public function __construct(ShortcodeService $shortcodeService)
    {
        $this->shortcodeService = $shortcodeService;
    }

    /**
     * 處理傳入的請求。
     *
     * @param Request $request
     * @param Closure $next
     * @return Response|mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        // 确保 $shortcodeService 实例化正确
        $shortcodeService = app(ShortcodeService::class);

        if ($response instanceof Response && $response->headers->contains('Content-Type', 'text/html')) {
            $content = $response->getContent();
            $content = $shortcodeService->parse($content);
            $response->setContent($content);
        }

        return $response;
    }
}
