<?php

namespace Bingo\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

/**
 * RedirectIfXForwardedHostMismatch 中間件
 *
 * 檢查請求的 X-Forwarded-Host 頭部與當前請求的域名是否匹配。這是為了防止 HTTP Host 頭部攻擊，
 * 並確保在使用反向代理的環境中，用戶總是被重定向到正確的域名。當 X-Forwarded-Host 頭部存在，
 * 並且與當前請求的域名不匹配時，此中間件會將請求重定向到正確的域名。
 *
 * 這個中間件主要用在反向代理的配置中，確保應用的安全性和正確的請求路由。它避免了因為錯誤配置的反向代理
 * 導致的潛在安全風險和邏輯錯誤。
 *
 * 使用方法：
 * 1. 通過在 app/Http/Kernel.php 文件的 $middleware 屬性中註冊，使其作為全局中間件運行。
 * 2. 或者，將其註冊到特定路由或路由組中，用於只對部分請求進行處理。
 *
 * 注意：
 * - 確保配置文件 bingo.php 中的 xForwardedHostVisitRedirect 項已正確設置。
 * - 此中間件應謹慎使用，確保它不會干擾開發或測試環境下的正常請求。
 *
 * 示例配置（config/bingo.php）:
 * ```php
 * 'xForwardedHostVisitRedirect' => true,
 * ```
 *
 * Class RedirectIfXForwardedHostMismatch
 * @package Bingo\Middleware
 * @author coso
 * @since 2024-03-01
 * @version 1.0
 */

class RedirectIfXForwardedHostMismatch
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if (config('bingo.xForwardedHostVisitRedirect', true)) {
            $forwardedHost = $request->header('X-Forwarded-Host');
            $domain = $request->getHost();

            if ($forwardedHost && $domain && $forwardedHost != $domain) {
                $localIgnores = ['localhost', '127.0.0.1'];
                if (! Str::contains($forwardedHost, $localIgnores)) {
                    $redirect = $request->getSchemeAndHttpHost().$request->getRequestUri();
                    Log::info('xForwardedHostVisitRedirect - '.$forwardedHost.' to '.$redirect);
                    return redirect()->to($redirect, 301);
                }
            }
        }

        return $next($request);
    }
}
