<?php

namespace Bingo\Contracts;

use Illuminate\Console\Scheduling\Schedule;

/**
 * BingoPackage
 *
 * @package bingo\contracts
 */
interface BingoPackage
{
    /**
     * registerComponents 注册此包实现的任何 CMS 组件。
     *
     *     return [
     *        \Acme\Demo\Components\LocalePicker::class => 'localePicker',
     *     ];
     *
     * @return array
     */
    public function registerComponents(): array;

    /**
     * registerPageSnippets 注册此包实现的任何 CMS 代码片段。
     *
     *     return [
     *        \Acme\Demo\Components\YouTubeVideo::class => 'youtubeVideo',
     *     ];
     *
     * @return array
     */
    public function registerPageSnippets(): array;

    /**
     * registerContentFields 注册此包实现的内容字段。
     *
     *     return [
     *        \Tailor\ContentFields\TextareaField::class => 'textarea',
     *     ];
     *
     * @return array
     */
    public function registerContentFields(): array;

    /**
     * registerNavigation 注册此包的后台导航项。
     *
     *     return [
     *         'blog' => []
     *     ];
     *
     * @return array
     */
    public function registerNavigation(): array;

    /**
     * registerPermissions 注册此包使用的任何权限。
     *
     *     return [
     *         'general.backend' => [
     *             'label' => '访问后台面板',
     *             'tab' => '通用'
     *         ],
     *     ];
     *
     * @return array
     */
    public function registerPermissions(): array;

    /**
     * registerSettings 注册此包使用的任何后台配置链接。
     *
     *     return [
     *         'updates' => []
     *     ];
     *
     * @return array
     */
    public function registerSettings(): array;

    /**
     * registerReportWidgets 注册此包提供的任何报表小部件。
     * 小部件必须以下列格式返回：
     *
     *     return [
     *         'className1' => [
     *             'label' => '我的小部件 1',
     *             'context' => ['context-1', 'context-2'],
     *         ],
     *         'className2' => [
     *             'label' => '我的小部件 2',
     *             'context' => 'context-1'
     *         ]
     *     ];
     *
     * @return array
     */
    public function registerReportWidgets(): array;

    /**
     * registerFormWidgets 注册此包实现的任何表单小部件。
     * 小部件必须以下列格式返回：
     *
     *     return [
     *         ['className1' => 'alias'],
     *         ['className2' => 'anotherAlias']
     *     ];
     *
     * @return array
     */
    public function registerFormWidgets(): array;

    /**
     * registerFilterWidgets 注册此包实现的任何过滤器小部件。
     * 小部件必须以下列格式返回：
     *
     *     return [
     *         ['className1' => 'alias'],
     *         ['className2' => 'anotherAlias']
     *     ];
     *
     * @return array
     */
    public function registerFilterWidgets(): array;

    /**
     * registerListColumnTypes 注册此包引入的自定义后台列表列类型。
     *
     * @return array
     */
    public function registerListColumnTypes(): array;


    /**
     * registerMailTemplates 注册此包实现的任何邮件模板。
     * 模板必须以下列格式返回：
     *
     *     return [
     *         'acme.blog::mail.welcome',
     *         'acme.blog::mail.forgot_password',
     *     ];
     *
     * @return array
     */
    public function registerMailTemplates(): array;


    /**
     * 注册调度任务
     *
     * @return array<string, array>
     */
    public function registerSchedule(): array;
}
