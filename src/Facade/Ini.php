<?php

namespace Bingo\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * Ini
 *
 * @method static array parse(string $contents)
 * @method static array parseFile(string $fileName)
 * @method static string render(array $vars = [], int $level = 1)
 *
 * @see \Bingo\Core\Parse\Ini
 */
class Ini extends Facade
{
    /**
     * getFacadeAccessor returns the registered name of the component
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'parse.ini';
    }
}
