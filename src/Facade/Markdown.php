<?php

namespace Bingo\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * Markdown
 *
 * @method static string parse(string $text)
 * @method static string parseClean(string $text)
 * @method static string parseSafe(string $text)
 * @method static string parseLine(string $text)
 *
 * @see \Bingo\Core\Parse\Markdown
 */
class Markdown extends Facade
{
    /**
     * getFacadeAccessor returns the registered name of the component
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'parse.markdown';
    }
}
