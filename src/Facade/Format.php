<?php

namespace Bingo\Facade;

use Bingo\Core\Input\Contract\ResponseFormat;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Pagination\AbstractCursorPaginator;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Facades\Facade as IlluminateFacade;

/**
 * @method static Format data(mixed $data = null, string $message = '', int|\BackedEnum $code = 200, $error = null)
 * @method static array|null                               get()
 * @method static array                                    paginator(AbstractPaginator|AbstractCursorPaginator|Paginator $resource)
 * @method static array                                    resourceCollection(ResourceCollection $collection)
 * @method static array                                    jsonResource(JsonResource $resource)
 * @method static JsonResponse                             response()
 *
 * @see Format
 */
class Format extends IlluminateFacade
{
    protected static function getFacadeAccessor(): string
    {
        return ResponseFormat::class;
    }
}
