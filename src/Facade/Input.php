<?php

namespace Bingo\Facade;

use Illuminate\Support\Facades\Facade;

/**
 * Input
 *
 * @see \Illuminate\Http\Request
 */
class Input extends Facade
{
    /**
     * get an item from the input data
     * This method is used for all request verbs (GET, POST, PUT, and DELETE)
     *
     * @param string|null $key
     * @param mixed|null $default
     * @return mixed
     */
    public static function get(string $key = null, mixed $default = null): mixed
    {
        return static::$app['request']->input($key, $default);
    }

    /**
     * getFacadeAccessor returns the registered name of the component
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'request';
    }
}
