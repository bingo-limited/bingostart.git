<?php

namespace Bingo\Facade;

use Illuminate\Support\Facades\URL as UrlBase;

/**
 * Url
 *
 * @see \Illuminate\Routing\UrlGenerator
 */
class Url extends UrlBase
{
    /**
     * getFacadeAccessor returns the registered name of the component
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'url';
    }
}
