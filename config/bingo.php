<?php

use Bingo\Core\Assets\Driver\LocalAssetsPath;
use Bingo\Core\Config\Driver\DatabaseBingoConfig;
use Bingo\Listeners\RequestHandledListener;
use Bingo\Middleware\IAMAuthMiddleware;
use Bingo\Middleware\JsonResponseMiddleware;
use Modules\User\Models\User;

return [
    'name' => 'Bingo',
    /*
    |--------------------------------------------------------------------------
    | bingo start default middleware
    |--------------------------------------------------------------------------
    |
    | where you can set default middlewares
    |
    */
    'middleware_group' => [

    ],

    /*
    |--------------------------------------------------------------------------
    | bingo start bingo_auth_middleware_alias
    |--------------------------------------------------------------------------
    |
    | where you can set default middlewares
    |
    */
    'bingo_auth_middleware_alias' => [

    ],

    /*
    |--------------------------------------------------------------------------
    | bingo start super admin id
    |--------------------------------------------------------------------------
    |
    | where you can set super admin id
    |
    */
    'super_admin' => 1,

    'request_allowed' => true,

    /*
    |--------------------------------------------------------------------------
    | bingo start module setting
    |--------------------------------------------------------------------------
    |
    | the root where module generate
    | the namespace is module root namespace
    | the default dirs is module generate default dirs
    */
    'module' => [
        'root' => 'Modules',

        'namespace' => 'Modules',

        'default' => ['develop', 'user', 'common'],

        'default_dirs' => [
            'Api'.DIRECTORY_SEPARATOR,

            'Api'.DIRECTORY_SEPARATOR.'Requests'.DIRECTORY_SEPARATOR,

            'Api'.DIRECTORY_SEPARATOR.'Controllers'.DIRECTORY_SEPARATOR,

            'Web'.DIRECTORY_SEPARATOR,

            'Web'.DIRECTORY_SEPARATOR.'Requests'.DIRECTORY_SEPARATOR,

            'Web'.DIRECTORY_SEPARATOR.'Controllers'.DIRECTORY_SEPARATOR,

            'OpenApi'.DIRECTORY_SEPARATOR,

            'OpenApi'.DIRECTORY_SEPARATOR.'Requests'.DIRECTORY_SEPARATOR,

            'OpenApi'.DIRECTORY_SEPARATOR.'Controllers'.DIRECTORY_SEPARATOR,

            'Admin'.DIRECTORY_SEPARATOR,

            'Admin'.DIRECTORY_SEPARATOR.'Requests'.DIRECTORY_SEPARATOR,

            'Admin'.DIRECTORY_SEPARATOR.'Controllers'.DIRECTORY_SEPARATOR,

            'Models'.DIRECTORY_SEPARATOR,

            'views'.DIRECTORY_SEPARATOR,
        ],

        // storage module information
        // which driver should be used?
        'driver' => [
            // currently, BingoStart support file and database
            // the default is driver
            'default' => 'file',

            // use database driver
            'table_name' => 'admin_modules'
        ],

        /**
         * module routes collection
         *
         */
        'routes' => [],
    ],

    /*
    |--------------------------------------------------------------------------
    | bingo start response
    |--------------------------------------------------------------------------
    */
    'response' => [
        // it's a controller middleware, it's set in BingoController
        // if you not need json response, don't extend BingoController
        'always_json' => JsonResponseMiddleware::class,

        // response listener
        // it  listens [RequestHandled] event, if you don't need this
        // you can change this config
        'request_handled_listener' => RequestHandledListener::class
    ],

    /*
   |--------------------------------------------------------------------------
   | database sql log
   |--------------------------------------------------------------------------
   */
    'listen_db_log' => true,

    /*
   |--------------------------------------------------------------------------
   | admin auth model
   |--------------------------------------------------------------------------
   */
    'auth_model' => User::class,

    /*
   |--------------------------------------------------------------------------
   | route config
   |--------------------------------------------------------------------------
   */
    'route' => [
        'prefix' => 'api',

        'middlewares' => [
            IAMAuthMiddleware::class,
            JsonResponseMiddleware::class
        ],

        // 'cache_path' => base_path('bootstrap/cache') . DIRECTORY_SEPARATOR . 'admin_route_cache.php'
    ],

    'excel' => [
        'export' => [
            'csv_limit' => 20000,

            'path' => 'excel/export/'
        ]
    ],

    'config' => [
        'driver' => DatabaseBingoConfig::class,

        'chunk_size' => 500 // Maximum number of rows per insert statement
    ],

    /**
     * 开启性能追踪，开启后会在日志中记录必要地请求，如慢SQL执行、多查询请求
     */
    'trackPerformance' => env('TRACK_PERFORMANCE', false),
    /**
     * 慢SQL阈值，单位毫秒
     */
    'trackLongSqlThreshold' => env('TRACK_LONG_SQL_THRESHOLD', 5000),
    'statisticServer' => env('STATISTIC_SERVER', null),

    'forceSchema' => env('FORCE_SCHEMA', null),

    // 防止X-Forwarded-Host直接访问，遇到通过CDN域名直接访问的情况，直接跳转到主域名
    'xForwardedHostVisitRedirect' => true,

    'subdirUrl' => env('SUBDIR_URL', null),
    'subdir' => env('SUBDIR', '/'),
    'trackMissingLang' => false,

    'admin' => [
        'disabled' => env('ADMIN_DISABLED', false),
        /**
         * 后台管理路径
         */
        'prefix' => trim(env('ADMIN_PATH', 'admin'), '/'),
        'directory' => app_path('Admin'),
        'login' => [
            /**
             * 后台登录页面验证码
             */
            'captcha' => true,
        ],
        /**
         * 后台多语言配置
         */
        'i18n' => [
            /**
             * 后台是否开启多语言
             */
            'enable' => false,
            'lang' => [
                'zh_CN' => '简体中文',
                'en' => 'English',
            ]
        ],
        /**
         * 后台请求忽略权限校验的 Controller 或 Action
         * 例如：['\App\Admin\Controller\AuthController@login', '\App\Admin\Controller\AuthController']
         */
        'authIgnores' => [],
    ],

    'web' => [
        'prefix' => trim(env('APP_PATH', ''), '/'),
        'directory' => app_path('Web'),
    ],

    'api' => [
        'prefix' => trim(env('API_PATH', 'api'), '/'),
        'directory' => app_path('Api'),
    ],

    'openApi' => [
        'prefix' => trim(env('API_PATH', 'openapi'), '/'),
        'directory' => app_path('OpenApi'),
    ],

    'i18n' => [
        'lang' => [
            'zh_CN' => '简体中文',
            'en' => 'English',
            'zh_HK' => '繁体中文(香港)',
        ]
    ],
    'https' => env('ADMIN_HTTPS', false),
    'upload' => [
        'disk' => 'public',
        'uniqueName' => false,
        'directory' => [
            'image' => 'images',
            'file' => 'files',
        ],
        'mimes' => 'jpeg,bmp,png,gif,jpg',
        'file_mimes' => 'doc,docx,xls,xlsx,ppt,pptx,pdf,zip,rar,7z',
    ],
    'asset' => [
        'driver' => LocalAssetsPath::class,
        'cdn' => env('CDN_URL', '/'),
        'image_none' => '',
    ],
];
